#include "PCH/pch.h"
#include "CommonDefines.h"
#include "MatchingState.h"

#include "Server/System/ServerSubSystems.h"

void MatchingState_NoClient::Start()
{
}

void MatchingState_NoClient::Update(float delta)
{
}

void MatchingState_NoClient::OnChangeEvent()
{
	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	msm->m_CountDownTimer = MatchingTimeLimit;
	m_IsNext = false;
}

void MatchingState_NoClient::OnNetWorkEvent(MessageTypes type)
{
}

void MatchingState_NoClient::OnLoginPlayer(const ZUUID & playerUUID)
{
	m_IsNext = true;
	m_NextStateName = "MatchingCountDown";
}

void MatchingState_MatchingCountDown::Start()
{
	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	
	// マッチングカウントダウン開始を全プレイヤーに通知
	{
		NotifyMatchingLastTimeLimitData matchingLastTimeLimitData;
		matchingLastTimeLimitData.MatchingLastTimeLimit = MatchingTimeLimit;
		SERVER.SendData(MessageTypes::NotifyLastMatchingTimeLimit, &matchingLastTimeLimitData, sizeof(NotifyMatchingLastTimeLimitData), 0);
	}
}

void MatchingState_MatchingCountDown::Update(float delta)
{
	if (m_IsNext)return;

	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	msm->m_CountDownTimer -= delta;

	if (msm->m_CountDownTimer > 0)
		return;

	// マッチングタイムオーバー
	uint securityCnt = msm->GetPlayerCount(LoginData::PlayStyle::Security);
	uint hackerCnt = msm->GetPlayerCount(LoginData::PlayStyle::Hacker);
	// マッチ開始出来る最低限のプレイヤーが揃っていれば
	if (securityCnt >= MinSecurityPlayer &&
		hackerCnt >= MinHackerPlayer)
	{
		m_NextStateName = "MatchStartCountDown";
	}
	else
	{
		// クライアント全員に通知
		SERVER.SendData(MessageTypes::NotifyMatchingTimeOver,nullptr,0,0);
		m_NextStateName = "NoClient";
	}
	m_IsNext = true;
}

void MatchingState_MatchingCountDown::OnChangeEvent()
{
	m_IsNext = false;
}

void MatchingState_MatchingCountDown::OnNetWorkEvent(MessageTypes type)
{
	if (type != MessageTypes::Logout)return;

	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	uint securityCnt = msm->GetPlayerCount(LoginData::PlayStyle::Security);
	uint hackerCnt = msm->GetPlayerCount(LoginData::PlayStyle::Hacker);
	if (securityCnt == 0 && hackerCnt == 0)
	{
		m_IsNext = true;
		m_NextStateName = "NoClient";
	}
}

void MatchingState_MatchingCountDown::OnLoginPlayer(const ZUUID & playerUUID)
{
	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	
	uint securityCnt = msm->GetPlayerCount(LoginData::PlayStyle::Security);
	uint hackerCnt = msm->GetPlayerCount(LoginData::PlayStyle::Hacker);
	
	// マッチング出来る最大プレイヤー数未満なら
	if (securityCnt < MaxSecurityPlayer ||
		hackerCnt < MaxHackerPlayer)
	{
		// ログインしてきたプレーヤーに残りのマッチング制限時間を通知
		NotifyMatchingLastTimeLimitData matchingLastTimeLimitData;
		matchingLastTimeLimitData.MatchingLastTimeLimit = msm->m_CountDownTimer;
		SERVER.SendData(MessageTypes::NotifyLastMatchingTimeLimit, 
			&matchingLastTimeLimitData, sizeof(NotifyMatchingLastTimeLimitData), 0,playerUUID);
	}
	else
	{
		// マッチング出来る最大プレイヤー数に達したらマッチスタート
		m_IsNext = true;
		m_NextStateName = "MatchStartCountDown";
	}
}

void MatchingState_MatchingCountDown::ImGui()
{
	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	ImGui::Text("TimeLimit at %.0f seconds ...", msm->m_CountDownTimer);
}


void MatchingState_MatchStartCountDown::Start()
{
	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	m_LastMatchingCount = msm->m_CountDownTimer;
	msm->m_CountDownTimer = MatchStartTimeLimit;

	// マッチ開始のカウントダウン開始を全プレイヤーに通知
	{
		NotifyLastStartMatchTimeLimitData startMatchTimeLimitData;
		startMatchTimeLimitData.LastStartMatchTimeLimit = MatchStartTimeLimit;
		SERVER.SendData(MessageTypes::NotifyLastStartMatchTimeLimit, &startMatchTimeLimitData, sizeof(NotifyLastStartMatchTimeLimitData), 0);
	}
}

void MatchingState_MatchStartCountDown::Update(float delta)
{
	if (m_IsNext)return;

	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	msm->m_CountDownTimer -= delta;

	if (msm->m_CountDownTimer > 0)
		return;

	// ~~~マッチ開始~~~

	m_IsNext = true;

	// マッチ開始を全プレイヤーに通知
	SERVER.SendData(MessageTypes::NotifyStartMatch, nullptr, 0, 0);
}

void MatchingState_MatchStartCountDown::OnChangeEvent()
{
	m_IsNext = false;
}

void MatchingState_MatchStartCountDown::OnNetWorkEvent(MessageTypes type)
{
	if (type != MessageTypes::Logout)return;

	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	uint securityCnt = msm->GetPlayerCount(LoginData::PlayStyle::Security);
	uint hackerCnt = msm->GetPlayerCount(LoginData::PlayStyle::Hacker);
	
	if (securityCnt <= MinSecurityPlayer &&
		hackerCnt <= MinHackerPlayer)
	{
		m_IsNext = true;
		if(securityCnt > 0 || securityCnt > 0)
		{
			m_NextStateName = "MatchingCountDown";
			msm->m_CountDownTimer = m_LastMatchingCount;
		}
		else
			m_NextStateName = "NoClient";
	}
}

void MatchingState_MatchStartCountDown::OnLoginPlayer(const ZUUID & playerUUID)
{
}

void MatchingState_MatchStartCountDown::ImGui()
{
	MatchingStateMachine* msm = (MatchingStateMachine*)m_pStateMachine;
	ImGui::Text("%.0f Second To Start ...", msm->m_CountDownTimer);
}

MatchingStateMachine::MatchingStateMachine() : StateMachine()
{
}

void MatchingStateMachine::ImGui()
{
	if (m_NowState == nullptr)return;
	((ZSP<MatchingStateBase>)m_NowState)->ImGui();
}

void MatchingStateMachine::OnLoginPlayer(const ZUUID& playerUUID)
{
	if (m_NowState == nullptr)return;
	((ZSP<MatchingStateBase>)m_NowState)->OnLoginPlayer(playerUUID);
}

void MatchingStateMachine::SetPlayerCount(LoginData::PlayStyle style, uint count)
{
	m_PlayerCnt[style] = count;
}

uint MatchingStateMachine::GetPlayerCount(LoginData::PlayStyle style)
{
	return m_PlayerCnt[style];
}

void MatchingStateMachine::OnNetWorkEvent(MessageTypes type)
{
	((ZSP<MatchingStateBase>)m_NowState)->OnNetWorkEvent(type);
}
