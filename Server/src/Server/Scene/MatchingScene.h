#ifndef __MATCHING_SCENE_H__
#define __MATCHING_SCENE_H__

#include "MessageTypes.h"
#include "Server/MatchingState/MatchingState.h"

class MatchingScene : public ZSceneBase
{
public:
	virtual ~MatchingScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

private:
	ZVector<ZUUID> m_Clients;
	ZUnorderedMap<ZUUID, LoginData> m_PlayersLoginData;
	
	MatchingStateMachine m_StateMachine;

	float m_CountDownTimer;				// カウントダウン用
	float m_LastMatchingCountDownTimer; // ステート推移前のマッチングカウントダウン状態保持用

	ZMap<LoginData::PlayStyle, uint> m_PlayerCnt;
};

#endif