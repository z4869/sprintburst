#include "Server/ServerApp.h"

#include "MainSystems/GameWorld/GameWorld.h"


#include "ECSComponents/Common/CommonComponents.h"
#include "ECSSystems/Common/CommonSystems.h"

#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "ECSSystems/MapObject/MapObjectSystems.h"

#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSSystems/Character/CharacterSystems.h"

#include "ECSListeners/Common/CommonListeners.h"

#include "MainSystems/GameWorld/GameWorld.h"

#include "CommonDefines.h"

#include "MatchingScene.h"

void MatchingScene::Init()
{
	m_StateMachine.RegisterState("NoClient", Make_Shared(MatchingState_NoClient, sysnew));
	m_StateMachine.RegisterState("MatchingCountDown", Make_Shared(MatchingState_MatchingCountDown, sysnew));
	m_StateMachine.RegisterState("MatchStartCountDown", Make_Shared(MatchingState_MatchStartCountDown, sysnew));

	m_StateMachine.SetStartState("NoClient");

	m_CountDownTimer = MatchingTimeLimit;
	m_LastMatchingCountDownTimer = MatchingTimeLimit;
	
	SERVER.AddConnectCallBack("MatchingSceneCallBack",
		[this](const ZUUID& uuid)
	{
		ZString str = "OnConnect UUID:" + uuid.GetUUIDStr();
		DW_SCROLL(0, str);
		m_Clients.push_back(uuid);
	});

	SERVER.AddRecvCallBack("MatchingSceneCallBack",
		[this](const ZUUID& uuid,const NetWork::PacketHeader& header,void* data)
	{
		ZString str = "OnReceive UUID:" + uuid.GetUUIDStr();
		DW_SCROLL(0, str);
		ZString headerStr;
		header.ToString(headerStr);
		DW_SCROLL(1, headerStr);

		switch (header.MessageType)
		{
			case MessageTypes::Login:
			{
				ZString loginUser = "LoginUser : ";
				LoginData* loginData = (LoginData*)data;
				loginUser += loginData->Name;
				DW_SCROLL(0, loginUser);

				switch (loginData->Style)
				{
					case LoginData::Security:
					{
						if (m_StateMachine.GetPlayerCount(LoginData::Security) < MaxSecurityPlayer)
							break;
						
						// セキュリティ側希望者が上限の場合はエラーを送信
						LoginErrorData loginErrorData;
						strcpy(loginErrorData.ErrorMsg, "The maximum number of SecurityPlayers has reached the limit");
						SERVER.SendData(MessageTypes::LoginError, &loginErrorData, sizeof(LoginErrorData), 0, uuid);
						return;
					}
					break;

					case LoginData::Hacker:
					{
						if (m_StateMachine.GetPlayerCount(LoginData::Hacker) < MaxHackerPlayer)
							break;
						
						// ハッカー側希望者が上限の場合はエラーを送信
						LoginErrorData loginErrorData;
						strcpy(loginErrorData.ErrorMsg, "The maximum number of HackerPlayers has reached the limit");
						SERVER.SendData(MessageTypes::LoginError, &loginErrorData, sizeof(LoginErrorData), 0, uuid);
						return;
					}
					break;
				}
	
				uint playerCnt = m_StateMachine.GetPlayerCount(loginData->Style);
				m_StateMachine.SetPlayerCount(loginData->Style, ++playerCnt);
				m_StateMachine.OnLoginPlayer(uuid);

				// 全ユーザーにログイン情報通知
				{
					NotifyLoginData notifyLoginData;
					notifyLoginData.UserUUID = uuid.GetUUID();
					notifyLoginData.LoginData = *loginData;
					for (auto& player : m_PlayersLoginData)
						SERVER.SendData(MessageTypes::NotifyLogin, &notifyLoginData, sizeof(NotifyLoginData), 0, player.first);
				}

				// ログインしてきたユーザーに現在ログインしているプレイヤー情報を送信
				for (auto& player : m_PlayersLoginData)
				{
					NotifyLoginData notifyLoginData;
					notifyLoginData.UserUUID = player.first.GetUUID();
					notifyLoginData.LoginData = player.second;
					SERVER.SendData(MessageTypes::NotifyLogin, &notifyLoginData, sizeof(NotifyLoginData), 0, uuid);
				}

				m_PlayersLoginData[uuid] = *loginData;

			}
			break;
		}
	
	});

	SERVER.AddDisConnectCallBack("MatchingSceneCallBack",
		[this](const ZUUID& uuid)
	{
		ZString str = "OnDisConnect UUID:" + uuid.GetUUIDStr();
		DW_SCROLL(0, str);
		auto it = std::find(m_Clients.begin(), m_Clients.end(), uuid);
		if (it != m_Clients.end()) m_Clients.erase(it);
		
		if (m_PlayersLoginData.find(uuid) == m_PlayersLoginData.end())
			return;

		uint playerCnt = m_StateMachine.GetPlayerCount(m_PlayersLoginData[uuid].Style);
		m_StateMachine.SetPlayerCount(m_PlayersLoginData[uuid].Style, --playerCnt);
		
		m_StateMachine.OnNetWorkEvent(MessageTypes::Logout);

		m_PlayersLoginData.erase(uuid);
		
		// 切断されたらログアウト情報を全ユーザーに通知
		{
			NotifyLogoutData notifyLogoutData;
			notifyLogoutData.UserUUID = uuid.GetUUID();
			for (auto& player : m_PlayersLoginData)
				SERVER.SendData(MessageTypes::NotifyLogout, &notifyLogoutData, sizeof(NotifyLogoutData), 0, player.first);
		}
	});
}

void MatchingScene::Update()
{
	SERVER.Update();

	m_StateMachine.Update(APP.m_DeltaTime);
}

void MatchingScene::ImGuiUpdate()
{
	DW_STATIC(1, "Matching_Scene");
	if (m_Clients.empty())return;

	auto func = [this]()
	{
		if (ImGui::Begin("Connecting Players"))
		{
			for (auto& player : m_PlayersLoginData)
				ImGui::Text("%s : %s", player.second.Name, LoginData::GetPlayStyleName(player.second.Style));
		
			ImGui::Separator();
			
			m_StateMachine.ImGui();
		}
		ImGui::End();

	};
	DW_IMGUI_FUNC(func);

}

void MatchingScene::Draw()
{
	auto& pSr = ShMgr.GetRenderer<SpriteRenderer>();
	pSr.Begin(false, true);
	{
		// 中央にロゴ表示
		auto texInfo = GW.m_TexBackImg->GetInfo();
		auto& window = APP.m_Window;
		float y = (window->GetHeight() - texInfo.Height) / 2.0f;

		pSr.Draw2D(*GW.m_TexBackImg, 0, y, (float)window->GetWidth(), (float)texInfo.Height, &ZVec4(0.2f, 0.2f, 0.2f, 1.f));
	}
	pSr.End();
}

void MatchingScene::Release()
{
	// このシーンで追加したコールバック削除
	SERVER.RemoveAllCallBackFunction("MatchingSceneCallBacks");
}
