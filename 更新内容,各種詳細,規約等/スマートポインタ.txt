ZSP(shared_ptr),ZUP(unique_ptr),ZWP(weak_ptr)追加
基本的にstdのスマートポインタと同じ様に使える(ようにしたつもり)
1つ違う点は、ZMemoryAllocatorから確保したポインタのみ管理する
>>なので間違ってもnewで確保したポインタを管理させないように<<

追加マクロ
Make_Shared(型名,確保関数(sysnew or appnew),コンストラクタの引数...)
Make_Unique(型名,確保関数(sysnew or appnew),コンストラクタの引数...)
↑のマクロはただ単に, ~ = ZSP<hoge>(sysnew(hoge,1,2)) と書くのが面倒なのでコレの短縮用

他追加クラス
ZEnable_Shared_From_This(std::enable_shared_from_thisのパクリ)
本家同様ZSPに管理させているオブジェクトのメンバ関数でthisポインタからZSPを生成するのに使う