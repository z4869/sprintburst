#include "PCH/pch.h"
#include "../EzLib/DebugWindow/DebugWindow.h"

using namespace EzLib;

ZResourceStorage::ZResourceStorage()
{
	// LoadTexture関数時の、読み込み詳細設定
	m_LoadTextureInfo.ShaderResource = true;
	m_LoadTextureInfo.RenderTarget = false;
	m_LoadTextureInfo.DepthStencil = false;
	m_LoadTextureInfo.MipLevels = 0;

}


ZSP<ZTexture> ZResourceStorage::LoadTexture(const ZString& filename, bool forceLoad)
{
	// データ追加
	bool bCreate;
	ZSP<ZTexture> pAdd = AddData_Type<ZTexture>(filename, &bCreate);

	// 新規生成時は読み込み　又は 強制指示時はは読み込み
	if (bCreate || forceLoad)
	{
		if (pAdd->LoadTextureEx(filename, &m_LoadTextureInfo) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;
}

ZSP<ZGameModel> ZResourceStorage::LoadMesh(const ZString& filename, bool bakeCurve, bool forceLoad)
{
	// データ追加
	bool bCreate;
	ZSP<ZGameModel> pAdd = AddData_Type<ZGameModel>(filename, &bCreate);

	// 新規生成時は読み込み　又は 強制指示時はは読み込み
	if (bCreate || forceLoad)
	{
		// 読み込み
		if (pAdd->LoadMesh(filename, bakeCurve) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;

}

ZSP<ZSoundData> ZResourceStorage::LoadSound(const ZString& filename, bool forceLoad)
{
	// データ追加
	bool bCreate;
	ZSP<ZSoundData> pAdd = AddData_Type<ZSoundData>(filename, &bCreate);

	// 新規生成時は読み込み　又は 強制指示時はは読み込み
	if (bCreate || forceLoad)
	{
		if (pAdd->LoadWaveFile(filename) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;
}

ZSP<ZPixelShader> ZResourceStorage::LoadPixelShader(const ZString& filename)
{
	bool bCreate;
	ZSP<ZPixelShader> pAdd = AddData_Type<ZPixelShader>(filename, &bCreate);

	if (bCreate)
	{
		if (pAdd->LoadShader(filename) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;
}

ZSP<ZVertexShader> EzLib::ZResourceStorage::LoadVertexShader(const ZString& filename)
{
	bool bCreate;
	ZSP<ZVertexShader> pAdd = AddData_Type<ZVertexShader>(filename, &bCreate);

	if (bCreate)
	{
		if (pAdd->LoadShader(filename) == false)
		{
			// 失敗時は空を返す
			pAdd = nullptr;
			Optimize();
		}
	}

	return pAdd;
}
