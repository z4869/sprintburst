#ifndef __SCENE_H__
#define __SCENE_H__

// シーン基本クラス
class ZSceneBase
{
public:
	ZSceneBase()
	{
	}
	virtual ~ZSceneBase()
	{
	}

	virtual void Init() = 0;
	virtual void Start() {}		// 初期化完了後の最初のフレーム
	virtual void PreUpdate() {}	// 物理、コリジョンエンジン等の衝突判定実行前に呼ぶ
	virtual void Update() = 0;
	virtual void TickUpdate(){}		// 毎秒
	virtual void ImGuiUpdate() = 0;
	virtual void Draw() = 0;

public:
	// 更新システム
	ECSSystemList m_PreUpdateSystems;	// 衝突判定前
	ECSSystemList m_UpdateSystems;
	// 描画システム
	ECSSystemList m_DrawSystems;

	//
	ZVector<ZSP<ECSListener>> m_Listeners;

	bool m_IsStarted{false};
};

// シーンファクトリ
class ZSceneManager
{
public:
	ZSceneManager();

	// 初期化
	void Init();

	// 初期化後の初回フレームの更新処理
	// 戻り値 : 初回フレームの更新処理が通ったか
	bool Start();

	// 更新
	void PreUpdate();	// 物理、コリジョンエンジン等の衝突判定実行前に呼ぶ
	void Update();
	void TickUpdate();	// 毎秒

	// ImGui更新
	void ImGuiUpdate();

	// 描画
	void Draw();

	// 解放
	void Release();

	template<typename T>
	void SubmitSceneGenerater(const ZString& sceneName)
	{
		m_SceneGenerateMap[sceneName] =
			[]()
		{
			return Make_Shared(T, sysnew);
		};
	}

	// シーンを変更
	//  sceneName	... あらかじめInit()で登録しておいたシーン名
	//  ※即座には変更されず次のUpdateの前に変更が実行される
	void ChangeScene(const ZString& sceneName);

	// 即座にシーンを変更
	//  sceneName	... あらかじめInit()で登録しておいたシーン名
	void NowChangeScene(const ZString& sceneName);

	// 現在のシーンを取得
	ZSP<ZSceneBase> GetNowScene();

private:
	// 現在のシーン
	ZSP<ZSceneBase> m_NowScene;

	// シーン生成マップ
	ZSMap<ZString, std::function<ZSP<ZSceneBase>()>>	m_SceneGenerateMap;

	ZString m_NextChangeSceneName;

};


#endif