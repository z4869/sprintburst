#include "PCH/pch.h"

namespace EzLib
{
	ZSP<ZVertexShader> ZShaderSet::GetVS(const ZString& name)
	{
		try
		{
			auto vs = m_VSMap.at(name);
			return vs;
		}
		catch (std::out_of_range&)
		{
			return ZSP<ZVertexShader>();
		}

	}

	ZSP<ZPixelShader> ZShaderSet::GetPS(const ZString& name)
	{
		try
		{
			auto ps = m_PSMap.at(name);
			return ps;
		}
		catch (std::out_of_range&)
		{
			return ZSP<ZPixelShader>();
		}
	}

	void ZShaderSet::SetVS(const ZString& name,ZSP<ZVertexShader> vs)
	{
		// 同じ名前ならあとからセットしたものを優先
		m_VSMap[name] = vs;
	}

	void ZShaderSet::SetPS(const ZString& name,ZSP<ZPixelShader> ps)
	{
		// 同じ名前ならあとからセットしたものを優先
		m_PSMap[name] = ps;
	}

	void ZShaderSet::RemoveAllShaders()
	{
		m_VSMap.clear();
		m_PSMap.clear();
	}


}
