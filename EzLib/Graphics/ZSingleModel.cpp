#include "PCH/pch.h"
#include "ZSingleModel.h"

using namespace EzLib;

//=======================================================================
//
// ZSingleModel
//
//=======================================================================
bool ZSingleModel::LoadMesh(const ZString& filename)
{
	Release();

	// モデルファイルを読み込み、先頭のモデルのみを使用する
	ZSP<ZGameModel> model = ZDx.GetResStg()->LoadMesh(filename);

	if (model && model->GetModelTbl().size() >= 1)
	{
		// メッシュ
		m_pMesh = model->GetModelTbl()[0]->GetMesh();
		// マテリアル
		m_Materials = model->GetModelTbl()[0]->GetMaterials();
		// 名前
		m_Name = model->GetModelTbl()[0]->GetName();
	}

	return false;
}

void ZSingleModel::Release()
{
	m_pMesh = nullptr;
	m_Materials.clear();

	m_Name = "";
}

void ZSingleModel::SetDrawData()
{
	if (m_pMesh == nullptr)return;

	m_pMesh->SetDrawData();
}

EzLib::ZSingleModel::ZSingleModel()
{
}