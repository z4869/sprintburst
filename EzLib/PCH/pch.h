#ifdef _DEBUG
#pragma comment(linker, "/nodefaultlib:libcmt.lib")
#else
#ifndef NDEBUG
#define NDEBUG
#endif
#endif

#define _GLIBCXX_DEBUG

// デバッグウィンドウを表示する
//#define SHOW_DEBUG_WINDOW

#include "../EzLib.h"