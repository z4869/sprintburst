#ifndef __ZBULLET_DEBUG_DRAW_H__
#define __ZBULLET_DEBUG_DRAW_H__

namespace EzLib
{
	class ZBullet_DebugDraw : public btIDebugDraw
	{
	public:
		ZBullet_DebugDraw();
		virtual ~ZBullet_DebugDraw()
		{
		}
	
		// ライン描画
		virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& fromColor, const btVector3& toColor)override;
		virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color)override;
	
		//未対応
		virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
		{
		}
		
		virtual void reportErrorWarning(const char* warningString)
		{
		}
		
		virtual void draw3dText(const btVector3& location, const char* textString)
		{
		}
	
		virtual void setDebugMode(int debugMode)
		{
			m_DebugMode = debugMode;
		}
		
		virtual int getDebugMode() const
		{
			return m_DebugMode;
		}
	
		void SetSubmitLineFunction1(std::function<void(const ZVec3&, const ZVec3&, const ZVec4&, const ZVec4&)> func)
		{
			m_SubmitLineFunc1 = func;
		}

		void SetSubmitLineFunction2(std::function<void(const ZVec3&, const ZVec3&, const ZVec4&)> func)
		{
			m_SubmitLineFunc2 = func;
		}

	private:
		int m_DebugMode;
		std::function<void(const ZVec3&, const ZVec3&, const ZVec4&, const ZVec4&)> m_SubmitLineFunc1;
		std::function<void(const ZVec3&, const ZVec3&, const ZVec4&)> m_SubmitLineFunc2;

	};
}

#endif