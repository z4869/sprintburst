#include "MainFrame/ZMainFrame.h"

namespace EzLib
{
	ZBullet_DebugDraw::ZBullet_DebugDraw()
		: m_DebugMode(0)
	{
	}

	void ZBullet_DebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& fromColor, const btVector3& toColor)
	{
		if (!m_SubmitLineFunc1)return;

		ZVec3 p1(from.x(), from.y(), from.z());
		ZVec3 p2(to.x(), to.y(), to.z());
		ZVec4 p1Color(fromColor.x(), fromColor.y(), fromColor.z(), 1);
		ZVec4 p2Color(toColor.x(), toColor.y(), toColor.z(), 1);

		m_SubmitLineFunc1(p1, p2, p1Color, p2Color);
	}

	void ZBullet_DebugDraw::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
	{
		if (!m_SubmitLineFunc2)return;
		
		ZVec3 p1(from.x(), from.y(), from.z());
		ZVec3 p2(to.x(), to.y(), to.z());
		ZVec4 Color(color.x(), color.y(), color.z(), 1);

		m_SubmitLineFunc2(p1, p2,Color);
	}
}