#include "ZBulletPhysicsEngine.h"

namespace EzLib
{
	bool CreateJoint(ZPhysicsJoint_Base** out, ZBP_Joint& jointBp, btRigidBody* body1, btRigidBody* body2)
	{
		if (body1 == nullptr || body2 == nullptr)
			return false;
	
		// ジョイント格納用ポインターがnullでなければ破棄
		Safe_Delete(*out);
	
		btMatrix3x3 rotMat;
		rotMat.setEulerZYX(jointBp.Rotate.z, jointBp.Rotate.y, jointBp.Rotate.x);
	
		btTransform trans;
		trans.setIdentity();
		trans.setOrigin(btVector3(jointBp.Translate.x, jointBp.Translate.y, jointBp.Translate.z));
		trans.setBasis(rotMat);
	
		// ジョイントポイントをそれぞれの剛体の相対位置に変換
		btTransform inv1 = body1->getWorldTransform().inverse();
		btTransform inv2 = body2->getWorldTransform().inverse();
		inv1 = inv1 * trans;
		inv2 = inv2 * trans;
		ZMatrix mat1, mat2;
		inv1.getOpenGLMatrix(&mat1._11);
		inv2.getOpenGLMatrix(&mat2._11);
	
		jointBp.RotateLowerLimit = ToDegree(jointBp.RotateLowerLimit);
		jointBp.RotateUpperLimit = ToDegree(jointBp.RotateUpperLimit);
	
		switch (jointBp.Type)
		{
			case ZBP_Joint::JointType::SpringDoF6:
			{
				auto* joint = sysnew(ZPhysicsJoint_6DofSpring);
	
				joint->Create(body1, body2, mat1, mat2);
				
				joint->SetLinearLowerLimit(jointBp.TranslateLowerLimit);
				joint->SetLinearUpperLimit(jointBp.TranslateUpperLimit);
	
				joint->SetAngularLowerLimit(jointBp.RotateLowerLimit);
				joint->SetAngularUpperLimit(jointBp.RotateUpperLimit);
	
				for (int index = 0; index < 3; index++)
				{
					if (jointBp.SpringTranslateFactor[index] == 0)
						continue;
					joint->EnableSpring(index, true);
					joint->SetSpring_Stiffness(index, jointBp.SpringTranslateFactor[index]);
				}
	
				for (int index = 0; index < 3; index++)
				{
					if (jointBp.SpringRotateFactor[index] == 0)
						continue;
					joint->EnableSpring(index, true);
					joint->SetSpring_Stiffness(index + 3, jointBp.SpringRotateFactor[index]);
				}
	
				*out = joint;
			}
			break;
	
			case ZBP_Joint::JointType::DoF6:
			{
				auto* joint = sysnew(ZPhysicsJoint_6Dof);
	
				joint->Create(body1, body2, mat1, mat2);
	
				joint->SetLinearLowerLimit(jointBp.TranslateLowerLimit);
				joint->SetLinearUpperLimit(jointBp.TranslateUpperLimit);
	
				joint->SetAngularLowerLimit(jointBp.RotateLowerLimit);
				joint->SetAngularUpperLimit(jointBp.RotateUpperLimit);
	
				*out = joint;
			}
			break;
	
			case ZBP_Joint::JointType::P2P:
			{
				auto* joint = sysnew(ZPhysicsJoint_Point);
	
				joint->Create(body1, body2, mat1.GetPos(), mat2.GetPos());
	
				*out = joint;
			}
			break;
	
			case ZBP_Joint::JointType::ConeTwist:
			{
				// 未実装
				return false;
			}
			break;
	
			case ZBP_Joint::JointType::Slider:
			{
				// 未実装
				return false;
			}
			break;
	
			case ZBP_Joint::JointType::Hinge:
			{
				auto* joint = sysnew(ZPhysicsJoint_Hinge);
	
				btVector3 axis1(jointBp.HingeAxisA.x, jointBp.HingeAxisA.y, jointBp.HingeAxisA.z);
				btVector3 axis2(jointBp.HingeAxisB.x, jointBp.HingeAxisB.y, jointBp.HingeAxisB.z);
	
				joint->Create(body1, body2, inv1.getOrigin(), inv2.getOrigin(), axis1, axis2);
	
				*out = joint;
			}
			break;
	
			default:
				return false;
		}
	
		return true;
	}
}