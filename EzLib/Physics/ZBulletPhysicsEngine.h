//===============================================================
//   「Bullet Physics Engine」関係のクラス
// 
//===============================================================
#ifndef ZBulletPhysicsEngine_h
#define ZBulletPhysicsEngine_h

#pragma warning(disable:4316)
#define BT_NO_SIMD_OPERATOR_OVERLOADS

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include "BulletCollision/CollisionShapes/btShapeHull.h"
#include "LinearMath/btIDebugDraw.h"

namespace EzLib
{

	class ZPhysicsRigidBody;
	class ZPhysicsJoint_Base;
	class ZBullet_DebugDraw;


	// ZPhysicsWorld				… 物理世界クラス

	// ZPhysicsRigidBody			… 物理剛体クラス

	// ZPhysicsShape_Box			… 立方体形状
	// ZPhysicsShape_Sphere			… 球形状
	// ZPhysicsShape_Capsule		… カプセル形状
	// ZPhysicsShape_Mesh			… メッシュ形状
	// ZPhysicsShape_Compound		… 複合形状

	// ZPhysicsJoint_Point			… ボールジョイント
	// ZPhysicsJoint_Hinge			… ヒンジジョイント
	// ZPhysicsJoint_6Dof			… 6Dofジョイント
	// ZPhysicsJoint_6DofSpring		… バネ付き6Dofジョイント


	//=====================================================
	//   「Bullet Physics Engine」ワールド用クラス
	// 
	//  @ingroup Physics
	//=====================================================
	class ZPhysicsWorld
	{
	public:
		//   物理ワールド取得
		btDynamicsWorld* GetWorld() { return m_dynamicsWorld; }

		ZUP<ZBullet_DebugDraw>& GetBulletDebugDraw() { return m_debugDraw; }

		//   初期化
		void Init();

		//   解放
		void Release();

		//   シミュレーション進行
		int StepSimulation(btScalar timeStep, int MaxSubSteps = 10, btScalar fixedTimeStep = btScalar(1.) / btScalar(120.));

		//   剛体を物理ワールドへ追加。ZPhysicsRigidBody::AddToWorld()でも可能。
		bool AddRigidBody(ZPhysicsRigidBody& rb, short group = 1, short mask = -1);

		//   ジョイントを物理ワールドへ追加。
		//  	disableCollisionsBetweenLinkedBodies	… ジョイントで接続した剛体同士の衝突を行わないようにする？
		bool AddJoint(const ZSP<ZPhysicsJoint_Base>& joint, bool disableCollisionsBetweenLinkedBodies = false);

		//   衝突検知用コールバック通知用
		// 
		//  剛体同士が衝突するとこの関数が実行される。				\n
		//  このクラスを継承し、この関数をオーバーライドして使う。	\n
		// 
		//  	cp		… 衝突の情報が入ってるぽい
		//  	body0	… 衝突した剛体その１
		//  	body1	… 衝突した剛体その２
		//	virtual bool ContactProcessedCallback(btManifoldPoint& cp, ZPhysicsRigidBody* body0, ZPhysicsRigidBody* body1){ return true; }

		//   初期化されてる？
		bool IsInit() { return (m_dynamicsWorld != nullptr); }

		// デバック描画モード設定
		void SetDebugDrawMode(int drawMode = btIDebugDraw::DBG_DrawWireframe);
		
		// デバック描画が有効か
		bool IsEnableDebugDraw();

		// デバック描画無効化
		void DisableDebugDraw();

		// デバッグ描画
		void DebugDraw();


		//   衝突検知コールバック通知用関数オブジェクト
		// 
		//  剛体同士が衝突するとこの関数が実行される。				\n
		//  このクラスを継承し、この関数をオーバーライドして使う。	\n
		//  引数1 … 衝突の情報が入ってるぽい	\n
		//  引数2 … 衝突した剛体その１			\n
		//  引数3 … 衝突した剛体その２			\n
		//  例) world.m_ContactProcessedCallback = [](btManifoldPoint& cp, ZPhysicsRigidBody* body0, ZPhysicsRigidBody* body1){return true;};
		std::function<bool(btManifoldPoint&, ZPhysicsRigidBody*, ZPhysicsRigidBody*)>	m_ContactProcessedCallback;

		// 
		ZPhysicsWorld()
		{
			m_collisionConfiguration = nullptr;
			m_dispatcher = nullptr;
			m_broadphase = nullptr;
			m_solver = nullptr;
			m_dynamicsWorld = nullptr;
		}
		~ZPhysicsWorld()
		{
			Release();
		}


	private:
		btDefaultCollisionConfiguration*	m_collisionConfiguration;
		btCollisionDispatcher*				m_dispatcher;
		btBroadphaseInterface*				m_broadphase;
		btConstraintSolver*					m_solver;
		btDynamicsWorld*					m_dynamicsWorld;
		ZUP<ZBullet_DebugDraw>				m_debugDraw;

	protected:
		// 衝突検知用静的コールバック関数
		static bool s_ContactProcessedCallback(btManifoldPoint& cp, void* body0, void* body1);

	private:
		// コピー禁止用
		ZPhysicsWorld(const ZPhysicsWorld& src) {}
		void operator=(const ZPhysicsWorld& src) {}
	};

	//==========================================================================
	//   Bulletオブジェクト基本クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsObj_Base : public ZEnable_Shared_From_This<ZPhysicsObj_Base>
	{
	public:
		ZPhysicsWorld* GetWorld() { return m_World; }
		void SetWorld(ZPhysicsWorld* world) { m_World = world; }

		virtual void Release() = 0;

		// ユーザポインタ
		void SetUserPointer(void* p) { m_lpUserPointer = p; }
		void* GetUserPointer() { return m_lpUserPointer; }

		// 
		ZPhysicsObj_Base() :
			m_World(nullptr),
			m_lpUserPointer(nullptr)
		{
		}

		// 
		virtual ~ZPhysicsObj_Base()
		{
			m_World = nullptr;
		}

	protected:

		ZPhysicsWorld* m_World;		// 登録しているワールド
		void*			m_lpUserPointer;

	};

}

#include "ZBullet_ObjectBlueprint.h"
#include "ZBullet_Shape.h"
#include "ZBullet_RigidBody.h"
#include "ZBullet_Joint.h"
#include "ZBullet_DebugDraw.h"

#endif
