inline const ZSVector<uint32>& ECSListener::GetComponentIDs()
{
	return m_ComponentIDs;
}

inline const ComponentBitSet& ECSListener::GetComponentBitSet()
{
	return m_CompBitSet;
}

inline bool ECSListener::NotifyOnAllComponentOperations()
{
	return m_IsNotifyOnAllComponentOperations;
}

inline bool ECSListener::NotifyOnAllEntityOperations()
{
	return m_IsNotifyOnAllEntityOperations;
}

inline void ECSListener::SetNotificationSettings(bool notifyOnAllComponentOperations,
												bool notifyOnAllEntityOperations)
{
	m_IsNotifyOnAllComponentOperations = notifyOnAllComponentOperations;
	m_IsNotifyOnAllEntityOperations = notifyOnAllEntityOperations;
}

inline void ECSListener::AddComponentID(uint32 id)
{
	m_ComponentIDs.push_back(id);
	m_CompBitSet.set(id);
}