#ifndef __ECS_LISTENER_H__
#define __ECS_LISTENER_H__

#include "PCH/pch.h"
#include "ECSCommon.h"

namespace EzLib
{
namespace ZECS
{
	// m_ComponentIDsに含まれるいずれかのComponentをもつEntityが生成、削除された時や
	// m_ComponentIDsに含まれるいずれかのComponentをEntityに追加、除外した際に
	// このECSListenerクラスの各コールバック関数が呼ばれる
	class ECSListener
	{
	public:
		ECSListener();
		virtual ~ECSListener();

		// コールバック関数
		virtual void OnMakeEntity(EntityHandle handle);						// Entity作成時に呼ばれる
		virtual void OnMadeEntityFromJson(EntityHandle handle);				// JsonでのEntity作成後に呼ばれる
		virtual void OnRemoveEntity(EntityHandle hadle);					// Entity削除時に呼ばれる
		virtual void OnAddComponent(EntityHandle handle, uint32 id);		// EntityにComponent追加した際に呼ばれる
		virtual void OnRemoveComponent(EntityHandle handle, uint32 id);		// EntityからComponent削除時に呼ばれる

		const ZSVector<uint32>& GetComponentIDs();
		const ComponentBitSet& GetComponentBitSet();

		bool NotifyOnAllComponentOperations();
		bool NotifyOnAllEntityOperations();

	protected:
		void SetNotificationSettings(bool notifyOnAllComponentOperations,bool notifyOnAllEntityOperations);
		void AddComponentID(uint32 id);

	private:
		ZSVector<uint32>  m_ComponentIDs;
		ComponentBitSet m_CompBitSet;
		
		// 無条件ですべてのコンポーネント操作時に↑のコールバック関数に通知するか(falseの場合通知を受けたいコンポーネント操作時のみ通知)
		bool m_IsNotifyOnAllComponentOperations;
		// 無条件ですべてのエンティティ操作時に↑のコールバック関数に通知するか(falseの場合通知を受けたいコンポーネントを含むエンティティ操作時のみ通知)
		bool m_IsNotifyOnAllEntityOperations;

	};

}
}

namespace EzLib
{
namespace ZECS
{
#include "ECSListener.inl"
}
}

#endif
