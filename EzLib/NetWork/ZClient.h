#ifndef __ZCLIENT_H__
#define __ZCLIENT_H__

namespace EzLib
{
namespace NetWork
{
	class ZClient
	{
	public:
		// コールバック関数の型
		using ConnectionCallBackFunc = std::function<void()>; // 接続 or 切断時
		using RecvCallBackFunc = std::function<void(const PacketHeader&, void*)>; // 受信時
		using ConnectResultCallBackFunc = std::function<void(bool)>; // 接続試行結果通知用
		static const uint TimeOut;

	public:
		ZClient();
		~ZClient();

		// 初期化
		bool Init();
		// 更新
		void Update();
		// 解放
		void Release();
		// 接続情報リセット
		void ResetConnection();
		// 接続
		void Connect(const char* _address);
		// 接続(別スレッドで処理)
		void Connect(const char* _address, ConnectResultCallBackFunc _resultFunc);
		// 切断
		void DisConnect();
		// データ送信
		// クライアント側はサーバーと1対1での通信を想定しているのでブロードキャストは使わない
		void SendData(char _msgType,void* _data, int _size, int _channel);

		// データ受信
		template <typename T>
		void RecvData(std::deque<T>& _DequeList, int _channel)
		{
			_DequeList.clear();
			for (auto data : m_Channel[_channel])
			{
				T tmpData = *(T *)data; // データコピー
				_DequeList.push_back(tmpData);
			}
		}

		bool IsConnected();

		// 各種コールバック関数追加用
		void AddConnectCallBack(const std::string& name, ConnectionCallBackFunc func);
		void AddRecvCallBack(const std::string& name, RecvCallBackFunc func);
		void AddDisConnectCallBack(const std::string& name, ConnectionCallBackFunc func);

		// 追加されたコールバック関数をすべて削除
		void RemoveAllCallBackFunction();

		// 指定のキー値で登録されたコールバックをすべて削除
		void RemoveAllCallBackFunction(const std::string& funcName);

		// 各種特定のキー値で追加されたコールバック関数削除
		void RemoveConnectCallBack(const std::string& funcName);
		void RemoveRecvCallBack(const std::string& funcName);
		void RemoveDisConnectCallBack(const std::string& funcName);

	private:
		ENetHost* m_Host;
		ENetAddress m_Address;
		ENetPeer* m_Peer;
		ENetEvent m_Event;
		
		std::vector<std::deque<enet_uint8 *>> m_Channel; // 受信データバッファ

		bool m_IsConnected;		// 接続済みか
		size_t m_SendCnt;		// sessionIDとして送信 なにか送信する毎にインクリメント
		size_t m_LastRecvID;	// 最後に受信したパケットのsessionID

		// 各種コールバック
		std::unordered_map<std::string, ConnectionCallBackFunc> m_OnConnectCallBackFuncs;
		std::unordered_map<std::string, RecvCallBackFunc> m_OnRecvCallBackFuncs;
		std::unordered_map<std::string, ConnectionCallBackFunc> m_OnDisConnectCallBackFuncs;
	};

}
}

#endif