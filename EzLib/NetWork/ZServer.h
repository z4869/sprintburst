#ifndef __ZSERVER_H__
#define __ZSERVER_H__

namespace EzLib
{
namespace NetWork
{
	class ZServer
	{
	public:
		// コールバック関数の型
		using ConnectionCallBackFunc = std::function<void(const ZUUID&)>; // 接続 or 切断時
		using RecvCallBackFunc = std::function<void(const ZUUID&, const PacketHeader&, void*)>; // 受信時
	
	public:
		// 接続中クライアント管理用
		struct ServerClient
		{
		public:
			ServerClient(ENetPeer* peer);
			~ServerClient();
			void PushSendData(enet_uint8* sendData, size_t dataLength, uint channel);

		public:
			struct SendData
			{
				int Length;
				enet_uint8* Data;
			};

			std::array<std::deque<SendData>, CHANNEL_SIZE> m_SendDataBuffers;
			ENetPeer* m_pPeer;
			ZUUID m_UUID;
			size_t m_SessionID;
			size_t m_LastRecvID;
		};

	public:
		ZServer();
		~ZServer();

		// 初期化
		bool Init();
		// 更新
		void Update();
		// 解放
		void Release();
		// データ送信
		void SendData(char _msgType, void* _data, int _size, int _channel,const ZUUID& clientUUID);
		void SendData(char _msgType, void* _data, int _size, int _channel); // broadcast

		// クライアントに送信バッファ内のデータを再送信する
		void ReSendDataFromBuffer(const ZUUID& clientUUID,int _channel);
		void ReSendDataFromBuffer(int _channel); // 全クライアント

		void DisConnect(const ZUUID& clientUUID);
		void DisConnectAll();

		// UUID一覧取得(文字列)
		void GetUUIDList(std::vector<std::string>& _vectorUUID);

		// UUID一覧取得(構造体)
		void GetUUIDList(std::vector<ZUUID>& _vectorUUID);

		// 各種コールバック関数追加用
		void AddConnectCallBack(const std::string& name,ConnectionCallBackFunc func);
		void AddRecvCallBack(const std::string& name, RecvCallBackFunc func);
		void AddDisConnectCallBack(const std::string& name, ConnectionCallBackFunc func);

		// 追加されたコールバック関数をすべて削除
		void RemoveAllCallBackFunction();

		// 指定のキー値で登録されたコールバックをすべて削除
		void RemoveAllCallBackFunction(const std::string& funcName);

		// 各種特定のキー値で追加されたコールバック関数削除
		void RemoveConnectCallBack(const std::string& funcName);
		void RemoveRecvCallBack(const std::string& funcName);
		void RemoveDisConnectCallBack(const std::string& funcName);

	private:
		void DeleteClient(const ZUUID& uuid);

	private:
		ENetHost* m_Host;		//ホスト
		ENetAddress m_Address;	//アドレス
		ENetEvent m_Event;		//イベント
		
		std::unordered_map<ZUUID, ServerClient*> m_ServerClients;

		// 各種コールバック格納用
		std::unordered_map<std::string, ConnectionCallBackFunc> m_OnConnectCallBackFuncs;
		std::unordered_map<std::string, RecvCallBackFunc> m_OnRecvCallBackFuncs;
		std::unordered_map<std::string, ConnectionCallBackFunc> m_OnDisConnectCallBackFuncs;
	};
}
}

#endif