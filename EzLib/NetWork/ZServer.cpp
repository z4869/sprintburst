#include "PCH/pch.h"

namespace EzLib
{
namespace NetWork
{
	ZServer::ServerClient::ServerClient(ENetPeer* peer)
		: m_pPeer(peer),m_UUID(), m_SessionID(0), m_LastRecvID(0)
	{
		m_pPeer->data = &m_UUID;
	}

	ZServer::ServerClient::~ServerClient()
	{
		m_pPeer->data = nullptr;
		// バッファメモリ解放
		for (auto buffer : m_SendDataBuffers)
		{
			for (auto data : buffer)
				free(data.Data);
			buffer.clear();
		}

	}

	void ZServer::ServerClient::PushSendData(enet_uint8* sendData, size_t dataLength, uint channel)
	{
		assert(sendData);
		// 送信データをコピー
		SendData data;
		data.Data = (enet_uint8*)malloc(dataLength);
		memcpy(data.Data, sendData, dataLength);
		data.Length = dataLength;

		auto& buffer = m_SendDataBuffers[channel];
		if (buffer.size() > BUFF_SIZE)
		{
			auto d = buffer.front();
			buffer.pop_front();
			free(d.Data);
		}
		buffer.push_back(data);
	}

	ZServer::ZServer()
	{
		m_Host = nullptr;
	}
	
	ZServer::~ZServer()
	{
		Release();
	}

	bool ZServer::Init()
	{
		std::string errMsg; // エラーメッセージ用

		if (enet_initialize() != 0)
		{
			errMsg = "An error occurred while initializing ENet.";
			goto INIT_ERROR;
		}
		atexit(enet_deinitialize);

		m_Address.host = ENET_HOST_ANY;
		m_Address.port = PORT_NO;
		m_Host = enet_host_create
		(
			&m_Address	,	/*バインドaddress*/
			8			,	/*最大クライアント数 余裕を持って8*/
			CHANNEL_SIZE,	/*チャンネル数(0 〜 n-1)*/
			0			,	/*受信帯域幅*/
			0				/*発信帯域幅*/
		);

		if (m_Host == NULL)
		{
			//エラー処理
			errMsg = "An error occurred while trying to create an ENet server host.";
			goto INIT_ERROR;
		}

		return true;

	INIT_ERROR:
		MessageBox(NULL, errMsg.c_str(), "ENet Error", MB_OK);
		return false;

	}

	void ZServer::Update()
	{
		while (enet_host_service(m_Host, &m_Event, 0) > 0)
		{
			switch (m_Event.type)
			{
				case ENET_EVENT_TYPE_CONNECT:
				{
					DebugLog_Line("new client %d:%u.data:%d",
								  m_Event.peer->address.host,
								  m_Event.peer->address.port,
								  m_Event.data);

					ServerClient* sc = new ServerClient(m_Event.peer);
					m_ServerClients[sc->m_UUID] = sc;
					ZUUID& uuid = sc->m_UUID;
					// pingを送信するインターバルをセット
					enet_peer_ping_interval(m_Event.peer, ENET_PEER_PING_INTERVAL);
					// ~~接続時のコールバック~~
					if(m_OnConnectCallBackFuncs.empty() == false)
					{
						//UUIDを取得
						for (auto& func : m_OnConnectCallBackFuncs)
							func.second(uuid);
					}
				}
				break;

				case ENET_EVENT_TYPE_RECEIVE:
				{
					//UUIDを取得
					ZUUID& client_uuid = *((ZUUID*)m_Event.peer->data);
					
					//printf("%s\n", strUUID.c_str());
					DebugLog_Line("%s", client_uuid.GetUUIDStr().c_str());

					PacketHeader header = *((PacketHeader*)m_Event.packet->data);
					m_ServerClients[client_uuid]->m_LastRecvID = header.LastRecvSessionID;

					// ~~データ受信時のコールバック~~
					if (m_OnRecvCallBackFuncs.empty() == false)
					{
						void* data = (m_Event.packet->data + sizeof(PacketHeader));
						for (auto& func : m_OnRecvCallBackFuncs)
							func.second(client_uuid, header,data);
					}

					/*パケットをクリーンアップ*/
					enet_packet_destroy(m_Event.packet);
				}
				break;

				case ENET_EVENT_TYPE_DISCONNECT:
				{
					const ZUUID& client_uuid = *((ZUUID*)m_Event.peer->data);
					DebugLog_Line("%s disconnected.", client_uuid.GetUUIDStr().c_str());

					// ~~切断時のコールバック~~
					if (m_OnDisConnectCallBackFuncs.empty() == false)
					{
						//UUIDを取得
						for (auto& func : m_OnDisConnectCallBackFuncs)
							func.second(client_uuid);
					}
					
					// クライアント削除
					DeleteClient(client_uuid);

					DW_SCROLL_CREAR(2);
				}
				break;

			} // switch (m_Event.type)

		} // while (enet_host_service(m_Host, &m_Event, 0) > 0)
	}

	void ZServer::Release()
	{
		DisConnectAll();
		
		if(m_Host != nullptr)
		{
			enet_host_destroy(m_Host);
			m_Host = nullptr;
		}

		RemoveAllCallBackFunction();
	}

	void ZServer::SendData(char _msgType, void* _data, int _size, int _channel,const ZUUID& clientUUID)
	{
		auto& client = m_ServerClients[clientUUID];
		auto peer = m_ServerClients[clientUUID]->m_pPeer;
		if (peer == nullptr)return; // 接続されていない or 不明なクライアントのUUID

		// 送信パケット生成
		ENetPacket* packet = CreatePacket(_msgType, client->m_SessionID++, client->m_LastRecvID, _data, _size); // sessionID未実装
		// 送信バッファに送信データ追加
		client->PushSendData(packet->data,_size + sizeof(PacketHeader),_channel);
		// 送信
		enet_peer_send(peer, _channel, packet);
	}

	void ZServer::SendData(char _msgType, void* _data, int _size, int _channel)
	{
		if (m_ServerClients.empty()) return;

		// 現在接続されている全クライアントにデータ送信
		for (auto& client : m_ServerClients)
			SendData(_msgType,_data,_size,_channel,client.first);
	}

	void ZServer::ReSendDataFromBuffer(const ZUUID& clientUUID, int _channel)
	{
		auto* client = m_ServerClients[clientUUID];
		for (auto& data : client->m_SendDataBuffers[_channel])
		{
			ENetPacket* packet = enet_packet_create(data.Data, data.Length, ENET_PACKET_FLAG_RELIABLE);
			enet_peer_send(client->m_pPeer, _channel, packet);
		}
	}

	void ZServer::ReSendDataFromBuffer(int _channel)
	{
		for (auto& client : m_ServerClients)
			ReSendDataFromBuffer(client.first, _channel);
	}

	void ZServer::DisConnect(const ZUUID& clientUUID)
	{
		if (m_ServerClients.find(clientUUID) == m_ServerClients.end())
			return; // 接続されていないクライアント

		auto* client = m_ServerClients[clientUUID];
		auto* peer = client->m_pPeer;
		enet_peer_disconnect(peer, 0);
		while (enet_host_service(m_Host, &m_Event, 3000) > 0)
		{
			switch (m_Event.type)
			{
				case ENET_EVENT_TYPE_RECEIVE:
					enet_packet_destroy(m_Event.packet);
				break;

				case ENET_EVENT_TYPE_DISCONNECT:
					DebugLog_Line("Disconnection succeeded.");
					DeleteClient(clientUUID);
				return;
			}
		}

		// 切断失敗
		// 強制切断
		enet_peer_reset(peer);
		DeleteClient(clientUUID);
	}

	void ZServer::DisConnectAll()
	{
		auto it = m_ServerClients.begin();

		for (; it != m_ServerClients.end();)
		{
			auto* peer = (*it).second->m_pPeer;
			enet_peer_disconnect(peer, 0);
			while (enet_host_service(m_Host, &m_Event, 3000) > 0)
			{
				switch (m_Event.type)
				{
					case ENET_EVENT_TYPE_RECEIVE:
						enet_packet_destroy(m_Event.packet);
					break;

					case ENET_EVENT_TYPE_DISCONNECT:
						DebugLog_Line("Disconnection succeeded.");
						delete (*it).second;
						(*it).second = nullptr;
					break;
				}
			}

			// 切断失敗
			// 強制切断
			if((*it).second != nullptr)
			{
				enet_peer_reset(peer);
				delete (*it).second;
			}
			it = m_ServerClients.erase(it);
		}

	}

	void ZServer::GetUUIDList(std::vector<std::string>& _vectorUUID)
	{
		_vectorUUID.clear();
		for (auto& client : m_ServerClients)
			_vectorUUID.push_back(client.first.GetUUIDStr().c_str());
	}
	
	void ZServer::GetUUIDList(std::vector<ZUUID>& _vectorUUID)
	{
		_vectorUUID.clear();
		for (auto& client : m_ServerClients)
			_vectorUUID.push_back(client.first);
	}

	void ZServer::AddConnectCallBack(const std::string& name, ConnectionCallBackFunc func)
	{
		auto it = m_OnConnectCallBackFuncs.find(name);
		if (it != m_OnConnectCallBackFuncs.end())
			return;
		m_OnConnectCallBackFuncs[name] = func;
	}

	void ZServer::AddRecvCallBack(const std::string & name, RecvCallBackFunc func)
	{
		auto it = m_OnRecvCallBackFuncs.find(name);
		if (it != m_OnRecvCallBackFuncs.end())
			return;
		m_OnRecvCallBackFuncs[name] = func;
	}

	void ZServer::AddDisConnectCallBack(const std::string& name, ConnectionCallBackFunc func)
	{
		auto it = m_OnDisConnectCallBackFuncs.find(name);
		if (it != m_OnDisConnectCallBackFuncs.end())
			return;
		m_OnDisConnectCallBackFuncs[name] = func;
	}

	void ZServer::RemoveAllCallBackFunction()
	{
		m_OnConnectCallBackFuncs.clear();
		m_OnRecvCallBackFuncs.clear();
		m_OnDisConnectCallBackFuncs.clear();
	}

	void ZServer::RemoveAllCallBackFunction(const std::string& funcName)
	{
		RemoveConnectCallBack(funcName);
		RemoveRecvCallBack(funcName);
		RemoveDisConnectCallBack(funcName);
	}

	void ZServer::RemoveConnectCallBack(const std::string & funcName)
	{
		auto it = m_OnConnectCallBackFuncs.find(funcName);
		if (it != m_OnConnectCallBackFuncs.end())m_OnConnectCallBackFuncs.erase(it);
	}

	void ZServer::RemoveRecvCallBack(const std::string & funcName)
	{
		auto it = m_OnRecvCallBackFuncs.find(funcName);
		if (it != m_OnRecvCallBackFuncs.end())m_OnRecvCallBackFuncs.erase(it);
	}

	void ZServer::RemoveDisConnectCallBack(const std::string & funcName)
	{
		auto it = m_OnDisConnectCallBackFuncs.find(funcName);
		if (it != m_OnDisConnectCallBackFuncs.end())m_OnDisConnectCallBackFuncs.erase(it);
	}

	void ZServer::DeleteClient(const ZUUID & uuid)
	{
		auto& it = m_ServerClients.find(uuid);
		if (it == m_ServerClients.end())
			return;

		delete it->second;
		m_ServerClients.erase(it);
	}

}
}
