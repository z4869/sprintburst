#include "PCH/pch.h"

namespace EzLib
{
namespace NetWork
{
	ENetPacket* CreatePacket(char _msgType, size_t _sessionID, size_t _lastRecvSessionID, const void* _data, int _size, uint _packetFlag)
	{
		size_t dataSize;
		void*p;
		PacketHeader* header;
		ENetPacket* packet;

		// ヘッダーのみ送信する場合を想定して一応maxマクロ使用
		dataSize = max(_size,0) + sizeof(PacketHeader);
		p = malloc(dataSize);
		header = new(p) PacketHeader();
		header->MessageType = _msgType;
		header->SessionID = _sessionID;
		header->LastRecvSessionID = _lastRecvSessionID;

		if(_data != nullptr && _size > 0)
			memcpy((((uint8*)p) + sizeof(PacketHeader)), _data, _size);

		packet = enet_packet_create(p, dataSize, _packetFlag);
		free(p);
		return packet;
	}

}
}