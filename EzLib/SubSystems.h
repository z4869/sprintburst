#ifndef __SUBSYSTEMS_H__
#define __SUBSYSTEMS_H__

#define RAND GetSubSystem<ZRand>()
#define ZDx GetSubSystem<ZDirectXGraphics>()
#define PHYSYS GetSubSystem<ZPhysicsWorld>()
#define THPOOL GetSubSystem<ZThreadPool>()

#endif