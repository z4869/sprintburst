#ifndef __STRING_HELPER_H__
#define	__STRING_HELPER_H__

namespace EzLib
{
	ZVector<ZString> SplitString(const ZString& string, const ZString& delimiters);
	ZVector<ZString> SplitString(const ZString& string, const char delimiters);
	ZVector<ZString> Tokenize(const ZString& string);
	ZVector<ZString> GetLines(const ZString& string);
	const char* FindToken(const char* str, const ZString& token);
	const char* FindToken(const ZString& string, const ZString& token);
	int32 FindStringPosition(const ZString& string, const ZString& search, uint offset = 0);
	ZString StringRange(const ZString& string, uint start, uint length);
	ZString RemoveStringRange(const ZString& string, uint start, uint length);
	ZString GetBlock(const char* str, const char** outPosition = nullptr);
	ZString GetBlock(const ZString& string, uint offset = 0);
	ZString GetStatement(const char* str, const char** outPosition = nullptr);
	bool StringContains(const ZString& string, const ZString& chars);
	bool StartsWith(const ZString& string, const ZString& chars);
	int32 NextInt(const ZString& string);

#include "StringHelper.inl"

}
#endif