#include "ZWindow.h"
inline uint32 ZWindow::GetWidth()const
{
	return m_Properties.Width;
}

inline uint32 ZWindow::GetHeight()const
{
	return m_Properties.Height;
}

inline HWND ZWindow::GetWindowHandle()const
{
	return m_WndHandle;
}

inline void ZWindow::EnableWindowActive()
{
	m_IsWindowActive = true;
}

inline void ZWindow::DisableWindowActive()
{
	m_IsWindowActive = false;
	INPUT.ClearAllKeyState();
}

inline bool ZWindow::IsWindowActive() const
{
	return m_IsWindowActive;
}

inline bool ZWindow::IsInitialized() const
{
	return m_Initialized;
}
