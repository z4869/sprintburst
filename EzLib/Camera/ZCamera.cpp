#include "MainFrame/ZMainFrame.h"

using namespace EzLib;

ZCamera ZCamera::LastCam;			// 最後に使用されたカメラ情報のコピーが入る(ZCameraクラスにより操作)

ZCamera::ZCamera()
{
	// 射影行列
	m_mProj.CreatePerspectiveFovLH(	m_Angle,			// 視野角
									m_Aspect,			// 画面のアスペクト比
									m_Near,			// 最近接距離
									m_Far);			// 最遠方距離
	ZCamera::LastCam.m_mProj = m_mProj;

	m_mOrtho.CreateOrthoLH(1280, 720, 0.01f, 100.f);
	ZCamera::LastCam.m_mProj = m_mOrtho;
}

void ZCamera::CameraToView()
{
	// カメラ行列の逆行列を作成
	m_mCam.Inverse(m_mView);

	ZCamera::LastCam.m_mCam		= m_mCam;
	ZCamera::LastCam.m_mView	= m_mView;
}

void ZCamera::ViewToCamera()
{
	// ビュー行列の逆行列を作成
	m_mView.Inverse(m_mCam);

	ZCamera::LastCam.m_mCam = m_mCam;
	ZCamera::LastCam.m_mView = m_mView;
}

void ZCamera::SetView(const ZMatrix &lpmView)
{
	m_mView = lpmView;

	// 逆にカメラ行列を作成
	m_mView.Inverse(m_mCam);

	ZCamera::LastCam.m_mCam = m_mCam;
	ZCamera::LastCam.m_mView = m_mView;
}

void ZCamera::SetPerspectiveFovLH(float ViewAngle, float m_Aspect, float zNear, float zFar)
{
	m_mProj.CreatePerspectiveFovLH(	ViewAngle,	// 視野角
									m_Aspect,		// 画面のアスペクト比
									zNear,		// 最近接距離
									zFar);		// 最遠方距離
	
	ZCamera::LastCam.m_mProj	= m_mProj;
	this->m_Near		= zNear;
	this->m_Far		= zFar;
	this->m_Aspect	= m_Aspect;
	this->m_Angle		= ViewAngle;
}

void ZCamera::SetOrthoLH(float w, float h, float zNear, float zFar)
{
	m_mProj.CreateOrthoLH(w, h, zNear, zFar);
	ZCamera::LastCam.m_mOrtho	= m_mProj;
	m_Near			= zNear;
	m_Far				= zFar;
	this->m_Aspect	= -1;
}

void ZCamera::SetProj(const ZMatrix& lpmProj)
{
	m_mProj = lpmProj;
	ZCamera::LastCam.m_mProj = m_mProj;
}

void ZCamera::SetProj()
{
	LastCam.m_mProj = m_mProj;
}

void ZCamera::UpdateFrustumPlanes()
{
	auto viewProj = m_mView * m_mProj;

	// left plane
	m_FrustumPlanes[0].x = viewProj._14 + viewProj._11;
	m_FrustumPlanes[0].y = viewProj._24 + viewProj._21;
	m_FrustumPlanes[0].z = viewProj._34 + viewProj._31;
	m_FrustumPlanes[0].w = viewProj._44 + viewProj._41;

	// right plane
	m_FrustumPlanes[1].x = viewProj._14 - viewProj._11;
	m_FrustumPlanes[1].y = viewProj._24 - viewProj._21;
	m_FrustumPlanes[1].z = viewProj._34 - viewProj._31;
	m_FrustumPlanes[1].w = viewProj._44 - viewProj._41;

	// top plane
	m_FrustumPlanes[2].x = viewProj._14 - viewProj._12;
	m_FrustumPlanes[2].y = viewProj._24 - viewProj._22;
	m_FrustumPlanes[2].z = viewProj._34 - viewProj._32;
	m_FrustumPlanes[2].w = viewProj._44 - viewProj._42;

	// bottom plane
	m_FrustumPlanes[3].x = viewProj._14 + viewProj._12;
	m_FrustumPlanes[3].y = viewProj._24 + viewProj._22;
	m_FrustumPlanes[3].z = viewProj._34 + viewProj._32;
	m_FrustumPlanes[3].w = viewProj._44 + viewProj._42;

	// near plane
	m_FrustumPlanes[4].x = viewProj._14 + viewProj._13;
	m_FrustumPlanes[4].y = viewProj._24 + viewProj._23;
	m_FrustumPlanes[4].z = viewProj._34 + viewProj._33;
	m_FrustumPlanes[4].w = viewProj._44 + viewProj._43;

	// far plane
	m_FrustumPlanes[5].x = viewProj._14 - viewProj._13;
	m_FrustumPlanes[5].y = viewProj._24 - viewProj._23;
	m_FrustumPlanes[5].z = viewProj._34 - viewProj._33;
	m_FrustumPlanes[5].w = viewProj._44 - viewProj._43;

	// normalize
	for (int i = 0; i < 6; i++)
		m_FrustumPlanes->PlaneNormalize();
}

void ZCamera::UpdateCameraAABB()
{
	m_CamAABB = GetAABB();
}

void ZCamera::Convert3Dto2D(ZVec3& lpvOut, const ZVec3& lpPos)const
{
	UINT Num = 1;
	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&Num, &vp);

	ZMatrix mW;
	mW.CreateMove(lpPos);
	ZMatrix m = mW* m_mView* m_mProj;

	float halfW = vp.Width* 0.5f;
	float halfH = vp.Height* 0.5f;

	lpvOut.x = (m._41 / m._44) * halfW + halfW;
	lpvOut.y = (m._42 / m._44) * -halfH + halfH;
	lpvOut.z = m._44;
}

void ZCamera::Convert2Dto3D(ZVec3& lpvOut, const ZVec3& lpvPos)const
{
	UINT Num = 1;
	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&Num, &vp);

	ZMatrix mW;
	lpvOut = DirectX::XMVector3Unproject(lpvPos, vp.TopLeftX, vp.TopLeftY, vp.Width, vp.Height, vp.MinDepth, vp.MaxDepth, m_mProj, m_mView, mW);
}

ZAABB ZCamera::GetAABB()const
{
	float width = (float)APP.m_Window->GetWidth();
	float height = (float)APP.m_Window->GetHeight();
	
	ZVec3 FrustumPos[8] =
	{
		{0,0,0},{0,width,0},
		{height,0,0},{height,width,0},
		{0,0,1},{0,width,1},
		{height,0,1},{height,width,1}
	};

	ZAABB aabb(ZVec3(FLT_MAX), ZVec3(FLT_MIN));
	// スクリーン座用 -> 3D座標
	for (auto& pos : FrustumPos)
	{
		Convert2Dto3D(pos, pos);
		for (int i = 0; i < 3; i++)
		{
			if (aabb.Min[i] > pos[i])
				aabb.Min[i] = pos[i];
			if (aabb.Max[i] < pos[i])
				aabb.Max[i] = pos[i];
		}
	}

	return std::move(aabb);
}

bool ZCamera::IsInsideTheFrustum(const ZAABB& aabb)const
{	
	// 視錐台に収まり切らないオブジェクトはどうするかは未定
	auto min = aabb.Min;
	auto size = (aabb.Max - aabb.Min);

	for (int i = 0; i < 6; i++)
	{
		// 視錐台平面の法線
		ZVec3 planeNormal = ZVec3(m_FrustumPlanes[i].x, m_FrustumPlanes[i].y, m_FrustumPlanes[i].z);

		// 法線に対して正方向の一番遠い頂点取得
		auto vert = min;
		for (int j = 0; j < 3; j++)
		{
			if (planeNormal[j] > 0)
				vert[j] += size[j];
		}

		// 1つでも視錐台平面の法線方向になければ視錐台内には入っていない
		if (ZVec4::PlaneDot(m_FrustumPlanes[i], vert) < 0.0f)
			return false;
	}

	return true;
}
