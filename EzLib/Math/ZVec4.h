//===============================================================
//   4次元ベクトルクラス
// 
//===============================================================
#ifndef ZVec4_h
#define ZVec4_h

namespace EzLib
{

	//============================================================================
	//   ４次元ベクトルクラス
	// 
	//  @ingroup Math
	//============================================================================
	class ZVec4 : public DirectX::XMFLOAT4
	{
	public:

		ZVec4() : DirectX::XMFLOAT4(0, 0, 0, 0) {}

		ZVec4(float f) : DirectX::XMFLOAT4(f, f, f, f) {}

		ZVec4(float fx, float fy, float fz, float fw) : DirectX::XMFLOAT4(fx, fy, fz, fw) {}

		ZVec4(const DirectX::XMFLOAT4& In) : DirectX::XMFLOAT4(In) {}

		ZVec4(DirectX::FXMVECTOR V) { DirectX::XMStoreFloat4(this, V); }

		void Set(float fx, float fy, float fz, float fw)
		{
			x = fx;
			y = fy;
			z = fz;
			w = fw;
		}

		template<typename T>
		void Set(const T& v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		void Set(const ZVec3& v3, float fw)
		{
			x = v3.x;
			y = v3.y;
			z = v3.z;
			w = fw;
		}

		//   文字列の配列からセット
		void Set(const std::vector<std::string>& strArray)
		{
			x = std::stof(strArray[0]);
			y = std::stof(strArray[1]);
			z = std::stof(strArray[2]);
			w = std::stof(strArray[3]);
		}

		//   Json11データからセット [ x, y, z, w]の配列形式
		void Set(const json11::Json& jsonAry)
		{
			auto ary = jsonAry.array_items();
			if (ary.size() != 4)return;
			x = (float)ary[0].number_value();
			y = (float)ary[1].number_value();
			z = (float)ary[2].number_value();
			w = (float)ary[3].number_value();
		}

		// casting
		operator float* () { return &x; }

		//   XMVECTORへ変換
		operator DirectX::XMVECTOR() const { return XMLoadFloat4(this); }
		//   SimpleMath::Vector4へ型のみ変換
		operator DirectX::SimpleMath::Vector4&() const { return *(DirectX::SimpleMath::Vector4*)this; }

		// 比較演算子
		bool operator == (const ZVec4& V) const;
		bool operator != (const ZVec4& V) const;

		// 代入演算子
		ZVec4& operator= (const ZVec4& V) { x = V.x; y = V.y; z = V.z; w = V.w; return *this; }
		ZVec4& operator+= (const ZVec4& V);
		ZVec4& operator-= (const ZVec4& V);
		ZVec4& operator*= (const ZVec4& V);
		ZVec4& operator*= (float S);
		ZVec4& operator/= (float S);

		ZVec4& operator=(const DirectX::XMVECTOR& v)
		{
			XMStoreFloat4(this, v);
			return *this;
		}

		template<typename T>
		ZVec4& operator= (const T& V) { x = V.x; y = V.y; z = V.z; w = V.w; return *this; }

		// Urnary operators
		ZVec4 operator+ () const { return *this; }
		ZVec4 operator- () const;

		// 要素アクセス
		float& operator[](int idx)
		{
			return (&x)[idx];
		}

		// 定数ベクトル
		static const ZVec4 zero;	// 全要素0ベクトル
		static const ZVec4 one;	// 全要素1ベクトル


		//-------------------------------------------------------------
		//   ベクトル内積
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  @return 内積値
		//-------------------------------------------------------------
		static float Dot(const ZVec4 &v1, const ZVec4 &v2);			// 静的関数

		//-------------------------------------------------------------
		//   内積(結果を-1〜1の範囲にする)
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  @return 内積値
		//-------------------------------------------------------------
		static float DotClamp(const ZVec4 &v1, const ZVec4 &v2);			// 静的関数 必ず -1<=dot<=1 で返す

		static float PlaneDot(const ZVec4& plane, const ZVec3& v);

		//-------------------------------------------------------------
		//   内積 自分とv1
		//  	v1		… ベクトル１
		//  @return 内積値
		//-------------------------------------------------------------
		float Dot(const ZVec4 &v1)
		{
			return Dot(*this, v1);
		}

		//-------------------------------------------------------------
		//   内積(結果を-1〜1の範囲にする) 自分とv1
		//  	v1		… ベクトル１
		//  @return 内積値
		//-------------------------------------------------------------
		float DotClamp(const ZVec4 &v1)
		{
			return DotClamp(*this, v1);
		}
		
		//-------------------------------------------------------------
		//   外積
		//  @param[out]	vOut	… 計算結果したベクトルが入る
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  	v3		… ベクトル３
		//-------------------------------------------------------------
		static void Cross(ZVec4 &vOut, const ZVec4 &v1, const ZVec4 &v2, const ZVec4 &v3);	// 静的関数

		//-------------------------------------------------------------
		//   外積 自分 x v1 x v2
		//  @param[out]	vOut	… 計算結果のベクトルが入る
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//-------------------------------------------------------------
		void Cross(ZVec4 &vOut, const ZVec4 &v2, const ZVec4 &v3)
		{
			Cross(vOut, *this, v2, v3);
		}

		//-------------------------------------------------------------
		//   ベクトルの長さ
		//  	vSrc	… 長さを求めるベクトル
		//-------------------------------------------------------------
		static float Length(const ZVec4 &vSrc);						// 静的関数

		//-------------------------------------------------------------
		//   ベクトルの長さ
		//-------------------------------------------------------------
		float Length() { return Length(*this); }

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//  @param[out]	vOut	… 正規化したベクトルが入る
		//  	vSrc	… 処理の基となるベクトル
		//-------------------------------------------------------------
		static void Normalize(ZVec4 &vOut, const ZVec4 &vSrc);			// 静的関数

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//-------------------------------------------------------------
		ZVec4& Normalize()
		{
			Normalize(*this, *this);
			return *this;
		}
		//-------------------------------------------------------------
		//   ベクトルを正規化
		//  @param[out]	vOut	… 正規化したベクトルが入る
		//-------------------------------------------------------------
		void Normalize(ZVec4 &vOut) const { Normalize(vOut, *this); }

		static void PlaneNormalize(ZVec4& vOut, const ZVec4& vSrc); // 静的関数

		ZVec4& PlaneNormalize()
		{
			PlaneNormalize(*this, *this);
			return *this;
		}

		//-------------------------------------------------------------
		//   線形補間
		//-------------------------------------------------------------
		static void Lerp(ZVec4 &vOut, const ZVec4 &v1, const ZVec4 &v2, float f);

		//-------------------------------------------------------------
		//   エルミネートスプライン補間
		//-------------------------------------------------------------
		static void Hermite(ZVec4 &vOut, const ZVec4 &v1, const ZVec4 &t1, const ZVec4 &v2, const ZVec4 &t2, float f);

		//-------------------------------------------------------------
		//   Catmull-Romスプライン補間
		//-------------------------------------------------------------
		static void CatmullRom(ZVec4 &vOut, const ZVec4 &v0, const ZVec4 &v1, const ZVec4 &v2, const ZVec4 &v3, float f);


		//-------------------------------------------------------------
		//   行列で変換
		//  @param[out]	vOut	… 変換後のベクトル
		//  	vSrc	… 処理の基となるベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		static void Transform(ZVec4 &vOut, const ZVec4 &vSrc, const ZMatrix &mat);	// 静的関数

		//-------------------------------------------------------------
		//   行列で変換
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void Transform(const ZMatrix &mat) { Transform(*this, *this, mat); }

		//-------------------------------------------------------------
		//   行列で変換
		//  param[out]	vOut	… 変換後のベクトル
		//  param[in]	mat		… 変換行列
		//-------------------------------------------------------------
		void Transform(ZVec4 &vOut, const ZMatrix &mat) const { Transform(vOut, *this, mat); }

		//-------------------------------------------------------------
		//    ZVec2として扱う
		//-------------------------------------------------------------
		ZVec2& Vec2()
		{
			return *(ZVec2*)this;
		}
		//-------------------------------------------------------------
		//    ZVec3として扱う
		//-------------------------------------------------------------
		ZVec3& Vec3()
		{
			return *(ZVec3*)this;
		}

		//   文字列として返す
		std::string GetUUIDStr()
		{
			std::string str = "( ";
			str += std::to_string(x) + " , ";
			str += std::to_string(y) + " , ";
			str += std::to_string(z) + " , ";
			str += std::to_string(w) + " )";
			return std::move(str);
		}

	};

	/*
	// Binary operators
	ZVec4 operator+ (const ZVec4& V1, const ZVec4& V2);
	ZVec4 operator- (const ZVec4& V1, const ZVec4& V2);
	ZVec4 operator* (const ZVec4& V1, const ZVec4& V2);
	ZVec4 operator* (const ZVec4& V, float S);
	ZVec4 operator/ (const ZVec4& V1, const ZVec4& V2);
	ZVec4 operator* (float S, const ZVec4& V);
	*/

}

#endif
