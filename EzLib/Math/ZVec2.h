//===============================================================
//   2次元ベクトルクラス
// 
//===============================================================
#ifndef ZVec2_h
#define ZVec2_h

namespace EzLib
{

	class ZMatrix;

	//============================================================================
	//   ２次元ベクトルクラス
	// 
	//  @ingroup Math
	//============================================================================
	class ZVec2 : public DirectX::XMFLOAT2
	{
	public:

		ZVec2() : DirectX::XMFLOAT2(0, 0) {}

		ZVec2(float fx, float fy) : DirectX::XMFLOAT2(fx, fy) {}

		ZVec2(int fx,int fy) : DirectX::XMFLOAT2((float)fx, (float)fy) {}

		ZVec2(UINT fx,UINT fy) : DirectX::XMFLOAT2((float)fx, (float)fy) {}

		ZVec2(float f) : DirectX::XMFLOAT2(f, f) {}

		ZVec2(int f) : DirectX::XMFLOAT2((float)f, (float)f) {}

		ZVec2(UINT f) : DirectX::XMFLOAT2((float)f, (float)f) {}

		ZVec2(const DirectX::XMFLOAT2& In) : DirectX::XMFLOAT2(In) {}

		ZVec2(DirectX::FXMVECTOR V) { DirectX::XMStoreFloat2(this, V); }

		void Set(float fx, float fy)
		{
			x = fx;
			y = fy;
		}

		template<typename T>
		void Set(const T& v)
		{
			x = v.x;
			y = v.y;
		}

		template<>
		void Set<float>(const float& f)
		{
			x = f;
			y = f;
		}

		template<>
		void Set<int>(const int& f)
		{
			x = (float)f;
			y = (float)f;
		}

		//   文字列の配列からセット
		void Set(const std::vector<std::string>& strArray)
		{
			x = std::stof(strArray[0]);
			y = std::stof(strArray[1]);
		}

		//   Json11データからセット [ x, y]の配列形式
		void Set(const json11::Json& jsonAry)
		{
			auto ary = jsonAry.array_items();
			if (ary.size() != 2)return;
			x = (float)ary[0].number_value();
			y = (float)ary[1].number_value();
		}

		// casting
		operator float* () { return &x; }

		//   XMVECTORへ変換
		operator DirectX::XMVECTOR() const { return XMLoadFloat2(this); }
		//   SimpleMath::Vector2へ型のみ変換
		operator DirectX::SimpleMath::Vector2&() const { return *(DirectX::SimpleMath::Vector2*)this; }

		// 比較演算子
		bool operator == (const ZVec2& V) const;
		bool operator != (const ZVec2& V) const;

		// 代入演算子
		ZVec2& operator= (const ZVec2& V) { x = V.x; y = V.y; return *this; }
		ZVec2& operator+= (const ZVec2& V);
		ZVec2& operator-= (const ZVec2& V);
		ZVec2& operator*= (const ZVec2& V);
		ZVec2& operator*= (float S);
		ZVec2& operator/= (float S);

		ZVec2& operator=(const DirectX::XMVECTOR& v)
		{
			XMStoreFloat2(this, v);
			return *this;
		}

		template<typename T>
		ZVec2& operator= (const T& V) { x = V.x; y = V.y; return *this; }

		// Urnary operators
		ZVec2 operator+ () const { return *this; }
		ZVec2 operator- () const { return ZVec2(-x, -y); }

		// 要素アクセス
		float& operator[](int idx)
		{
			return (&x)[idx];
		}

		// 定数ベクトル
		static const ZVec2 zero;	// 全要素0ベクトル
		static const ZVec2 one;	// 全要素1ベクトル

		//-------------------------------------------------------------
		//   ベクトル内積
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  @return 内積値
		//-------------------------------------------------------------
		static float Dot(const ZVec2 &v1, const ZVec2 &v2);			// 静的関数

		//-------------------------------------------------------------
		//   内積(結果を-1〜1の範囲にする)
		//  	v1		… ベクトル１
		//  	v2		… ベクトル２
		//  @return 内積値
		//-------------------------------------------------------------
		static float DotClamp(const ZVec2 &v1, const ZVec2 &v2);			// 静的関数 必ず -1<=dot<=1 で返す

		//-------------------------------------------------------------
		//   内積 自分とv1
		//  	v1		… ベクトル１
		//  @return 内積値
		//-------------------------------------------------------------
		float Dot(const ZVec2 &v1)
		{
			return Dot(*this, v1);
		}

		//-------------------------------------------------------------
		//   内積(結果を-1〜1の範囲にする) 自分とv1
		//  	v1		… ベクトル１
		//  @return 内積値
		//-------------------------------------------------------------
		float DotClamp(const ZVec2 &v1)
		{
			return DotClamp(*this, v1);
		}

		//-------------------------------------------------------------
		//   ベクトルの長さを求める
		//  	vSrc	… 長さを求めるベクトル
		//-------------------------------------------------------------
		static float Length(const ZVec2 &vSrc);						// 静的関数

		//-------------------------------------------------------------
		//   ベクトルの長さを求める
		//-------------------------------------------------------------
		float Length()
		{
			return Length(*this);
		}

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//  @param[out]	vOut	… 正規化したベクトルが入る
		//  	vSrc	… 処理の基となるベクトル
		//-------------------------------------------------------------
		static void Normalize(ZVec2 &vOut, const ZVec2 &vSrc);			// 静的関数

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//-------------------------------------------------------------
		ZVec2& Normalize()
		{
			Normalize(*this, *this);
			return *this;
		}

		//-------------------------------------------------------------
		//   ベクトルを正規化
		//  @param[out]	vOut	… 正規化したベクトルが入る
		//-------------------------------------------------------------
		void Normalize(ZVec2 &vOut) const
		{
			Normalize(vOut, *this);
		}

		//-------------------------------------------------------------
		//   線形補間
		//-------------------------------------------------------------
		static void Lerp(ZVec2 &vOut, const ZVec2 &v1, const ZVec2 &v2, float f);

		//-------------------------------------------------------------
		//    エルミネートスプライン補間
		//-------------------------------------------------------------
		static void Hermite(ZVec2 &vOut, const ZVec2 &v1, const ZVec2 &t1, const ZVec2 &v2, const ZVec2 &t2, float f);

		//-------------------------------------------------------------
		//    Catmull-Romスプライン補間
		//-------------------------------------------------------------
		static void CatmullRom(ZVec2 &vOut, const ZVec2 &v0, const ZVec2 &v1, const ZVec2 &v2, const ZVec2 &v3, float f);


		//-------------------------------------------------------------
		//    行列で変換
		//  @param[out]	vOut	… 変換後のベクトル
		//  	vSrc	… 処理の基となるベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		static void Transform(ZVec2 &vOut, const ZVec2 &vSrc, const ZMatrix &mat);	// 静的関数

		//-------------------------------------------------------------
		//    行列で変換
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void Transform(const ZMatrix &mat)
		{
			Transform(*this, *this, mat);
		}

		//-------------------------------------------------------------
		//    行列で変換
		//  @param[out]	vOut	… 変換後のベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void Transform(ZVec2 &vOut, const ZMatrix &mat) const
		{
			Transform(vOut, *this, mat);
		}

		//-------------------------------------------------------------
		//    行列で変換(回転のみ)
		//  @param[out]	vOut	… 変換後のベクトル
		//  	vSrc	… 処理の基となるベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		static void TransformNormal(ZVec2 &vOut, const ZVec2 &vSrc, const ZMatrix &mat);	// 静的関数

		//-------------------------------------------------------------
		//    行列で変換(回転のみ)
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void TransformNormal(const ZMatrix &mat)
		{
			TransformNormal(*this, *this, mat);
		}

		//-------------------------------------------------------------
		//    行列で変換(回転のみ)
		//  @param[out]	vOut	… 変換後のベクトル
		//  	mat		… 変換行列
		//-------------------------------------------------------------
		void TransformNormal(ZVec2 &vOut, const ZMatrix &mat) const
		{
			TransformNormal(vOut, *this, mat);
		}

		//-------------------------------------------------------------
		//   指定方向に指定角度だけ回転。(正反対でも曲がる)
		//  	vTargetDir	… 向きたいの方向
		//  	MaxAng		… 最大回転角度　この角度以上は回転しない
		//-------------------------------------------------------------
		void Homing(const ZVec2& vTargetDir, float MaxAng);

		//-------------------------------------------------------------
		//   三角形のAABB求める
		//  	v1		… 頂点１の座標
		//  	v2		… 頂点２の座標
		//  	v3		… 頂点３の座標
		//  @param[out]	outMinP	… AABBの最少座標
		//  @param[out]	outMaxP	… AABBの最大座標
		//-------------------------------------------------------------
		static void CreateAABB(const ZVec2 &v1, const ZVec2 &v2, const ZVec2 &v3, ZVec2 &outMinP, ZVec2 &outMaxP);

		//-------------------------------------------------------------
		//   線分のAABBを算出
		//  	v1		… 座標１
		//  	v2		… 座標２
		//  @param[out]	outMinP	… AABBの最少座標
		//  @param[out]	outMaxP	… AABBの最大座標
		//-------------------------------------------------------------
		static void CreateAABBFromLineSegment(const ZVec2 &v1, const ZVec2 &v2, ZVec2 &outMinP, ZVec2 &outMaxP);

		//   文字列として返す
		std::string GetUUIDStr()
		{
			std::string str = "( ";
			str += std::to_string(x) + " , ";
			str += std::to_string(y) + " )";
			return std::move(str);
		}

	};

	/*
	// Binary operators
	ZVec2 operator+ (const ZVec2& V1, const ZVec2& V2);
	ZVec2 operator- (const ZVec2& V1, const ZVec2& V2);
	ZVec2 operator* (const ZVec2& V1, const ZVec2& V2);
	ZVec2 operator* (const ZVec2& V, float S);
	ZVec2 operator/ (const ZVec2& V1, const ZVec2& V2);
	ZVec2 operator* (float S, const ZVec2& V);
	*/

}

#endif
