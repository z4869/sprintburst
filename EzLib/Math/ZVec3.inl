//=========================================================================
//
//
// ZVec3
//
//
//=========================================================================

namespace EzLib
{

	//------------------------------------------------------------------------------
	// 比較演算子
	//------------------------------------------------------------------------------

	inline bool ZVec3::operator == (const ZVec3& V) const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR v2 = XMLoadFloat3(&V);
		return XMVector3Equal(v1, v2);
	}

	inline bool ZVec3::operator != (const ZVec3& V) const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR v2 = XMLoadFloat3(&V);
		return XMVector3NotEqual(v1, v2);
	}

	//------------------------------------------------------------------------------
	// 代入演算子
	//------------------------------------------------------------------------------

	inline ZVec3& ZVec3::operator+= (const ZVec3& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR v2 = XMLoadFloat3(&V);
		XMVECTOR X = XMVectorAdd(v1, v2);
		XMStoreFloat3(this, X);
		return *this;
	}

	inline ZVec3& ZVec3::operator-= (const ZVec3& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR v2 = XMLoadFloat3(&V);
		XMVECTOR X = XMVectorSubtract(v1, v2);
		XMStoreFloat3(this, X);
		return *this;
	}

	inline ZVec3& ZVec3::operator*= (const ZVec3& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR v2 = XMLoadFloat3(&V);
		XMVECTOR X = XMVectorMultiply(v1, v2);
		XMStoreFloat3(this, X);
		return *this;
	}

	inline ZVec3& ZVec3::operator*= (float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR X = XMVectorScale(v1, S);
		XMStoreFloat3(this, X);
		return *this;
	}

	inline ZVec3& ZVec3::operator/= (float S)
	{
		using namespace DirectX;
		assert(S != 0.0f);
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR X = XMVectorScale(v1, 1.f / S);
		XMStoreFloat3(this, X);
		return *this;
	}

	//------------------------------------------------------------------------------
	// Urnary operators
	//------------------------------------------------------------------------------

	inline ZVec3 ZVec3::operator- () const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR X = XMVectorNegate(v1);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline float ZVec3::Dot(const ZVec3 &v1, const ZVec3 &v2)
	{
		DirectX::XMVECTOR V1 = DirectX::XMLoadFloat3(&v1);
		DirectX::XMVECTOR V2 = DirectX::XMLoadFloat3(&v2);
		DirectX::XMVECTOR X = DirectX::XMVector3Dot(V1, V2);
		return DirectX::XMVectorGetX(X);
	}

	inline float ZVec3::DotClamp(const ZVec3& v1, const ZVec3& v2)
	{
		float retdot = Dot(v1, v2);
		if (retdot < -1.0f)
		{
			retdot = -1.0f;
		}
		if (retdot > 1.0f)
		{
			retdot = 1.0f;
		}
		return retdot;
	}

	inline void ZVec3::Cross(ZVec3& vOut, const ZVec3& v1, const ZVec3& v2)
	{
		DirectX::XMVECTOR V1 = DirectX::XMLoadFloat3(&v1);
		DirectX::XMVECTOR V2 = DirectX::XMLoadFloat3(&v2);
		DirectX::XMVECTOR R = DirectX::XMVector3Cross(V1, V2);
		DirectX::XMStoreFloat3(&vOut, R);
	}

	inline ZVec3 ZVec3::Cross(const ZVec3& v1, const ZVec3& v2)
	{
		ZVec3 v;
		Cross(v, v1, v2);
		return std::move(v);
	}

	inline float ZVec3::Length(const ZVec3& vSrc)
	{
		DirectX::XMVECTOR v1 = DirectX::XMLoadFloat3(&vSrc);
		DirectX::XMVECTOR X = DirectX::XMVector3Length(v1);
		return DirectX::XMVectorGetX(X);
	}

	inline float ZVec3::LengthSq(const ZVec3& vSrc)
	{
		DirectX::XMVECTOR v1 = DirectX::XMLoadFloat3(&vSrc);
		DirectX::XMVECTOR X = DirectX::XMVector3LengthSq(v1);
		return DirectX::XMVectorGetX(X);
	}

	inline void ZVec3::Normalize(ZVec3& vOut, const ZVec3& vSrc)
	{
		DirectX::XMVECTOR v1 = DirectX::XMLoadFloat3(&vSrc);
		DirectX::XMVECTOR X = DirectX::XMVector3Normalize(v1);
		DirectX::XMStoreFloat3(&vOut, X);
	}

	inline void ZVec3::Clamp(const ZVec3& vmin, const ZVec3& vmax)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(this);
		XMVECTOR v2 = XMLoadFloat3(&vmin);
		XMVECTOR v3 = XMLoadFloat3(&vmax);
		XMVECTOR X = XMVectorClamp(v1, v2, v3);
		XMStoreFloat3(this, X);
	}

	inline void ZVec3::Min(ZVec3& out, const ZVec3& v1, const ZVec3& v2)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat3(&v1);
		XMVECTOR x2 = XMLoadFloat3(&v2);
		XMVECTOR X = XMVectorMin(x1, x2);
		XMStoreFloat3(&out, X);
	}

	inline void ZVec3::Max(ZVec3& out, const ZVec3& v1, const ZVec3& v2)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat3(&v1);
		XMVECTOR x2 = XMLoadFloat3(&v2);
		XMVECTOR X = XMVectorMax(x1, x2);
		XMStoreFloat3(&out, X);
	}

	inline bool ZVec3::NearEqual(const ZVec3& v1, const ZVec3& v2,const ZVec3& eps)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat3(&v1);
		XMVECTOR x2 = XMLoadFloat3(&v2);
		XMVECTOR e = XMLoadFloat3(&eps);
		return XMVector3NearEqual(x1, x2, e);
	}

	inline void ZVec3::Lerp(ZVec3& vOut, const ZVec3& v1, const ZVec3& v2, float f)
	{
		DirectX::XMVECTOR x1 = DirectX::XMLoadFloat3(&v1);
		DirectX::XMVECTOR x2 = DirectX::XMLoadFloat3(&v2);
		DirectX::XMVECTOR X = DirectX::XMVectorLerp(x1, x2, f);
		DirectX::XMStoreFloat3(&vOut, X);
	}

	inline void ZVec3::Hermite(ZVec3& vOut, const ZVec3& v1, const ZVec3& t1, const ZVec3& v2, const ZVec3& t2, float f)
	{
		DirectX::XMVECTOR x1 = DirectX::XMLoadFloat3(&v1);
		DirectX::XMVECTOR x2 = DirectX::XMLoadFloat3(&t1);
		DirectX::XMVECTOR x3 = DirectX::XMLoadFloat3(&v2);
		DirectX::XMVECTOR x4 = DirectX::XMLoadFloat3(&t2);
		DirectX::XMVECTOR X = DirectX::XMVectorHermite(x1, x2, x3, x4, f);
		DirectX::XMStoreFloat3(&vOut, X);
	}

	inline void ZVec3::CatmullRom(ZVec3& vOut, const ZVec3& v0, const ZVec3& v1, const ZVec3& v2, const ZVec3& v3, float f)
	{
		DirectX::XMVECTOR x1 = DirectX::XMLoadFloat3(&v0);
		DirectX::XMVECTOR x2 = DirectX::XMLoadFloat3(&v1);
		DirectX::XMVECTOR x3 = DirectX::XMLoadFloat3(&v2);
		DirectX::XMVECTOR x4 = DirectX::XMLoadFloat3(&v3);
		DirectX::XMVECTOR X = DirectX::XMVectorCatmullRom(x1, x2, x3, x4, f);
		DirectX::XMStoreFloat3(&vOut, X);
	}

	inline void ZVec3::CreateAABB(const ZVec3& v1, const ZVec3& v2, const ZVec3& v3, ZVec3& outMinP, ZVec3& outMaxP)
	{
		outMinP = v1;
		outMaxP = v1;

		// 最小点算出
		if (outMinP.x > v2.x)outMinP.x = v2.x;
		if (outMinP.y > v2.y)outMinP.y = v2.y;
		if (outMinP.z > v2.z)outMinP.z = v2.z;
		if (outMinP.x > v3.x)outMinP.x = v3.x;
		if (outMinP.y > v3.y)outMinP.y = v3.y;
		if (outMinP.z > v3.z)outMinP.z = v3.z;
		// 最大点算出
		if (outMaxP.x < v2.x)outMaxP.x = v2.x;
		if (outMaxP.y < v2.y)outMaxP.y = v2.y;
		if (outMaxP.z < v2.z)outMaxP.z = v2.z;
		if (outMaxP.x < v3.x)outMaxP.x = v3.x;
		if (outMaxP.y < v3.y)outMaxP.y = v3.y;
		if (outMaxP.z < v3.z)outMaxP.z = v3.z;
	}

	inline void ZVec3::CreateAABBFromLineSegment(const ZVec3& v1, const ZVec3& v2, ZVec3& outMinP, ZVec3& outMaxP)
	{

		// 線分のバウンディングボックス算出
		if (v1.x < v2.x)
		{
			outMinP.x = v1.x;
			outMaxP.x = v2.x;
		}
		else
		{
			outMinP.x = v2.x;
			outMaxP.x = v1.x;
		}

		if (v1.y < v2.y)
		{
			outMinP.y = v1.y;
			outMaxP.y = v2.y;
		}
		else
		{
			outMinP.y = v2.y;
			outMaxP.y = v1.y;
		}

		if (v1.z < v2.z)
		{
			outMinP.z = v1.z;
			outMaxP.z = v2.z;
		}
		else
		{
			outMinP.z = v2.z;
			outMaxP.z = v1.z;
		}
	}

	//------------------------------------------------------------------------------
	// Binary operators
	//------------------------------------------------------------------------------
	inline ZVec3 operator+ (const ZVec3& V1, const ZVec3& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V1);
		XMVECTOR v2 = XMLoadFloat3(&V2);
		XMVECTOR X = XMVectorAdd(v1, v2);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline ZVec3 operator- (const ZVec3& V1, const ZVec3& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V1);
		XMVECTOR v2 = XMLoadFloat3(&V2);
		XMVECTOR X = XMVectorSubtract(v1, v2);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline ZVec3 operator* (const ZVec3& V1, const ZVec3& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V1);
		XMVECTOR v2 = XMLoadFloat3(&V2);
		XMVECTOR X = XMVectorMultiply(v1, v2);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline ZVec3 operator* (const ZVec3& V, float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V);
		XMVECTOR X = XMVectorScale(v1, S);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline ZVec3 operator/ (const ZVec3& V1, const ZVec3& V2)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V1);
		XMVECTOR v2 = XMLoadFloat3(&V2);
		XMVECTOR X = XMVectorDivide(v1, v2);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline ZVec3 operator/ (const ZVec3& V1, float S)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V1);
		XMVECTOR X = XMVectorScale(v1, 1 / S);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}

	inline ZVec3 operator* (float S, const ZVec3& V)
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat3(&V);
		XMVECTOR X = XMVectorScale(v1, S);
		ZVec3 R;
		XMStoreFloat3(&R, X);
		return R;
	}



	inline void ZVec3::Transform(ZVec3 &vOut, const ZVec3 &vSrc, const ZMatrix &mat)
	{
		DirectX::XMVECTOR v1 = DirectX::XMLoadFloat3(&vSrc);
		DirectX::XMMATRIX M = DirectX::XMLoadFloat4x4(&mat);
		DirectX::XMVECTOR X = DirectX::XMVector3TransformCoord(v1, M);
		DirectX::XMStoreFloat3(&vOut, X);
	}


	inline void ZVec3::TransformNormal(ZVec3 &vOut, const ZVec3 &vSrc, const ZMatrix &mat)
	{
		DirectX::XMVECTOR v1 = DirectX::XMLoadFloat3(&vSrc);
		DirectX::XMMATRIX M = DirectX::XMLoadFloat4x4(&mat);
		DirectX::XMVECTOR X = DirectX::XMVector3TransformNormal(v1, M);
		DirectX::XMStoreFloat3(&vOut, X);
	}


}
