//===============================================================
//   行列クラス
// 
//===============================================================
#ifndef ZOperationalMatrix_h
#define ZOperationalMatrix_h

namespace EzLib
{

	//============================================================================
	//   演算用３次元ベクトルクラス
	// 
	//  @ingroup Math
	//============================================================================
	class ZOperationalVec3 : public DirectX::XMVECTORF32
	{
	public:
		ZOperationalVec3()
		{
			f[0] = 0;
			f[1] = 0;
			f[2] = 0;
		}

		ZOperationalVec3(float x, float y, float z)
		{
			f[0] = x;
			f[1] = y;
			f[2] = z;
		}

		ZOperationalVec3(const DirectX::XMFLOAT3& v1)
		{
			v = DirectX::XMLoadFloat3(&v1);
		}

		ZOperationalVec3(const DirectX::XMVECTOR& v1)
		{
			v = v1;
		}

		//	operator DirectX::XMVECTOR&() { return m_v; }

		ZOperationalVec3& operator+= (const ZOperationalVec3& V);
		ZOperationalVec3& operator-= (const ZOperationalVec3& V);
		ZOperationalVec3& operator*= (float S)
		{
			v = DirectX::XMVectorScale(v, S);
			return *this;
		}
		ZOperationalVec3& operator/= (float S)
		{
			v = DirectX::XMVectorScale(v, 1.0f / S);
			return *this;
		}

		void operator=(const DirectX::XMVECTOR& v1)
		{
			v = v1;
		}

		static float Dot(const DirectX::XMVECTOR& v1, const DirectX::XMVECTOR& v2);
		//   内積
		float Dot(const DirectX::XMVECTOR& v1)const;

		static void Cross(ZOperationalVec3 &vOut, const ZOperationalVec3& v1, ZOperationalVec3& v2);

		//   長さ
		float Length()const;
		DirectX::XMVECTOR Length2()const;

		float LengthSq()const;

		//   正規化
		void Normalize();
		//   外積(this X v1)
		void CrossBack(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR& v1)const;
		//   外積(v1 X this)
		void CrossFront(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR& v1)const;

		static void Transform(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR& v1, const DirectX::XMMATRIX& mat);
		static void TransformNormal(DirectX::XMVECTOR& vOut, const DirectX::XMVECTOR& v1, const DirectX::XMMATRIX& mat);

		void Transform(const DirectX::XMMATRIX& mat)
		{
			Transform(v, v, mat);
		}

		void TransformNormal(const DirectX::XMMATRIX& mat)
		{
			TransformNormal(v, v, mat);
		}

		//   vの内容をthisにストアする
		void Store(DirectX::XMFLOAT3& out)const;
		DirectX::XMFLOAT3 Store()const;

		void Set(float x, float y, float z)
		{
			f[0] = x;
			f[1] = y;
			f[2] = z;
		}

		float& X() { return f[0]; }
		float& Y() { return f[1]; }
		float& Z() { return f[2]; }

		float X() const { return f[0]; }
		float Y() const { return f[1]; }
		float Z() const { return f[2]; }
	};


	//==========================================================================================================
	//   演算専用行列クラス
	//  
	//  @ingroup Math
	//==========================================================================================================
	class ZOperationalMatrix : public DirectX::XMMATRIX
	{
	public:

		ZOperationalMatrix()
		{
			*this = DirectX::XMMatrixIdentity();
		}

		ZOperationalMatrix(const DirectX::XMFLOAT4X4& mat)
		{
			*this = DirectX::XMLoadFloat4x4(&mat);
		}

		ZOperationalMatrix(const DirectX::XMMATRIX& mat)
		{
			*this = mat;
		}

		void operator=(const DirectX::XMMATRIX& mat)
		{
			*(DirectX::XMMATRIX*)this = mat;
		}

		ZOperationalVec3& GetXAxis() { return reinterpret_cast<ZOperationalVec3&>(r[0]); }
		ZOperationalVec3& GetYAxis() { return reinterpret_cast<ZOperationalVec3&>(r[1]); }
		ZOperationalVec3& GetZAxis() { return reinterpret_cast<ZOperationalVec3&>(r[2]); }
		ZOperationalVec3& GetPos() { return reinterpret_cast<ZOperationalVec3&>(r[3]); }

		//===========================================================
		//
		//
		// 行列作成系
		//
		//
		//===========================================================

		//-----------------------------------------------------------
		//   移動行列作成
		//  	x … x座標
		//  	y … y座標
		//  	z … z座標
		//-----------------------------------------------------------
		void CreateMove(float x, float y, float z);

		//-----------------------------------------------------------
		//   X回転行列を作成
		//-----------------------------------------------------------
		void CreateRotateX(float deg);
		//-----------------------------------------------------------
		//   Y回転行列を作成
		//-----------------------------------------------------------
		void CreateRotateY(float deg);
		//-----------------------------------------------------------
		//   Z回転行列を作成
		//-----------------------------------------------------------
		void CreateRotateZ(float deg);
		//-----------------------------------------------------------
		//   指定軸回転行列を作成
		//-----------------------------------------------------------
		void CreateRotateAxis(const DirectX::XMVECTOR& vAxis, float deg);

		//-----------------------------------------------------------
		//   拡大縮小行列を作成
		//-----------------------------------------------------------
		void CreateScale(float x, float y, float z);

		//===========================================================
		//
		//
		// 行列操作系
		//
		//
		//===========================================================

	//	DirectX::XMMATRIX& operator*=(const ZOperationalMatrix& m) {
	//		Multiply(m);
	//		return *this;
	//	}

		//-----------------------------------------------------------
		//   逆行列化
		//-----------------------------------------------------------
		void Inverse();
		void Inverse(DirectX::XMMATRIX& mOut) const;

		//-----------------------------------------------------------
		//   移動行列を後ろから合成
		//-----------------------------------------------------------
		void Move(float x, float y, float z);

		//-----------------------------------------------------------
		//   X回転行列を後ろから合成
		//-----------------------------------------------------------
		void RotateX(float deg);

		//-----------------------------------------------------------
		//   Y回転行列を後ろから合成
		//-----------------------------------------------------------
		void RotateY(float deg);

		//-----------------------------------------------------------
		//   X回転行列を後ろから合成
		//-----------------------------------------------------------
		void RotateZ(float deg);

		//-----------------------------------------------------------
		//   行列合成(後ろからmを合成)
		//  	m … 合成する行列
		//-----------------------------------------------------------
		void Multiply(const ZOperationalMatrix& m);

		//-----------------------------------------------------------
		//   行列合成　※データ行列を演算行列に変換してから合成する
		//  	m … 合成する行列
		//-----------------------------------------------------------
		void Multiply(const DirectX::XMFLOAT4X4& m);

		//-----------------------------------------------------------
		//   行列合成(手前からmを合成)
		//  	m … 合成する行列
		//-----------------------------------------------------------
		void Multiply_Local(const DirectX::XMMATRIX& m);

		//-----------------------------------------------------------
		//   行列合成　※データ行列を演算行列に変換してから合成する
		//  	m … 合成する行列
		//-----------------------------------------------------------
		void Multiply_Local(const DirectX::XMFLOAT4X4& m);

		//-----------------------------------------------------------
		//   拡大成分を正規化する
		//-----------------------------------------------------------
		void NormalizeScale();

		//-----------------------------------------------------------
		//   演算行列から、データ行列に変換
		//-----------------------------------------------------------
		void Store(DirectX::XMFLOAT4X4& out);

	};


}

#endif
