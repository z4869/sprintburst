#include "ZMatrix.h"

//=========================================================================
//
//
// ZMatrix
//
//
//=========================================================================

namespace EzLib
{

	//------------------------------------------------------------------------------
	// 比較演算子
	//------------------------------------------------------------------------------

	inline bool ZMatrix::operator == (const ZMatrix& M) const
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		return (XMVector4Equal(x1, y1)
				&& XMVector4Equal(x2, y2)
				&& XMVector4Equal(x3, y3)
				&& XMVector4Equal(x4, y4)) != 0;
	}

	inline bool ZMatrix::operator != (const ZMatrix& M) const
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		return (XMVector4NotEqual(x1, y1)
				|| XMVector4NotEqual(x2, y2)
				|| XMVector4NotEqual(x3, y3)
				|| XMVector4NotEqual(x4, y4)) != 0;
	}

	//------------------------------------------------------------------------------
	// 代入演算子
	//------------------------------------------------------------------------------

	inline ZMatrix& ZMatrix::operator+= (const ZMatrix& M)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		x1 = XMVectorAdd(x1, y1);
		x2 = XMVectorAdd(x2, y2);
		x3 = XMVectorAdd(x3, y3);
		x4 = XMVectorAdd(x4, y4);

		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_41), x4);
		return *this;
	}

	inline ZMatrix& ZMatrix::operator-= (const ZMatrix& M)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		x1 = XMVectorSubtract(x1, y1);
		x2 = XMVectorSubtract(x2, y2);
		x3 = XMVectorSubtract(x3, y3);
		x4 = XMVectorSubtract(x4, y4);

		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_41), x4);
		return *this;
	}

	inline ZMatrix& ZMatrix::operator*= (const ZMatrix& M)
	{
		using namespace DirectX;
		XMMATRIX M1 = XMLoadFloat4x4(this);
		XMMATRIX M2 = XMLoadFloat4x4(&M);
		XMMATRIX X = XMMatrixMultiply(M1, M2);
		XMStoreFloat4x4(this, X);
		return *this;
	}

	inline ZMatrix& ZMatrix::operator*= (float S)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_41));

		x1 = XMVectorScale(x1, S);
		x2 = XMVectorScale(x2, S);
		x3 = XMVectorScale(x3, S);
		x4 = XMVectorScale(x4, S);

		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_41), x4);
		return *this;
	}

	inline ZMatrix& ZMatrix::operator/= (float S)
	{
		using namespace DirectX;
		assert(S != 0.f);
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_41));

		float rs = 1.f / S;

		x1 = XMVectorScale(x1, rs);
		x2 = XMVectorScale(x2, rs);
		x3 = XMVectorScale(x3, rs);
		x4 = XMVectorScale(x4, rs);

		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_41), x4);
		return *this;
	}

	inline ZMatrix& ZMatrix::operator/= (const ZMatrix& M)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<XMFLOAT4*>(&_41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		x1 = XMVectorDivide(x1, y1);
		x2 = XMVectorDivide(x2, y2);
		x3 = XMVectorDivide(x3, y3);
		x4 = XMVectorDivide(x4, y4);

		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&_41), x4);
		return *this;
	}

	//------------------------------------------------------------------------------
	// Urnary operators
	//------------------------------------------------------------------------------

	inline ZMatrix ZMatrix::operator- () const
	{
		using namespace DirectX;
		XMVECTOR v1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_11));
		XMVECTOR v2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_21));
		XMVECTOR v3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_31));
		XMVECTOR v4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&_41));

		v1 = XMVectorNegate(v1);
		v2 = XMVectorNegate(v2);
		v3 = XMVectorNegate(v3);
		v4 = XMVectorNegate(v4);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), v1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), v2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), v3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), v4);
		return R;
	}


	//------------------------------------------------------------------------------
	// Binary operators
	//------------------------------------------------------------------------------

	inline ZMatrix operator+ (const ZMatrix& M1, const ZMatrix& M2)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._41));

		x1 = XMVectorAdd(x1, y1);
		x2 = XMVectorAdd(x2, y2);
		x3 = XMVectorAdd(x3, y3);
		x4 = XMVectorAdd(x4, y4);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), x4);
		return R;
	}

	inline ZMatrix operator- (const ZMatrix& M1, const ZMatrix& M2)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._41));

		x1 = XMVectorSubtract(x1, y1);
		x2 = XMVectorSubtract(x2, y2);
		x3 = XMVectorSubtract(x3, y3);
		x4 = XMVectorSubtract(x4, y4);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), x4);
		return R;
	}

	inline ZMatrix operator* (const ZMatrix& M1, const ZMatrix& M2)
	{
		using namespace DirectX;
		XMMATRIX m1 = XMLoadFloat4x4(&M1);
		XMMATRIX m2 = XMLoadFloat4x4(&M2);
		XMMATRIX X = XMMatrixMultiply(m1, m2);

		ZMatrix R;
		XMStoreFloat4x4(&R, X);
		return R;
	}

	inline ZMatrix operator* (const ZMatrix& M, float S)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		x1 = XMVectorScale(x1, S);
		x2 = XMVectorScale(x2, S);
		x3 = XMVectorScale(x3, S);
		x4 = XMVectorScale(x4, S);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), x4);
		return R;
	}

	inline ZMatrix operator/ (const ZMatrix& M, float S)
	{
		using namespace DirectX;
		assert(S != 0.f);

		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		float rs = 1.f / S;

		x1 = XMVectorScale(x1, rs);
		x2 = XMVectorScale(x2, rs);
		x3 = XMVectorScale(x3, rs);
		x4 = XMVectorScale(x4, rs);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), x4);
		return R;
	}

	inline ZMatrix operator/ (const ZMatrix& M1, const ZMatrix& M2)
	{
		using namespace DirectX;
		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M1._41));

		XMVECTOR y1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._11));
		XMVECTOR y2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._21));
		XMVECTOR y3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._31));
		XMVECTOR y4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M2._41));

		x1 = XMVectorDivide(x1, y1);
		x2 = XMVectorDivide(x2, y2);
		x3 = XMVectorDivide(x3, y3);
		x4 = XMVectorDivide(x4, y4);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), x4);
		return R;
	}

	inline ZMatrix operator* (float S, const ZMatrix& M)
	{
		using namespace DirectX;

		XMVECTOR x1 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._11));
		XMVECTOR x2 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._21));
		XMVECTOR x3 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._31));
		XMVECTOR x4 = XMLoadFloat4(reinterpret_cast<const XMFLOAT4*>(&M._41));

		x1 = XMVectorScale(x1, S);
		x2 = XMVectorScale(x2, S);
		x3 = XMVectorScale(x3, S);
		x4 = XMVectorScale(x4, S);

		ZMatrix R;
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._11), x1);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._21), x2);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._31), x3);
		XMStoreFloat4(reinterpret_cast<XMFLOAT4*>(&R._41), x4);
		return R;
	}


	inline void ZMatrix::Move(float x, float y, float z)
	{
		_41 += x;
		_42 += y;
		_43 += z;
	}

	inline void ZMatrix::Move(const ZVec3 &v)
	{
		_41 += v.x;
		_42 += v.y;
		_43 += v.z;
	}

	inline void ZMatrix::Move_Local(float x, float y, float z)
	{
		auto m = DirectX::SimpleMath::Matrix::CreateTranslation(x, y, z);

		Multiply_Local(m);
	}

	inline void ZMatrix::Move_Local(const ZVec3 &v)
	{
		auto m = DirectX::SimpleMath::Matrix::CreateTranslation(v.x, v.y, v.z);

		Multiply_Local(m);
	}

	inline void ZMatrix::RotateX(float deg)
	{
		auto m = DirectX::SimpleMath::Matrix::CreateRotationX(DirectX::XMConvertToRadians(deg));

		Multiply(m);
	}

	inline void ZMatrix::RotateY(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateY(deg);

		Multiply(m);
	}

	inline void ZMatrix::RotateZ(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateZ(deg);

		Multiply(m);
	}

	inline void ZMatrix::RotateAxis(const ZVec3 &vAxis, float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateAxis(vAxis, deg);

		Multiply(m);
	}

	inline void ZMatrix::RotateX_Normal(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateX(deg);

		ZVec3 vTmp;
		vTmp.x = _41;
		vTmp.y = _42;
		vTmp.z = _43;
		_41 = _42 = _43 = 0;
		Multiply(m);// 合成
		_41 = vTmp.x;
		_42 = vTmp.y;
		_43 = vTmp.z;
	}

	inline void ZMatrix::RotateY_Normal(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateY(deg);

		ZVec3 vTmp;
		vTmp.x = _41;
		vTmp.y = _42;
		vTmp.z = _43;
		_41 = _42 = _43 = 0;
		Multiply(m);// 合成
		_41 = vTmp.x;
		_42 = vTmp.y;
		_43 = vTmp.z;
	}

	inline void ZMatrix::RotateZ_Normal(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateZ(deg);

		ZVec3 vTmp;
		vTmp.x = _41;
		vTmp.y = _42;
		vTmp.z = _43;
		_41 = _42 = _43 = 0;
		Multiply(m);// 合成
		_41 = vTmp.x;
		_42 = vTmp.y;
		_43 = vTmp.z;
	}

	inline void ZMatrix::RotateAxis_Normal(const ZVec3 &vAxis, float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateAxis(vAxis, deg);

		ZVec3 vTmp;
		vTmp.x = _41;
		vTmp.y = _42;
		vTmp.z = _43;
		_41 = _42 = _43 = 0;
		Multiply(m);// 合成
		_41 = vTmp.x;
		_42 = vTmp.y;
		_43 = vTmp.z;
	}

	inline void ZMatrix::RotateX_Local(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateX(deg);

		Multiply_Local(m);
	}

	inline void ZMatrix::RotateY_Local(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateY(deg);

		Multiply_Local(m);
	}

	inline void ZMatrix::RotateZ_Local(float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateZ(deg);

		Multiply_Local(m);
	}

	inline void ZMatrix::RotateAxis_Local(const ZVec3 &vAxis, float deg)
	{
		ZOperationalMatrix m;
		m.CreateRotateAxis(vAxis, deg);

		Multiply_Local(m);
	}

	inline void ZMatrix::Scale(float x, float y, float z)
	{
		ZOperationalMatrix m;
		m.CreateScale(x, y, z);

		Multiply(m);
	}

	inline void ZMatrix::Scale(const ZVec3 &v)
	{
		ZOperationalMatrix m;
		m.CreateScale(v.x, v.y, v.z);

		Multiply(m);
	}

	inline void ZMatrix::Scale(const float scale)
	{
		Scale(scale, scale, scale);
	}

	inline void ZMatrix::Scale_Local(float x, float y, float z)
	{
		ZOperationalMatrix m;
		m.CreateScale(x, y, z);

		Multiply_Local(m);
	}

	inline void ZMatrix::Scale_Local(const ZVec3 &v)
	{
		ZOperationalMatrix m;
		m.CreateScale(v.x, v.y, v.z);

		Multiply_Local(m);
	}

	inline void ZMatrix::Scale_Normal(float x, float y, float z)
	{
		ZOperationalMatrix m;
		m.CreateScale(x, y, z);

		ZVec3 vTmp;
		vTmp.x = _41;
		vTmp.y = _42;
		vTmp.z = _43;
		_41 = _42 = _43 = 0;
		Multiply(m);// 合成
		_41 = vTmp.x;
		_42 = vTmp.y;
		_43 = vTmp.z;
	}

	inline void ZMatrix::Scale_Normal(const ZVec3 &v)
	{
		ZOperationalMatrix m;
		m.CreateScale(v.x, v.y, v.z);

		ZVec3 vTmp;
		vTmp.x = _41;
		vTmp.y = _42;
		vTmp.z = _43;
		_41 = _42 = _43 = 0;
		Multiply(m);// 合成
		_41 = vTmp.x;
		_42 = vTmp.y;
		_43 = vTmp.z;
	}

	inline void ZMatrix::Multiply(const ZMatrix &mat)
	{
		ZOperationalMatrix mThis(*this);
		mThis.Multiply(mat);
		mThis.Store(*this);
	}

	inline void ZMatrix::Multiply(const ZOperationalMatrix& mat)
	{
		ZOperationalMatrix mThis(*this);
		mThis.Multiply(mat);
		mThis.Store(*this);
	}

	inline void ZMatrix::Multiply(ZMatrix &outMat, const ZMatrix &mat1, const ZMatrix &mat2)
	{
		ZOperationalMatrix m(mat1);
		m.Multiply(mat2);
		m.Store(outMat);
	}

	inline void ZMatrix::Multiply_Local(const ZMatrix &mat)
	{
		ZOperationalMatrix mThis(*this);
		mThis.Multiply_Local(mat);
		mThis.Store(*this);
	}

	inline void ZMatrix::Multiply_Local(const ZOperationalMatrix& mat)
	{
		ZOperationalMatrix mThis(*this);
		mThis.Multiply_Local(mat);
		mThis.Store(*this);
	}

	inline void ZMatrix::Multiply_NoMove(const ZMatrix& mat)
	{
		ZOperationalMatrix mThis(*this);
		mThis.r[3].m128_f32[0] = 0;
		mThis.r[3].m128_f32[1] = 0;
		mThis.r[3].m128_f32[2] = 0;
		mThis.Multiply(mat);
		mThis.r[3].m128_f32[0] = _41;
		mThis.r[3].m128_f32[1] = _42;
		mThis.r[3].m128_f32[2] = _43;
		mThis.Store(*this);
	}

	inline void ZMatrix::Multiply_NoMove(ZMatrix& outMat, const ZMatrix& mat1, const ZMatrix& mat2)
	{
		ZOperationalMatrix m1(mat1);
		m1.r[3].m128_f32[0] = 0;
		m1.r[3].m128_f32[1] = 0;
		m1.r[3].m128_f32[2] = 0;
		m1.Multiply(mat2);
		m1.r[3].m128_f32[0] = mat1._41;
		m1.r[3].m128_f32[1] = mat1._42;
		m1.r[3].m128_f32[2] = mat1._43;
		m1.Store(outMat);
	}


	//   単位行列にする。
	inline void ZMatrix::CreateIdentity()
	{
		_11 = 1; _12 = 0; _13 = 0; _14 = 0;
		_21 = 0; _22 = 1; _23 = 0; _24 = 0;
		_31 = 0; _32 = 0; _33 = 1; _34 = 0;
		_41 = 0; _42 = 0; _43 = 0; _44 = 1;
	}

	//   回転のみ単位行列にする。
	inline void ZMatrix::CreateIdentityRotate()
	{
		_11 = 1; _12 = 0; _13 = 0; _14 = 0;
		_21 = 0; _22 = 1; _23 = 0; _24 = 0;
		_31 = 0; _32 = 0; _33 = 1; _34 = 0;
	}

	//   逆行列にする
	inline void ZMatrix::Inverse()
	{
		Inverse(*this, *this);
	}

	//   この行列を逆行列にしたものを、mOutに入れる
	//  @param[out]	mOut	… 逆行列にしたものを格納する行列
	inline void ZMatrix::Inverse(ZMatrix &mOut) const
	{
		Inverse(mOut, *this);
	}

	inline ZMatrix ZMatrix::Inversed()const
	{
		ZMatrix mat;
		this->Inverse(mat);
		return std::move(mat);
	}

	//   指定行列を逆行列にしたものを、mOutに入れる
	//  @param[out]	mOut			… 逆行列にしたものを格納する変数
	//  @param[out]	pDeterminant	… 行列式をうけとる変数
	//  	mIn				… 逆行列にする行列
	inline void ZMatrix::Inverse(ZMatrix &mOut, const ZMatrix &mIn)
	{
		ZOperationalMatrix mThis(mIn);
		mThis.Inverse();
		mThis.Store(mOut);
	}

	inline void ZMatrix::Transpose()
	{
		using namespace DirectX;
		XMMATRIX M = XMLoadFloat4x4(this);
		XMStoreFloat4x4(this, XMMatrixTranspose(M));
	}

	//   移動行列を作成
	//  	x	… x座標
	//  	y	… y座標
	//  	z	… z座標
	inline void ZMatrix::CreateMove(float x, float y, float z)
	{
		*this = DirectX::SimpleMath::Matrix::CreateTranslation(x, y, z);
	}

	//   移動行列を作成
	//  	v	… 座標(x,y,z)
	inline void ZMatrix::CreateMove(const ZVec3 &v)
	{
		*this = DirectX::SimpleMath::Matrix::CreateTranslation(v.x, v.y, v.z);
	}

	//   移動行列を作成しmOutに入れる
	//  @param[out]	mOut	… 受け取る行列
	//  	x		… x座標
	//  	y		… y座標
	//  	z		… z座標
	inline void ZMatrix::CreateMove(ZMatrix &mOut, float x, float y, float z)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateTranslation(x, y, z);
	}

	//   移動行列を作成しmOutに入れる
	//  @param[out]	mOut	… 受け取る行列
	//  	v		… 座標(x,y,z)
	inline void ZMatrix::CreateMove(ZMatrix &mOut, const ZVec3& v)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateTranslation(v.x, v.y, v.z);
	}

	inline ZMatrix ZMatrix::CreateMoveMat(const ZVec3& v)
	{
		ZMatrix mat;
		mat.CreateMove(v);
		return mat;
	}

	//   X回転行列を作成
	//  	Ang		… 回転角度(度)
	inline void ZMatrix::CreateRotateX(float deg)
	{
		*this = DirectX::SimpleMath::Matrix::CreateRotationX(DirectX::XMConvertToRadians(deg));
	}

	//   X回転行列を作成を作成し、mOutに入れる
	//  @param[out]	mOut	… 受け取る行列
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateX(ZMatrix &mOut, float deg)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateRotationX(DirectX::XMConvertToRadians(deg));
	}

	//   Y回転行列を作成
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateY(float deg)
	{
		*this = DirectX::SimpleMath::Matrix::CreateRotationY(DirectX::XMConvertToRadians(deg));
	}

	//   Y回転行列を作成を作成し、mOutに入れる
	//  @param[out]	mOut	… 受け取る行列
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateY(ZMatrix &mOut, float deg)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateRotationY(DirectX::XMConvertToRadians(deg));
	}

	//   Z回転行列を作成
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateZ(float deg)
	{
		*this = DirectX::SimpleMath::Matrix::CreateRotationZ(DirectX::XMConvertToRadians(deg));
	}
	//   Z回転行列を作成を作成し、mOutに入れる
	//  @param[out]	mOut	… 受け取る行列
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateZ(ZMatrix &mOut, float deg)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateRotationZ(DirectX::XMConvertToRadians(deg));
	}

	//   任意軸回転行列を作成
	//  	vAxis	… 回転軸
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateAxis(const ZVec3 &vAxis, float deg)
	{
		*this = DirectX::SimpleMath::Matrix::CreateFromAxisAngle((DirectX::SimpleMath::Vector3&)vAxis, DirectX::XMConvertToRadians(deg));
	}
	//   任意軸回転行列を作成し、mOutに入れる
	//  @param[out]	mOut	… 受け取る行列
	//  	vAxis	… 回転軸
	//  	deg		… 回転角度(度)
	inline void ZMatrix::CreateRotateAxis(ZMatrix &mOut, const ZVec3 &vAxis, float deg)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateFromAxisAngle((DirectX::SimpleMath::Vector3&)vAxis, DirectX::XMConvertToRadians(deg));
	}

	//   スケーリング行列を作成
	//  	x	… x拡大率
	//  	y	… y拡大率
	//  	z	… z拡大率
	inline void ZMatrix::CreateScale(float x, float y, float z)
	{
		*this = DirectX::SimpleMath::Matrix::CreateScale(x, y, z);
	}

	//   スケーリング行列を作成
	//  	v	… 拡大率(x,y,z)
	inline void ZMatrix::CreateScale(const ZVec3 &v)
	{
		*this = DirectX::SimpleMath::Matrix::CreateScale(v.x, v.y, v.z);
	}

	//   スケーリング行列を作成
	//  @param[out]	mOut	… 受け取る行列
	//  	x		… x拡大率
	//  	y		… y拡大率
	//  	z		… z拡大率
	inline void ZMatrix::CreateScale(ZMatrix &mOut, float x, float y, float z)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateScale(x, y, z);
	}

	//   スケーリング行列を作成
	//  @param[out]	mOut	… 受け取る行列
	//  	v		… 拡大率(x,y,z)
	inline void ZMatrix::CreateScale(ZMatrix &mOut, const ZVec3 &v)
	{
		mOut = DirectX::SimpleMath::Matrix::CreateScale(v.x, v.y, v.z);
	}

	inline ZMatrix ZMatrix::CreateScaleMat(const ZVec3& v)
	{
		ZMatrix mat;
		mat.Scale(v);
		return mat;
	}


	//   指定平面でミラー反転させるための行列を作成
	//   planePos		… 平面上の座標
	//   planeDir		… 平面の方向
	inline void ZMatrix::CreateReflect(const ZVec3& planePos, const ZVec3& planeDir)
	{
		// 面データ作成
		auto plane = DirectX::XMPlaneFromPointNormal(planePos, planeDir);
		// 指定面でミラー反転させるための行列を作成
		*this = DirectX::SimpleMath::Matrix::CreateReflection(plane);
	}

	//   拡大成分を正規化
	inline void ZMatrix::NormalizeScale()
	{
		GetXAxis().Normalize();
		GetYAxis().Normalize();
		GetZAxis().Normalize();
	}

	inline void ZMatrix::SetBillBoard(const ZMatrix &lpCamMat)
	{
		float x = GetXScale();
		float y = GetXScale();
		float z = GetXScale();

		// 回転部分だけをコピー(拡大は維持)
		_11 = lpCamMat._11*x;
		_12 = lpCamMat._12*x;
		_13 = lpCamMat._13*x;
		_21 = lpCamMat._21*y;
		_22 = lpCamMat._22*y;
		_23 = lpCamMat._23*y;
		_31 = lpCamMat._31*z;
		_32 = lpCamMat._32*z;
		_33 = lpCamMat._33*z;
	}

	inline void ZMatrix::SetBillBoardAxisY(const ZVec3 &vCamPos, const ZVec3 &vY)
	{
		ZVec3 scale = GetScale();

		ZVec3 vX, vZ, vCam, vW;
		ZVec3::Normalize(vW, vY);
		vCam = GetPos() - vCamPos;
		vCam.Normalize();
		ZVec3::Cross(vX, vW, vCam);
		vX.Normalize();
		ZVec3::Cross(vZ, vX, vW);
		vZ.Normalize();

		vX *= scale.x;
		vW *= scale.y;
		vZ *= scale.z;

		_11 = vX.x;
		_12 = vX.y;
		_13 = vX.z;

		_21 = vW.x;
		_22 = vW.y;
		_23 = vW.z;

		_31 = vZ.x;
		_32 = vZ.y;
		_33 = vZ.z;
	}

	inline void ZMatrix::ToQuaternion(const ZQuat& out, bool bNormalizeScale) const
	{
		ZOperationalMatrix mThis(*this);
		if (bNormalizeScale)mThis.NormalizeScale();
		auto Q = DirectX::XMQuaternionRotationMatrix(mThis);
		DirectX::XMStoreFloat4((DirectX::XMFLOAT4*)&out, Q);
	}

	inline ZQuat ZMatrix::ToQuaternion()const
	{
		ZQuat q;
		ToQuaternion(q);
		return q;
	}

	inline void ZMatrix::ToQuaternion(const ZQuat& out, const ZMatrix& mIn, bool bNormalizeScale)
	{
		ZOperationalMatrix m(mIn);
		if (bNormalizeScale)m.NormalizeScale();
		auto Q = DirectX::XMQuaternionRotationMatrix(m);
		DirectX::XMStoreFloat4((DirectX::XMFLOAT4*)&out, Q);
	}

	inline void ZMatrix::MirrorZ()
	{
		ZVec3 vZ = GetZAxis();
		vZ.Normalize();
		float dot = ZVec3::DotClamp(vZ, ZVec3(0, 0, 1));
		ZVec3 vC;
		ZVec3::Cross(vC, vZ, ZVec3(0, 0, 1));
		if (vC.Length() != 0) {
			vC.Normalize();
			RotateAxis_Normal(vC, ToDegree(acos(dot)) * 2);
		}
		_43 *= -1;
	}
}