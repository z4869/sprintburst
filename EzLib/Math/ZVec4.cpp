#include "PCH/pch.h"

using namespace EzLib;

const ZVec4 ZVec4::zero(0, 0, 0, 0);
const ZVec4 ZVec4::one(1, 1, 1, 1);
