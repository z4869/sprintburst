//===============================================================
//   クォータニオンクラス
// 
//===============================================================
#ifndef ZQuat_h
#define ZQuat_h

namespace EzLib
{

	class ZMatrix;

	//============================================================================
	//   クォータニオンクラス
	// 
	//  @ingroup Math
	//============================================================================
	class ZQuat : public DirectX::XMFLOAT4
	{
	public:

		ZQuat() : DirectX::XMFLOAT4(0, 0, 0, 1) {}

		ZQuat(const DirectX::XMFLOAT4 &q) : DirectX::XMFLOAT4(q) {}

		ZQuat(const float *f) : DirectX::XMFLOAT4(f) {}

		ZQuat(float X, float Y, float Z, float W) : DirectX::XMFLOAT4(X, Y, Z, W) {}

		ZQuat(DirectX::FXMVECTOR V) { XMStoreFloat4(this, V); }

		operator DirectX::XMVECTOR() const { return DirectX::XMLoadFloat4(this); }

		// casting
		operator float* () { return &x; }

		// 比較演算子
		bool operator == (const ZQuat& q) const;
		bool operator != (const ZQuat& q) const;

		// 代入演算子
		ZQuat& operator= (const ZQuat& q) { x = q.x; y = q.y; z = q.z; w = q.w; return *this; }
		ZQuat& operator+= (const ZQuat& q);
		ZQuat& operator-= (const ZQuat& q);
		ZQuat& operator*= (const ZQuat& q);
		ZQuat& operator*= (float S);
		ZQuat& operator/= (const ZQuat& q);

		ZQuat& operator=(const DirectX::XMVECTOR& q)
		{
			XMStoreFloat4(this, q);
			return *this;
		}

		template<typename T>
		ZQuat& operator=(const T& q)
		{
			x = q.x;
			y = q.y;
			z = q.z;
			w = q.w;
			return *this;
		}

		// Urnary operators
		ZQuat operator+ () const { return *this; }
		ZQuat operator- () const;


		//-----------------------------------------------------
		//   自分と内積
		//  	q	… 対称のクォータにオン
		//  @return 内積値
		//-----------------------------------------------------
		float Dot(const ZQuat &q) const;

		//-----------------------------------------------------
		//   内積
		//  	q1	… クォータにオン１
		//  	q2	… クォータにオン２
		//  @return 内積値
		//-----------------------------------------------------
		static float Dot(const ZQuat &q1, const ZQuat &q2);

		//-----------------------------------------------------
		//   共役
		//  (-x, -y, -z, w) のクオータニオンを返します
		//-----------------------------------------------------
		void Conjugate();

		//-----------------------------------------------------
		//   単位クォータニオンにする
		//  @param[out]	qOut	… 結果が入る変数
		//-----------------------------------------------------
		static void CreateIdentity(ZQuat &qOut)
		{
			qOut.x = qOut.y = qOut.z = 0;
			qOut.w = 1;
		}

		//-----------------------------------------------------
		//   単位クォータニオンにする
		//-----------------------------------------------------
		void CreateIdentity()
		{
			x = y = z = 0;
			w = 1;
		}

		//-----------------------------------------------------
		//   クォータニオンを共役して、再正規化します。
		//  @param[out]	qOut	… 結果が入る変数
		//  	qIn		… 処理の基になるクォータニオン
		//-----------------------------------------------------
		static void Inverse(ZQuat &qOut, const ZQuat &qIn);

		//-----------------------------------------------------
		//   クオータニオンの逆行列を計算
		//-----------------------------------------------------
		void Inverse()
		{
			Inverse(*this, *this);
		}

		ZQuat Inversed()
		{
			ZQuat q;
			Inverse(q, *this);
			return q;
		}

		//-----------------------------------------------------
		//   正規化
		//-----------------------------------------------------
		void Normalize()
		{
			Normalize(*this, *this);
		}

		ZQuat Normalized()
		{
			ZQuat q;
			Normalize(q, *this);
			return q;
		}

		//-----------------------------------------------------
		//   正規化
		//  @param[out]	qOut	… 結果が入る変数
		//  	qIn		… 処理の基になるクォータニオン
		//-----------------------------------------------------
		static void Normalize(ZQuat &out, const ZQuat &qIn);

		//-----------------------------------------------------
		//   クォータニオンの長さ
		//-----------------------------------------------------
		float Length()
		{
			return Length(*this);
		}

		//-----------------------------------------------------
		//   クォータニオンの長さ
		//  	qIn		… 処理の基になるクォータニオン
		//-----------------------------------------------------
		static float Length(const ZQuat &qIn);

		//-----------------------------------------------------
		//   積(後ろから)
		//  	qIn		… 処理の基になるクォータニオン
		//-----------------------------------------------------
		void Multiply(const ZQuat &qIn);

		//-----------------------------------------------------
		//   積(手前から)
		//  	qIn		… 処理の基になるクォータニオン
		//-----------------------------------------------------
		void Multiply_Local(const ZQuat &qIn);

		//-----------------------------------------------------
		//   積(手前から)
		//  @param[out]	qOut	… 結果が入る変数
		//  	qIn1	… クォータニオン１
		//  	qIn2	… クォータニオン２
		//-----------------------------------------------------
		static void Multiply(ZQuat &qOut, const ZQuat &qIn1, const ZQuat &qIn2);

		//-----------------------------------------------------
		//  brief クォータニオンを任意軸ワールド回転
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度
		//-----------------------------------------------------
		void RotateAxis(const ZVec3 &vAxis, float deg)
		{
			ZQuat q;
			q.CreateRotateAxis(vAxis, deg);
			Multiply(q);
		}
		//-----------------------------------------------------
		//  brief クォータニオンを任意軸ローカル回転
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度
		//-----------------------------------------------------
		void RotateAxis_Local(const ZVec3 &vAxis, float deg)
		{
			ZQuat q;
			q.CreateRotateAxis(vAxis, deg);
			Multiply_Local(q);
		}

		//  brief 任意軸クォータニオンを作成する
		//  	vAxis	… 回転軸
		//  	deg		… 回転角度
		void CreateRotateAxis(const ZVec3 &vAxis, float deg)
		{
			using namespace DirectX;
			XMVECTOR axis = XMLoadFloat3(&vAxis);

			ZQuat R;
			XMStoreFloat4(this, XMQuaternionRotationAxis(axis, DirectX::XMConvertToRadians(deg)));
		}

		//  brief 回転行列に変換
		//  @param[out]	out	… 変換された行列を入れる変数
		void ToMatrix(ZMatrix &out) const;

		ZMatrix ToMatrix()const;

		//  brief 指定クォータニオンを回転行列に変換
		//  @param[out]	out		… 変換された行列を入れる変数
		//  	pIn		… 処理の基になるクォータニオン
		static void ToMatrix(ZMatrix &out, ZQuat &qIn);

		//  brief 球面線形補間 q1〜q2の中間位置のクォータニオンを求める
		//  @param[out]	out		… 補間結果が入る変数
		//  	q1		… クォータニオン１
		//  	q2		… クォータニオン２
		//  	f		… 補間位置(0〜1)
		static void Slerp(ZQuat &out, const ZQuat &q1, const ZQuat &q2, float f);

		//  brief  球面二次補間 q1〜q2の中間位置のクォータニオンを求める
		//  XMQuaternionSquad関数参照
		static void Squad(ZQuat &out, const ZQuat &qQ1, const ZQuat &q1, const ZQuat &q2, const ZQuat &q3, float f)
		{
			using namespace DirectX;
			XMStoreFloat4(&out, XMQuaternionSquad(qQ1, q1, q2, q3, f));
		}

		// 球状平方補間の制御点を設定
		//例)
		// z 軸を中心とした回転
		//	ZQuat Q0 = ZQuat(0,  0, 0.707f, -.707f);
		//	ZQuat Q1 = ZQuat(0,  0, 0.000f, 1.000f);
		//	ZQuat Q2 = ZQuat(0,  0, 0.707f, 0.707f);
		//	ZQuat Q3 = ZQuat(0,  0, 1.000f, 0.000f);
		//	ZQuat A, B, C, Qt;
		//	FLOAT time = 0.5f;
		//
		//	SquadSetup(&A, &B, &C, &Q0, &Q1, &Q2, &Q3);
		//	Squad(&Qt, &Q1, &A, &B, &C, time);

		//  brief  球面二次補間の制御ポイント算出
		//  XMQuaternionSquadSetup関数参照
		static void SquadSetup(ZQuat &Aout, ZQuat &Bout, ZQuat &Cout, const ZQuat &qQ0, const ZQuat &qQ1, const ZQuat &qQ2, const ZQuat &qQ3)
		{
			using namespace DirectX;
			XMVECTOR A, B, C;
			XMQuaternionSquadSetup(&A, &B, &C, qQ0, qQ1, qQ2, qQ3);
			Aout = A;
			Bout = B;
			Cout = C;
		}

	};

	/*
	// Binary operators
	ZQuat operator+ (const ZQuat& Q1, const ZQuat& Q2);
	ZQuat operator- (const ZQuat& Q1, const ZQuat& Q2);
	ZQuat operator* (const ZQuat& Q1, const ZQuat& Q2);
	ZQuat operator* (const ZQuat& Q, float S);
	ZQuat operator/ (const ZQuat& Q1, const ZQuat& Q2);
	ZQuat operator* (float S, const ZQuat& Q);
	*/

}
#endif
