#ifndef __SYSTEM_DEFINES_H__
#define __SYSTEM_DEFINES_H__

//   度 → ラジアン角 変換
template<typename T>
inline constexpr auto ToRadian(T x)
{
	return x * 0.017453f;
}

//   ラジアン角 → 度 変換
template<typename T>
inline constexpr auto ToDegree(T x)
{
	return x * 57.3f;
}

//   RGBA指定からABGR形式のUINT作成
template<typename Tr, typename Tg, typename Tb, typename Ta>
inline constexpr auto RGBA(Tr r,Tg g,Tb b,Ta a)
{
	constexpr bool isFloating = std::is_floating_point<Tr>::value |
								std::is_floating_point<Tg>::value |
								std::is_floating_point<Tb>::value |
								std::is_floating_point<Ta>::value;
	using elementType = std::conditional_t<isFloating, float, DWORD>;
	return RGBA((elementType)r, (elementType)g, (elementType)b, (elementType)a, std::conditional_t<isFloating, std::true_type, std::false_type>{});
}

template<typename T>
inline constexpr UINT RGBA(T r, T g, T b, T a,std::true_type)
{
	return RGBA((DWORD)((r)*255.f), (DWORD)((g)*255.f), (DWORD)((b)*255.f), (DWORD)((a)*255.f), std::false_type{});
}

template<typename T>
inline constexpr UINT RGBA(T r,T g,T b,T a, std::false_type)
{
	return (UINT)((((a & 0xff) << 24) | ((b & 0xff) << 16) | ((g & 0xff) << 8) | (r & 0xff)));
}

//===============================================
// スマートポインタ 短縮名
//===============================================
template<typename T>
using sptr = std::shared_ptr<T>;
template<typename T>
using wptr = std::weak_ptr<T>;
template<typename T>
using uptr = std::unique_ptr<T>;

template<typename Tptr, typename Telm>
inline sptr<Tptr> sptr_dynamic_cast(Telm elment)
{
	return std::dynamic_pointer_cast<Tptr>(elment);
}

template<typename Tptr, typename Telm>
inline sptr<Tptr> sptr_static_cast(Telm elment)
{
	return std::static_pointer_cast<Tptr>(elment);
}

// weak_ptrから生ポインタを取得(lock()より高速)
// ※1フレームに何万回とlock()を行ってしまうとかなり重い
// 　lock()みたいにshared_ptrはいらんが、生ポインタでいい場合はコレ
#define wptr_Get(wp)	(wp.expired() == false ? wp._Get() : nullptr)

//===============================================
// COMポインタ 短縮名
//===============================================
template<typename T>
using ComPtr = Microsoft::WRL::ComPtr<T>;

using int8 = int8_t;
using int16 = int16_t;
using int32 = int32_t;
using int64 = int64_t;

using uint8 = uint8_t;
using uint16 = uint16_t;
using uint32 = uint32_t;
using uint64 = uint64_t;

using uint = unsigned int;
using ulong = unsigned long;

#endif
