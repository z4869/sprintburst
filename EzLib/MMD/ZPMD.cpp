#include "ZPMD.h"

#include "Utils/ZFile.h"

#include <sstream>
#include <iomanip>

namespace EzLib
{
namespace
{
	template<typename T>
	bool Read(T* data, ZFile& file)
	{
		return file.Read(data);
	}

	bool ReadHeader(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		auto& header = pmd->Header;
		Read(&header.Magic, file);
		Read(&header.Version, file);
		Read(&header.ModelName, file);
		Read(&header.Comment, file);

		header.HaveEnglishNameExt = 0;

		if (header.Magic.toStr() != "Pmd")
		{
			MessageBox(nullptr, "PMD Header Error", "PMD File Load Error", MB_OK);
			return false;
		}

		if (header.Version != 1.0f)
		{
			MessageBox(nullptr, "PMD Version Error","PMD File Load Error", MB_OK);
			return false;
		}

		return !file.IsBad();
	}

	bool ReadVertex(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint32 vertCnt = 0;
		if (Read(&vertCnt, file) == false)
			return false;

		auto& vertices = pmd->Vertices;
		vertices.resize(vertCnt);
		for (auto& vertex : vertices)
		{
			Read(&vertex.Position, file);
			Read(&vertex.Normal, file);
			Read(&vertex.UV, file);
			Read(&vertex.Bone[0], file);
			Read(&vertex.Bone[1], file);
			Read(&vertex.BoneWeight, file);
			Read(&vertex.Edge, file);
		}

		return !file.IsBad();
	}

	bool ReadFace(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint32 faceCnt = 0;
		if (Read(&faceCnt, file) == false)
			return false;

		auto& faces = pmd->Faces;
		faces.resize(faceCnt / 3);
		for (auto& face : faces)
		{
			for(int i = 0;i<3;i++)
				Read(&face.Vertices[i], file);
		}

		return !file.IsBad();
	}

	bool ReadMaterial(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint32 mateCnt = 0;
		if (Read(&mateCnt, file) == false)
			return false;

		auto& materials = pmd->Materials;
		materials.resize(mateCnt);
		for (auto& mate : materials)
		{
			Read(&mate.Diffuse, file);
			Read(&mate.Alpha, file);
			Read(&mate.SpePower, file);
			Read(&mate.Specular, file);
			Read(&mate.Ambient, file);
			Read(&mate.ToonIndex, file);
			Read(&mate.EdgeFlag, file);
			Read(&mate.FaceVertexCount, file);
			Read(&mate.TextureName, file);
		}

		return !file.IsBad();

	}

	bool ReadBone(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint16 boneCnt = 0;
		if (Read(&boneCnt, file) == false)
			return false;

		auto& bones = pmd->Bones;
		bones.resize(boneCnt);
		for (auto& bone : bones)
		{
			Read(&bone.BoneName, file);
			Read(&bone.Parent, file);
			Read(&bone.Tail, file);
			Read(&bone.BoneType, file);
			Read(&bone.IKParent, file);
			Read(&bone.Position, file);
		}

		return !file.IsBad();
	}

	bool ReadIK(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint16 ikCnt = 0;

		if (Read(&ikCnt, file) == false)
			return false;

		auto& iks = pmd->IKs;
		iks.resize(ikCnt);
		for (auto& ik : iks)
		{
			Read(&ik.IKNode, file);
			Read(&ik.IKTarget, file);
			Read(&ik.NumChain, file);
			Read(&ik.NumIteration, file);
			Read(&ik.RotateLimit, file);

			ik.Chains.resize(ik.NumChain);
			for (auto& ikChain : ik.Chains)
				Read(&ikChain, file);

		}

		return !file.IsBad();
	}

	bool ReadBlendShape(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint16 blendShapeCnt = 0;
		if (Read(&blendShapeCnt, file) == false)
			return false;

		auto& morphs = pmd->Morphs;
		morphs.resize(blendShapeCnt);

		for (auto& morph : morphs)
		{
			Read(&morph.MorphName, file);

			uint32 vertexCnt = 0;
			Read(&vertexCnt, file);
			morph.Vertices.resize(vertexCnt);
			
			Read(&morph.MorphType, file);
			for (auto& vertex : morph.Vertices)
			{
				Read(&vertex.VertexIndex, file);
				Read(&vertex.Position, file);
			}
		}

		return !file.IsBad();
	}

	bool ReadBlendShapeDisplayList(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint8 displayListCnt = 0;

		if (Read(&displayListCnt, file) == false)
			return false;

		pmd->MorphDisplayList.DispList.resize(displayListCnt);
		for (auto& displayList : pmd->MorphDisplayList.DispList)
			Read(&displayList, file);

		return !file.IsBad();
	}

	bool ReadBoneDisplayList(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint8 displayListCnt = 0;
		if (Read(&displayListCnt, file) == false)
			return false;

		// ボーンの枠にデフォルトでセンターが用意されているのでサイズを+1する
		pmd->BoneDisplayLists.resize(displayListCnt + 1);
		bool first = true;
		for (auto& displayList : pmd->BoneDisplayLists)
		{
			if (first)
				first = false;
			else
				Read(&displayList.Name, file);
		}

		uint32 dispCnt = 0;
		if (Read(&dispCnt, file) == false)
			return false;

		for (uint32 dispIndex = 0; dispIndex < dispCnt; dispIndex++)
		{
			uint16 boneIndex = 0;
			Read(&boneIndex, file);

			uint8 frameIndex = 0;
			Read(&frameIndex, file);
			pmd->BoneDisplayLists[frameIndex].DispList.push_back(boneIndex);
		}

		return !file.IsBad();
	}

	bool ReadExt(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		Read(&pmd->Header.HaveEnglishNameExt, file);

		if (pmd->Header.HaveEnglishNameExt != 0)
		{
			// ModelName
			Read(&pmd->Header.EnglishModelNameExt, file);

			// Comment
			Read(&pmd->Header.EnglishCommentExt, file);

			// BoneName
			for (auto& bone : pmd->Bones)
				Read(&bone.EnglishBoneNameExt, file);

			// BlendShape Name
			// BlendShapeの最初はbaseなのでEnglishNameを読まない
			size_t numBlensShape = pmd->Morphs.size();
			for (size_t bsIndex = 1; bsIndex < numBlensShape; bsIndex++)
			{
				auto& morpsh = pmd->Morphs[bsIndex];
				Read(&morpsh.EnglishShapeNameExt, file);
			}

			// BoneDisplayNameLists
			// BoneDisplayNameの最初はセンターとして使用しているのでEnglishNameは読まない
			size_t numBoneDisplayName = pmd->BoneDisplayLists.size();
			for (size_t displayIndex = 1; displayIndex < numBoneDisplayName; displayIndex++)
			{
				auto& display = pmd->BoneDisplayLists[displayIndex];
				Read(&display.EnglishNameExt, file);
			}

		}

		return !file.IsBad();
	}

	bool ReadToonTextureName(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		for (auto& toonTexName : pmd->ToonTextureNames)
			Read(&toonTexName, file);

		return !file.IsBad();
	}

	bool ReadRigidBodyExt(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint32 rigidBodyCnt = 0;
		if (Read(&rigidBodyCnt, file) == false)
			return false;

		pmd->PhysicsDataSet.RigidBodyTbl.resize(rigidBodyCnt);

		for (auto& rb : pmd->PhysicsDataSet.RigidBodyTbl)
		{
			Read(&rb.RigidBodyName, file);
			Read(&rb.BoneIndex, file);
			Read(&rb.GroupIndex, file);
			Read(&rb.GroupTarget, file);
			Read(&rb.ShapeType, file);
			Read(&rb.ShapeWidth, file);
			Read(&rb.ShapeHeight, file);
			Read(&rb.ShapeDepth, file);
			Read(&rb.Pos, file);
			Read(&rb.Rot, file);
			Read(&rb.RigidBodyWeight, file);
			Read(&rb.RigidBodyPosDimmer, file);
			Read(&rb.RigidBodyRotDimmer, file);
			Read(&rb.RigidBodyRecoil, file);
			Read(&rb.RigidBodyFriction, file);
			Read(&rb.RigidBodyType, file);
		}

		return !file.IsBad();
	}

	bool ReadJointExt(ZPMD* pmd, ZFile& file)
	{
		if (file.IsBad())
			return false;

		uint32 jointCnt = 0;
		if (Read(&jointCnt, file) == false)
			return false;

		pmd->PhysicsDataSet.JointTbl.resize(jointCnt);
		for (auto& joint : pmd->PhysicsDataSet.JointTbl)
		{
			Read(&joint.JointName, file);
			Read(&joint.RigidBodyA, file);
			Read(&joint.RigidBodyB, file);
			Read(&joint.JointPos, file);
			Read(&joint.JointRot, file);
			Read(&joint.ConstraintPos1, file);
			Read(&joint.ConstraintPos2, file);
			Read(&joint.ConstraintRot1, file);
			Read(&joint.ConstraintRot2, file);
			Read(&joint.SpringPos, file);
			Read(&joint.SpringRot, file);
		}

		return !file.IsBad();
	}

	bool ReadPMDFile(ZPMD* pmd, ZFile& file)
	{
		if (ReadHeader(pmd, file) == false)
		{
			MessageBox(nullptr, "Read Header Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadVertex(pmd, file) == false)
		{
			MessageBox(nullptr, "Read Vertex Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadFace(pmd, file) == false)
		{
			MessageBox(nullptr, "Read Face Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadMaterial(pmd, file) == false)
		{
			MessageBox(nullptr, "Read Material Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadBone(pmd, file) == false)
		{
			MessageBox(nullptr, "Read Bone Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadIK(pmd, file) == false)
		{
			MessageBox(nullptr, "Read IK Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadBlendShape(pmd, file) == false)
		{
			MessageBox(nullptr, "Read BlendShape Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadBlendShapeDisplayList(pmd, file) == false)
		{
			MessageBox(nullptr, "Read BlendShapeDisplayList Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		if (ReadBoneDisplayList(pmd, file) == false)
		{
			MessageBox(nullptr, "Read BoneDisplayList Fail", "PMD File Load Error", MB_OK);
			return false;
		}

		size_t toonTexIndex = 1;
		for (auto& toonTexName : pmd->ToonTextureNames)
		{
			ZStringStream ss;
			ss << "toon" << std::setfill('0') << std::setw(2) << ".bmd";
			ZString name = ss.str();
			toonTexName.Set(name.c_str());
			toonTexIndex++;
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadExt(pmd, file) == false)
			{
				MessageBox(nullptr, "Read Ext Fail", "PMD File Load Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadToonTextureName(pmd, file) == false)
			{
				MessageBox(nullptr, "Read Toon Texture Name Fail", "PMD File Load Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadRigidBodyExt(pmd, file) == false)
			{
				MessageBox(nullptr, "Read RigidBodyExt Fail", "PMD File Load Error", MB_OK);
				return false;
			}
		}

		if (file.Tell() < file.GetSize())
		{
			if (ReadJointExt(pmd, file) == false)
			{
				MessageBox(nullptr, "Read JointExt Fail", "PMD File Load Error", MB_OK);
				return false;
			}
		}

		return true;
	}

}
	
	bool ReadPMDFile(ZPMD* pmd,const char* fileName)
	{

		ZFile file;
		if (file.Open(fileName,ZFile::OPEN_MODE::READ) == false)
		{
			MessageBox(nullptr, "PMD File Open Fail", "Error", MB_OK);
			return false;
		}

		if (ReadPMDFile(pmd, file) == false)
		{
			MessageBox(nullptr, "PMD File Read Fail", "Error", MB_OK);
			return false;
		}

		return true;
	}


}