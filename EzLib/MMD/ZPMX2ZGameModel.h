#ifndef __ZPMX_TO_ZGAME_MODEL_H__
#define __ZPMX_TO_ZGAME_MODEL_H__

namespace EzLib
{
	struct ZPMX;
	class ZGameModel;

	bool ZPMX2ZGameModel(ZPMX& pmx, ZGameModel& model,const ZString& filename);
}

#endif