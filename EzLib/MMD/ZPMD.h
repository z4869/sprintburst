#ifndef __ZPMD_H__
#define __ZPMD_H__

#include <cstdint>
#include <vector>
#include <array>
#include <string>

#include "SystemDefines.h"

#include "ZMMDFileStr.h"

#include "Math/ZMath.h"

namespace EzLib
{
	template<size_t size>
	using ZPMDString = ZMMDFileStr<size>;

	struct ZPMDHeader
	{
		ZPMDString<3> Magic;
		float Version;
		ZPMDString<20> ModelName;
		ZPMDString<256> Comment;
		uint8 HaveEnglishNameExt;
		ZPMDString<20> EnglishModelNameExt;
		ZPMDString<256> EnglishCommentExt;
	};

	struct ZPMDVertex
	{
		ZVec3 Position;
		ZVec3 Normal;
		ZVec2 UV;
		uint16 Bone[2];
		uint8 BoneWeight;
		uint8 Edge;
	};

	struct ZPMDFace
	{
		uint16 Vertices[3];
	};

	struct ZPMDMaterial
	{
		ZVec3 Diffuse;
		float Alpha;
		float SpePower;
		ZVec3 Specular;
		ZVec3 Ambient;
		uint8 ToonIndex;
		uint8 EdgeFlag;
		uint32 FaceVertexCount;
		ZPMDString<20> TextureName;
	};

	struct ZPMDBone
	{
		ZPMDString<20> BoneName;
		uint16 Parent;
		uint16 Tail;
		uint8 BoneType;
		uint16 IKParent;
		ZVec3 Position;
		ZPMDString<20> EnglishBoneNameExt;
	};

	struct ZPMDIK
	{
	public:
		using ChainList = ZAVector<uint16>;

	public:
		uint16 IKNode;
		uint16 IKTarget;
		uint8 NumChain;
		uint16 NumIteration;
		float RotateLimit;
		ChainList Chains;
	
	};

	struct ZPMDMorph
	{
	public:
		struct Vertex
		{
			uint32 VertexIndex;
			ZVec3 Position;
		};
		using VertexList = ZAVector<Vertex>;

		enum MorphType : uint8
		{
			BASE,
			EYEBROW,
			EYE,
			RIP,
			OTHER
		};

	public:
		ZPMDString<20> MorphName;
		MorphType MorphType;
		VertexList Vertices;
		ZPMDString<20> EnglishShapeNameExt;

	};

	struct ZPMDMorphDisplayList
	{
		using DisplayList = ZAVector<uint16>;

		DisplayList DispList;
	};

	struct ZPMDBoneDisplayList
	{
		using DisplayList = ZAVector<uint16>;

		ZPMDString<50> Name;
		DisplayList DispList;
		ZPMDString<50> EnglishNameExt;
	};

	enum class ZPMDRigidBodyShape : uint8
	{
		SPHERE = 0,	// 
		BOX = 1,	//  
		CAPSULE = 2	// JvZ
	};

	enum class ZPMDRigidBodyOperation : uint8
	{
		STATIC = 0,				// BoneΗ]
		DYNAMIC = 1,			// ¨Z
		DYNAMIC_ABJUST_BONE = 2	// ¨Z(BoneΚuνΉ)
	};

	struct ZPMDRigidBodyExt
	{
		ZPMDString<20> RigidBodyName;
		uint16 BoneIndex;
		uint8 GroupIndex;
		uint16 GroupTarget; // ΥΛO[v
		ZPMDRigidBodyShape ShapeType;
		float ShapeWidth;
		float ShapeHeight;
		float ShapeDepth;
		ZVec3 Pos;
		ZVec3 Rot;
		float RigidBodyWeight;
		float RigidBodyPosDimmer;
		float RigidBodyRotDimmer;
		float RigidBodyRecoil;
		float RigidBodyFriction;
		ZPMDRigidBodyOperation RigidBodyType;
	};

	struct ZPMDJointExt
	{
		enum
		{
			NUM_JOINT_NAME = 20
		};

		ZPMDString<20> JointName;
		uint32 RigidBodyA;
		uint32 RigidBodyB;
		ZVec3 JointPos;
		ZVec3 JointRot;
		ZVec3 ConstraintPos1;
		ZVec3 ConstraintPos2;
		ZVec3 ConstraintRot1;
		ZVec3 ConstraintRot2;
		ZVec3 SpringPos;
		ZVec3 SpringRot;
	};

	struct ZPMDPhysicsDataSet
	{
		ZAVector<ZPMDRigidBodyExt> RigidBodyTbl;
		ZAVector<ZPMDJointExt> JointTbl;
	};

	struct ZPMD
	{
		ZPMDHeader Header;
		ZAVector<ZPMDVertex> Vertices;
		ZAVector<ZPMDFace> Faces;
		ZAVector<ZPMDMaterial> Materials;
		ZAVector<ZPMDBone> Bones;
		ZAVector<ZPMDIK> IKs;
		ZAVector<ZPMDMorph> Morphs;
		ZPMDMorphDisplayList MorphDisplayList;
		ZAVector<ZPMDBoneDisplayList> BoneDisplayLists;
		std::array<ZPMDString<100>, 10> ToonTextureNames;
		ZPMDPhysicsDataSet PhysicsDataSet;
	};

	bool ReadPMDFile(ZPMD* omd, const char* fileName);

}
#endif