#ifndef __TEXTFILE_READER_H__
#define __TEXTFILE_READER_H__

#include <string>
#include <vector>
#include "ZFile.h"

namespace EzLib
{
	class TextFileReader
	{
	public:
		TextFileReader() = default;
		TextFileReader(const char* path);
		TextFileReader(const ZString& path);
	
		bool Open(const char* path);
		bool Open(const ZString& path);
		
		void Close();
		bool IsOpen();
	
		ZString ReadLine();
		void ReadAllLine(ZVector<ZString>& lines);
		ZString ReadAll();
		bool IsEOF();
	
	private:
		ZFile m_File;
	};
}

#endif