#include "ZFile.h"

namespace EzLib
{
	ZFile::ZFile() :m_fp(nullptr), m_FileSize(0), m_badFlag(false)
	{
	}
	
	ZFile::~ZFile()
	{
		Close();
	}

	bool ZFile::Open(const char* path, OPEN_MODE mode)
	{
		return FileOpen(path,mode == OPEN_MODE::READ ? "rb" : "wb");
	}

	bool ZFile::OpenText(const char* path, OPEN_MODE mode)
	{
		return FileOpen(path, mode == OPEN_MODE::READ ? "r" : "w");
	}

	void ZFile::Close()
	{
		if (m_fp == nullptr)
			return;

		fclose(m_fp);
		m_fp = nullptr;
		m_FileSize = 0;
		m_badFlag = false;
	}

	bool ZFile::IsOpen()
	{
		return m_fp != nullptr;
	}

	uint ZFile::GetSize() const
	{
		return m_FileSize;
	}

	bool ZFile::IsBad() const
	{
		return m_badFlag;
	}

	void ZFile::ClearBadFlag()
	{
		m_badFlag = false;
	}

	bool ZFile::IsEOF()
	{
		return feof(m_fp) != 0;
	}

	FILE* ZFile::GetFilePtr() const
	{
		return m_fp;
	}
	
	bool ZFile::ReadAll(ZVector<char>& buffer)
	{
		if (m_fp == nullptr)
			return false;

		buffer.resize(static_cast<UINT>(m_FileSize));
		Seek(0, SeekDir::BEGIN);

		return Read(buffer.data(), static_cast<size_t>(m_FileSize));
	}
	
	bool ZFile::ReadAll(ZVector<uint8>& buffer)
	{
		if (m_fp == nullptr)
			return false;

		buffer.resize(static_cast<UINT>(m_FileSize));
		Seek(0, SeekDir::BEGIN);

		return Read(buffer.data(), static_cast<size_t>(m_FileSize));
	}
	
	bool ZFile::ReadAll(ZVector<int8>& buffer)
	{
		if (m_fp == nullptr)
			return false;

		buffer.resize(static_cast<UINT>(m_FileSize));
		Seek(0, SeekDir::BEGIN);

		return Read(buffer.data(), static_cast<size_t>(m_FileSize));
	}
	
	bool ZFile::Seek(uint offset, SeekDir origin)
	{
		if (m_fp == nullptr)
			return false;

		int Origin;
		switch (origin)
		{
			case SeekDir::BEGIN:
				Origin = SEEK_SET;
			break;

			case SeekDir::CURRENT:
				Origin = SEEK_CUR;
			break;

			case SeekDir::END:
				Origin = SEEK_END;
			break;

			default:
				return false;
		}

		if(_fseeki64(m_fp,offset,Origin) != 0)
		{
			m_badFlag = true;
			return false;
		}

		return true;
	}
	
	uint ZFile::Tell()
	{
		if (m_fp == nullptr)
			return -1;

		return (uint)_ftelli64(m_fp);
	}
	
	bool ZFile::FileOpen(const char* path, const char* mode)
	{
		if (m_fp != nullptr)
			Close();

		auto error = fopen_s(&m_fp, path, mode);
		if (error != 0)
			return false;

		ClearBadFlag();

		Seek(0, SeekDir::END);
		m_FileSize = Tell();
		Seek(0, SeekDir::BEGIN);
		if (IsBad())
		{
			Close();
			return false;
		}

		return true;
	}

}