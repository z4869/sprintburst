#include "PCH/pch.h"

namespace EzLib
{
	ZUUID::ZUUID()
	{
		m_UUID = CreateUUID();
		m_UUIDStr = UUIDToStr(m_UUID);
	}

	ZUUID::ZUUID(const UUID& uuid)
	{
		m_UUID = uuid;
		m_UUIDStr = UUIDToStr(m_UUID);
	}

	ZUUID::ZUUID(const char* uuidStr)
	{
		StrToUUID(uuidStr, m_UUID);
		m_UUIDStr = uuidStr;
	}

	ZUUID::ZUUID(const ZString& uuidStr)
	{
		StrToUUID(uuidStr.c_str(), m_UUID);
		m_UUIDStr = uuidStr;
	}
	
	bool ZUUID::operator==(const ZUUID& uuid)const
	{
		UUID ruuid = m_UUID;
		UUID luuid = uuid.GetUUID();
		return UUIDEqual(ruuid,luuid);
	}
}
