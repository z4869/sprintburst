#ifndef __ZTHREAD_POOL_H__
#define __ZTHREAD_POOL_H__

namespace EzLib
{
	class ZThreadPool
	{
	private:
		using Task = std::function<void()>;

	public:
		struct ZThreadTaskData
		{
			uint32 TaskIndex;
			uint32 GroupIndex;
		};

	public:
		ZThreadPool();
		ZThreadPool(size_t threadCount);
		~ZThreadPool();

		void Init(size_t threadCount);
		void Release();

		void ThreadLoop(); // スレッドで実行する処理

		// タスク追加
		template<class T>
		auto AddTask(T runnable)->std::future<decltype(runnable())>;

		// タスク追加
		// 指定数のタスクを指定数のグループに振り分けて処理する
		void AddTask(uint32 taskCount, uint32 groupSize, const std::function<void(ZThreadTaskData)>& task);

		// すべてのタスクが終了するまで待機
		void WaitForAllTasksFinish();

		size_t GetNumThreads() const;

	private:
		bool m_IsInitialized;
		bool m_IsTerminationRequested;			// 終了要求があったか
		ZSQueue<Task> m_Tasks;
		std::mutex m_Mtx;
		std::condition_variable m_CV;
		std::condition_variable m_CVWait; // 待機用
		ZSVector<std::thread> m_Threads;
		size_t m_TotalTasks;

	};

}

namespace EzLib
{
#include "ZThreadPool.inl"
}

#endif