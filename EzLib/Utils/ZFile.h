#ifndef __ZFILE_H__
#define __ZFILE_H__

#include <cstdio>
#include <vector>
#include <cstdint>
#include <string>
#include "SystemDefines.h"

namespace EzLib
{
	class ZFile
	{
	public:
		enum OPEN_MODE
		{
			READ = 0,
			WRITE = 1
		};

		enum class SeekDir
		{
			BEGIN,
			CURRENT,
			END
		};

	public:
		ZFile();
		~ZFile();

		// コピー禁止
		ZFile(const ZFile& file) = delete;
		ZFile& operator=(const ZFile& file) = delete;

		// バイナリモードでオープン
		bool Open(const char* path,OPEN_MODE mode);
		bool Open(const ZString& path, OPEN_MODE mode)
		{
			return Open(path.c_str(), mode);
		}

		// テキストモードでオープン
		bool OpenText(const char* path, OPEN_MODE mode);
		bool OpenText(const ZString& path, OPEN_MODE mode)
		{
			return OpenText(path.c_str(), mode);
		}

		void Close();

		bool IsOpen();

		uint GetSize()const;

		bool IsBad()const;

		void ClearBadFlag();

		bool IsEOF();

		FILE* GetFilePtr()const;

		bool ReadAll(ZVector<char>& buffer);
		bool ReadAll(ZVector<uint8>& buffer);
		bool ReadAll(ZVector<int8>& buffer);

		bool Seek(uint offset, SeekDir origin);
		uint Tell();

		template<typename T>
		bool Read(T* buffer,size_t count = 1)
		{
			if (buffer == nullptr)
				return false;

			if (IsOpen() == false)
				return false;

			if (fread_s(buffer, sizeof(T) * count, sizeof(T), count, m_fp) != count)
			{
				m_badFlag = true;
				return false;
			}

			return true;
		}

		template<typename T>
		bool Write(T* buffer,size_t count = 1)
		{
			if (buffer == nullptr)
				return false;

			if (IsOpen() == false)
				return false;

			if (fwrite(buffer,sizeof(T), count, m_fp) != count)
			{
				m_badFlag = true;
				return false;
			}

			return true;
		}

	private:
		bool FileOpen(const char* path, const char* mode);

	private:
		FILE * m_fp;
		uint m_FileSize;
		bool m_badFlag;
	};

	
}
#endif