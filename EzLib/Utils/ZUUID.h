#ifndef __ZUUID_H__
#define __ZUUID_H__

namespace EzLib
{
	class ZUUID
	{
	public:
		ZUUID();
		ZUUID(const UUID& uuid);
		ZUUID(const char* uuidStr);
		ZUUID(const ZString& uuidStr);

		inline const UUID& GetUUID()const { return m_UUID; }
		inline const ZString& GetUUIDStr()const { return m_UUIDStr; }

		bool operator==(const ZUUID& uuid)const;
	private:
		UUID m_UUID;
		ZString m_UUIDStr;
	};
}

// ZUUIDをunordered_mapのキー値として使えるように
namespace std
{
	template<>
	struct hash<EzLib::ZUUID>
	{
		size_t operator()(const EzLib::ZUUID& uuid)const
		{
			auto& uuidStr = uuid.GetUUIDStr();
			return hash<EzLib::Memory::ZString>()(uuidStr);
		}
	};
}

#endif