//===============================================================
//   キーフレームアニメーション系のクラス群
// 
//===============================================================
#ifndef ZKeyFrameAnimatoin_h
#define ZKeyFrameAnimatoin_h

namespace EzLib
{

	//=====================================================
	//   キーフレームアニメ用クォータニオンデータ
	// 
	//  @ingroup Animation
	//=====================================================
	struct ZAnimeKey_Quaternion
	{
		float Time;			// キーの位置(時間)
		ZQuat Value;		// キーのクォータニオン値

		// ※XEDやXMD読み込みでは、処理速度優先のために1や2の補間は曲線になるような0として焼き込ので、基本は0を使う
		char CalcType;		// 補間計算方法 0:球面線形補間 1:球面２次補間 2:ベジェ曲線補間

	//	ZQuat qABC[3];		// CalcType == 1用の制御点A,B,C(事前計算)

		// ベジェ用
		char BezierA[2];
		char BezierB[2];

		// 
		ZAnimeKey_Quaternion() : Time(0), CalcType(0)
		{
			// とりあえずMMDと同じ感じの初期値
			BezierA[0] = 20;
			BezierA[1] = 20;
			BezierB[0] = 107;
			BezierB[1] = 107;
		}

		//   指定時間の回転データを補間計算で求める(static関数)
		//   rotateList		… キーフレーム配列
		//   time				… 補間計算で求めたい時間を指定
		//  @param[out]	outQ			… 補間結果の回転(クォータニオン)が入る
		//  @param[inout] nowKeyIndex	… キーフレーム配列のどの位置からキーの検索を行うか？さらに検索後はそのIndexが入る。nullptr指定で最初から検索(処理効率低)
		static bool InterpolationFromTime(const ZAVector<ZAnimeKey_Quaternion>& keyList, double time, ZQuat &outQ, UINT* nowKeyIndex = nullptr);

		//   回転キーリスト(keyList)内の、曲線系の補間キー(スプラインやベジェ)を、通常キーとして焼き込みを行う(static関数)
		//  @param[inout] keyList		… 編集するキーフレーム配列
		//  	  bakeSpace		… 焼き込み間隔  0で全て埋める 1でTimeを1つづ間隔をあけ焼き込む 2〜同様
		static void BakeCurve(ZAVector<ZAnimeKey_Quaternion>& keyList, int bakeSpace);


	};
	//=====================================================
	//   キーフレームアニメ用ベクトルデータ
	// 
	//  @ingroup Animation
	//=====================================================
	struct ZAnimeKey_Vector3
	{
		float Time;			// キーの位置
		ZVec3 Value;		// キーのベクトル値

		// ※XEDやXMD読み込みでは、処理速度優先のために1や2の補間は曲線になるような0として焼き込ので、基本は0を使う
		char CalcType;		// 補間計算方法 0:線形補間 1:Catmull-Romスプライン補間  2:ベジェ曲線補間

		// ベジェ用
		char BezierX_A[2];
		char BezierX_B[2];
		char BezierY_A[2];
		char BezierY_B[2];
		char BezierZ_A[2];
		char BezierZ_B[2];

		// 
		ZAnimeKey_Vector3() : Time(0), Value(0, 0, 0), CalcType(0)
		{
			BezierX_A[0] = BezierX_A[1] = BezierY_A[0] = BezierY_A[1] = BezierZ_A[0] = BezierZ_A[1] = 20;
			BezierX_B[0] = BezierX_B[1] = BezierY_B[0] = BezierY_B[1] = BezierZ_B[0] = BezierZ_B[1] = 107;
		}

		//   指定時間の回転データを補間計算で求める(static関数)
		//   rotateList		… キーフレーム配列
		//   time				… 補間計算で求めたい時間を指定
		//  @param[out]	outQ			… 補間結果の回転(クォータニオン)が入る
		//  @param[inout] nowKeyIndex	… キーフレーム配列のどの位置からキーの検索を行うか？さらに検索後はそのIndexが入る。nullptr指定で最初から検索(処理効率低)
		static bool InterpolationFromTime(const ZAVector<ZAnimeKey_Vector3>& keyList, double time, ZVec3 &outV, UINT* nowKeyIndex = nullptr);

		//   座標キーリスト(m_Rotate)内の、曲線系の補間キー(スプラインやベジェ)を、通常キーとして焼き込みを行う
		//  @param[inout] keyList		… 編集するキーフレーム配列
		//  	bakeSpace	… 焼き込み間隔  0で全て埋める 1でTimeを1つづ間隔をあけ焼き込む 2〜同様
		static void BakeCurve(ZAVector<ZAnimeKey_Vector3>& keyList, int bakeSpace);

	};
	//=====================================================
	//   キーフレームアニメ用スクリプトデータ
	// 
	//  @ingroup Animation
	//=====================================================
	struct ZAnimeKey_Script
	{
		float Time;			// キーの位置
		ZString Value;	// キーのスクリプト文字列

		ZAnimeKey_Script() : Time(0)
		{
		}
	};

	//==========================================================
	//   複数の回転・拡大・座標を持ったアニメーションデータ
	// 
	//  @ingroup Animation
	//==========================================================
	class ZKeyframeAnime
	{
	public:
		ZAVector<ZAnimeKey_Quaternion>		m_Rotate;	// 回転キーリスト
		ZAVector<ZAnimeKey_Vector3>			m_Scale;	// 拡大キーリスト
		ZAVector<ZAnimeKey_Vector3>			m_Pos;		// 座標キーリスト

		//   解放
		void Release();

		//
		ZKeyframeAnime()
		{
		}
		//
		~ZKeyframeAnime()
		{
			Release();
		}
	};

	//==========================================================
	// 
	//   １種類のアニメーションのデータ
	// 
	//  複数の行列(ボーンなど)をアニメーション可能	\n
	// 
	//  @ingroup Animation
	//==========================================================
	class ZBoneController;
	class ZAnimationSet
	{
	public:
		ZString						m_AnimeName;			// アニメ名
		double							m_TicksPerSecond;		// アニメの実行速度
		double							m_AnimeLen;				// アニメの長さ
		ZAVector<ZKeyframeAnime*>	m_FrameAnimeList;		// 全フレームぶんのキーフレームデ−タ
		ZAVector<ZAnimeKey_Script>	m_Script;				// スクリプトキー

		//   解放
		void Release();

		//   m_FrameAnimeListを指定sizeの個数分確保
		void CreateFrameAnimeList(UINT size);

		//   アニメーションファイル(XMD)を読み込む
		//   XMDファイルはKAMADA SkinMesh Editorから出力できる形式です
		//   filename				… XMDファイルパスを指定
		//   registerAnimeName	… 読み込んだアニメ名をこの名前に変更する　nullptrなら変更しない
		//   bakeCurve			… 曲線補間系のキーは、全て線形補間として変換・追加する(処理が軽くなります)
		bool LoadXMD(const ZString& filename, const char* registerAnimeName = nullptr, bool bakeCurve = true);

		//   アニメーションファイル(VMD)を読み込む
		//   filename				… VMDファイルパスを指定
		//   registerAnimeName	… 読み込んだアニメ名をこの名前に変更する　nullptrなら変更しない
		//   bakeCurve			… 曲線補間系のキーは、全て線形補間として変換・追加する(処理が軽くなります)
		bool LoadVMD(const ZString& filename,ZBoneController& bc, const char* registerAnimeName = nullptr, bool bakeCurve = true);

		// 
		ZAnimationSet() : m_TicksPerSecond(60), m_AnimeLen(0)
		{
		}
		// 
		~ZAnimationSet()
		{
			Release();
		}


	};


	//=====================================================================
	// 
	//   アニメーショントラック情報クラス
	// 
	//  アニメーションの進行を記憶するための物
	// 
	//  @ingroup Animation
	//=====================================================================
	class ZAnimatorTrack
	{
	public:
		//=====================================================================
		// トラックデータ
		//=====================================================================
		ZSP<ZAnimationSet>	m_pSkinAnime;	// アニメデータへのアドレス
		int						m_AnimeNo;		// 現在選択中のアニメ番号
		float					m_Weight;		// 重み	
		double					m_Speed;		// 速度
		double					m_AnimePos;		// アニメ位置	
		bool					m_Loop;			// ループフラグ
		bool					m_IsActive;		// 有効フラグ
		bool					m_DeleteTrack;	// 削除フラグ trueでトラック削除

		double					m_PrevAnimePos = 0;

		//=====================================================================
		// トラックイベントノード用クラス
		//=====================================================================
		//   トラックイベント基本クラス
		class Event_Base
		{
		public:
			//   イベントの種類
			enum SKINTRACKEVENT
			{
				STE_ENABLE,			// 有効無効イベント
				STE_POSITION,		// アニメ位置イベント
				STE_SPEED,			// アニメ速度イベント
				STE_WEIGHT,			// 重みイベント
				STE_DELETETRACK,	// トラック削除イベント
			};

			UINT Type;			// イベントの種類
			double fStartTime;	// 開始時間
		};

		//    Enableイベント
		class Event_Enable : public Event_Base
		{
		public:
			bool bl;			// Enableイベント用
		};

		//    Deleteイベント
		class Event_DeleteTrack : public Event_Base
		{
		public:
		};

		//    Potisionイベント
		class Event_Position : public Event_Base
		{
		public:
			double Pos;
		};

		//    Speedイベント
		class Event_Speed : public Event_Base
		{
		public:
			double fDuration;	// 変化時間
			double NewSpeed;
			double OldSpeed;

			char startflag;
			Event_Speed() :startflag(0) {}
		};

		//    ウェイトイベント
		class Event_Weight : public Event_Base
		{
		public:
			double fDuration;	// 変化時間
			float NewWeight;
			float OldWeight;

			char startflag;
			Event_Weight() :startflag(0) {}
		};

		//=====================================================================
		//   初期化
		//=====================================================================
		void Init()
		{
			m_pSkinAnime = nullptr;
			m_AnimeNo = 0;
			m_Weight = 0;
			m_Speed = 0;
			m_AnimePos = 0;
			m_IsActive = false;
			m_Loop = false;
			m_DeleteTrack = false;

			m_PrevAnimePos = 0;
		}

		//--------------------------------------------------------------------------
		//   Event進行
		//--------------------------------------------------------------------------
		void UpdateEvent(double val);

		//=====================================================================
		// イベント系関数
		//=====================================================================
		void EventTrackEnable(bool NewEnable, double StartTime)
		{
			ZUP<Event_Enable> lpAdd = Make_Unique(Event_Enable, appnew);

			lpAdd->Type = Event_Base::STE_ENABLE;
			lpAdd->bl = NewEnable;
			lpAdd->fStartTime = StartTime;
			EventList.push_back(std::move(lpAdd));
		}
		void EventTrackDelete(double StartTime)
		{
			ZUP<Event_DeleteTrack> lpAdd = Make_Unique(Event_DeleteTrack,appnew);

			lpAdd->Type = Event_Base::STE_DELETETRACK;
			lpAdd->fStartTime = StartTime;
			EventList.push_back(std::move(lpAdd));
		}
		void EventTrackPosition(double NewPos, double StartTime)
		{
			ZUP<Event_Position> lpAdd = Make_Unique(Event_Position, appnew);

			lpAdd->Type = Event_Base::STE_POSITION;
			lpAdd->Pos = NewPos;
			lpAdd->fStartTime = StartTime;
			EventList.push_back(std::move(lpAdd));
		}
		void EventTrackSpeed(double NewSpeed, double StartTime, double Duration)
		{
			ZUP<Event_Speed> lpAdd = Make_Unique(Event_Speed, appnew);

			lpAdd->Type = Event_Base::STE_SPEED;
			lpAdd->NewSpeed = NewSpeed;
			lpAdd->OldSpeed = 0;
			lpAdd->fStartTime = StartTime;
			lpAdd->fDuration = Duration;
			EventList.push_back(std::move(lpAdd));
		}
		void EventTrackWeight(float NewWeight, double StartTime, double Duration)
		{
			ZUP<Event_Weight> lpAdd = Make_Unique(Event_Weight, appnew);

			lpAdd->Type = Event_Base::STE_WEIGHT;
			lpAdd->NewWeight = NewWeight;
			lpAdd->OldWeight = 0;
			lpAdd->fStartTime = StartTime;
			lpAdd->fDuration = Duration;
			EventList.push_back(std::move(lpAdd));
		}

		// イベントをリストから全て消す
		void UnkeyTrackEvents()
		{
			ZAList<ZUP<Event_Base>>::iterator it = EventList.begin();
			while (it != EventList.end())
			{
				it = EventList.erase(it);
			}
		}

		// 
		ZAnimatorTrack()
		{
			Init();
		}
		~ZAnimatorTrack()
		{
			UnkeyTrackEvents();
		}

	private:

		ZAList<ZUP<Event_Base>> EventList;	// イベントリスト

	};



	//=====================================================================
	// 
	//   アニメーション管理クラス
	//  
	//   アニメーションの再生全般を担い、登録している行列に反映する							\n
	//   ZBoneControllerも、これでアニメーション可能										\n
	//   m_Trackに稼働しているアニメーションの情報が入る									\n
	//   基本はm_Track[0]が現在のアニメとなる。アニメ合成中は[1]以降も作成される			\n
	//   (ChangeAnimeSmooth関数など)														\n
	// 
	//  @ingroup Animation
	//=====================================================================
	class ZAnimator
	{
	public:

		//   アニメリスト取得
		const ZAVector<ZSP<ZAnimationSet>>& GetAnimeList() { return m_AnimeList; }

		//-----------------------------------------------------------
		//   ルートモーションを有効/無効にする
		// 
		//  ルートモーション有効時は、0番目に登録されている参照行列は更新されなくなります \n
		//  代わりにAnimation関数の引数で、この行列の変化量を受け取ることができます。
		// 
		//-----------------------------------------------------------
		void EnableRootMotion(bool enable)
		{
			m_EnableRootMotion = enable;
		}

		//=====================================================================
		//
		//
		// 初期設定系
		//
		//
		//=====================================================================

		//-----------------------------------------------------------
		//   初期化
		//-----------------------------------------------------------
		void Init();

		//-----------------------------------------------------------
		//   初期化し、srcAnimatorの内容と同じようにする
		//   アニメリストと行列参照テーブルを同じように構築する。トラック情報はコピーされない。
		//-----------------------------------------------------------
		void Init(ZAnimator& srcAnimator);

		//-----------------------------------------------------------
		//   初期化し、行列参照テーブルにrefMatを登録する
		//  さらに行列を追加したい場合はAddRefMatrix関数で。
		//-----------------------------------------------------------
		void Init(ZMatrix* refMat);


		//=====================================================================
		//
		//
		// アニメーション結果を適用する、行列参照テーブル設定
		//
		//
		//=====================================================================

		//-----------------------------------------------------------
		//   アニメーション結果を適用する参照行列テーブルを削除する
		//-----------------------------------------------------------
		void ClearRefMatrix()
		{
			m_RefMatrixTbl.clear();
			m_RefMatrixTbl.shrink_to_fit();
		}

		//-----------------------------------------------------------
		//   アニメーション結果を適用する参照行列を１つ追加登録する
		//  @param[inout] mat				… アニメーション進行時に更新される行列のアドレス
		//-----------------------------------------------------------
		void AddRefMatrix(ZMatrix* mat)
		{
			m_RefMatrixTbl.emplace_back(mat);
		}

		//-----------------------------------------------------------
		//   アニメーション結果を適用する参照行列を変更する
		//   idx					… 登録Indexを指定
		//  @param[inout] mat				… アニメーション進行時に更新される行列のアドレス
		//-----------------------------------------------------------
		void ChangeRefMatrix(int idx, ZMatrix* mat)
		{
			m_RefMatrixTbl[idx].refTransMat = mat;
		}

		//=====================================================================
		//
		//
		// アニメーション登録
		//
		//
		//=====================================================================

		//-----------------------------------------------------------
		//   アニメーションデータ１つを追加登録する
		//   allowSameName	… true:無条件で追加する 　false:同名のアニメ名がある場合、そこに上書きされる
		//-----------------------------------------------------------
		void AddAnimation(ZSP<ZAnimationSet> anime, bool allowSameName = false);

		//-----------------------------------------------------------
		//   アニメーションデータをファイルから読み込み、追加登録する
		//   XmdFileName			… XMD形式のアニメーションファイル(KAMADA SkinMesh Editorで出力可能)
		//   registerAnimeName	… 読み込んだアニメ名をこの名前に変更する　nullptrなら変更しない
		//   allowSameName		… true:無条件で追加する 　false:同名のアニメ名がある場合、そこに上書きされる
		//  @return 読み込み・追加されたアニメーションデータのshared_ptr
		//-----------------------------------------------------------
		ZSP<ZAnimationSet> AddAnimation(const ZString& XmdFileName, const char* registerAnimeName = nullptr, bool allowSameName = false);

		//-----------------------------------------------------------
		//   アニメーションリストをコピーする
		//   animeTbl		… コピー元のアニメーションリスト
		//-----------------------------------------------------------
		void CopyAnimationList(const ZAVector<ZSP<ZAnimationSet>>& animeTbl)
		{
			m_AnimeList = animeTbl;
		}

		//=====================================================================
		//
		//
		// アニメーション進行・計算
		//
		//
		//=====================================================================

		//-----------------------------------------------------------
		//   トラック情報取得
		//  現在再生中のトラックは[0]
		//-----------------------------------------------------------
		ZADeque<ZSP<ZAnimatorTrack>>& GetTrack() { return m_Track; }

		//-----------------------------------------------------------
		//   指定トラックのアニメのNo取得
		//  	trackNo	… 取得したいトラック番号
		//-----------------------------------------------------------
		int		GetAnimeNo(int trackNo = 0) { return m_Track[trackNo]->m_AnimeNo; }

		//-----------------------------------------------------------
		//   指定トラックのアニメ速度のNo取得
		//  	trackNo	… 取得したいトラック番号
		//-----------------------------------------------------------
		double	GetAnimeSpeed(int trackNo = 0) { return m_Track[trackNo]->m_Speed; }

		//-----------------------------------------------------------
		//   指定トラックのアニメ位置を取得
		//  	trackNo	… 取得したいトラック番号
		//-----------------------------------------------------------
		double	GetAnimePos(int trackNo = 0) { return m_Track[trackNo]->m_AnimePos; }

		//-----------------------------------------------------------
		//   最大アニメ数取得
		//-----------------------------------------------------------
		int		GetMaxAnimeNum();

		//-----------------------------------------------------------
		//   アニメーションが最後まで行ったか判定
		//  	trackNo	… 取得したいトラック番号
		//  @return 最後までいった:true
		//-----------------------------------------------------------
		bool IsAnimationEnd(UINT TrackNo = 0)
		{
			if (TrackNo >= m_Track.size())return true;
			if (m_Track[TrackNo]->m_IsActive == false)return true;
			if (m_Track[TrackNo]->m_pSkinAnime == nullptr)return true;

			if (m_Track[TrackNo]->m_AnimePos >= m_Track[TrackNo]->m_pSkinAnime->m_AnimeLen)return true;

			return false;
		}

		//-----------------------------------------------------------
		//   Valだけアニメーションを進行させ、参照行列を更新する
		//  	  Val			… 進行速度
		//  @param[inout] inoutRootMotionMatrix… 0番目に登録されている参照行列の変化量を、この行列に合成出力する。
		//-----------------------------------------------------------
		void Animation(double Val, ZMatrix* inoutRootMotionMatrix = nullptr);

		//-----------------------------------------------------------
		//   Valだけアニメーションを進行させ、参照行列を更新する
		// 
		//  ※現在のアニメ位置からValの間にスクリプトキーが存在するとき、そのスクリプトキーの個数分	\n
		//  　Animation(Val)を実行し、runProc(scr)を実行する。										\n
		// 
		//  	Val					… 進行速度
		//  	onScriptExecProc	… 実行するスクリプトデータが来たときに呼び出す関数\n
		// 										引数にはそのスクリプトデータ(AnimeKey_Script型)が来る
		//  @param[inout] inoutRootMotionMatrix	… 0番目に登録されている参照行列の変化量を、この行列に合成出力する。
		//-----------------------------------------------------------
		void AnimationAndScript(double Val, std::function<void(ZAnimeKey_Script*)> onScriptExecProc, ZMatrix* inoutRootMotionMatrix = nullptr);

		//-----------------------------------------------------------
		//   アニメーション変更
		// 
		//  ※直接指定したトラックにデータを上書きする										\n
		//    指定したトラックが存在しなかった場合や存在しないAnimeNoの場合はfalseが返る	\n
		// 
		//   AnimeNo		… アニメ番号
		//   loop			… ループ再生をする？
		//   SetTrackNo	… 情報をセットするトラックの番号
		//   bEnableTrack	… そのトラックを有効にする？
		//   Speed		… アニメ速度
		//   Weight		… 重み
		//   AnimePos		… アニメ位置
		//  @return 正常に変更できたらtrue
		//-----------------------------------------------------------
		bool ChangeAnime(UINT AnimeNo, bool loop, UINT SetTrackNo = 0, bool bEnableTrack = true, double Speed = 1.0, float Weight = 1.0f, double AnimePos = 0);

		// 
		bool AddAnime(UINT AnimeNo, bool loop, bool bEnableTrack = true, double Speed = 1.0, float Weight = 1.0f, double AnimePos = 0);

		//-----------------------------------------------------------
		//   ChangeAnime関数の、アニメ名指定バージョン
		//-----------------------------------------------------------
		bool ChangeAnime(const ZString& AnimeName, bool loop, UINT SetTrackNo = 0, bool bEnableTrack = true, double Speed = 1.0, float Weight = 1.0f, double AnimePos = 0);

		//-----------------------------------------------------------
		//   滑らかにアニメを変更
		// 
		//  ※新しいトラックを作成・設定し、前回のトラックは削除する				\n
		//  ※Durationにより、滑らかにトラック同士のブレンドを行うことができるので、\n
		//    ゲームのアニメ切り替えで使用すると良い								\n
		// 
		//   AnimeNo		… アニメ番号
		//   StartTime	… 変化を開始する時間
		//   Duration		… 変化にかかる時間 0だと即座に変更
		//   loop			… ループ再生をする？
		//   Speed		… アニメ速度
		//   AnimePos		… アニメ位置
		//  @return 正常に変更できたらtrue
		//-----------------------------------------------------------
		bool ChangeAnimeSmooth(UINT AnimeNo, float StartTime, float Duration, bool loop, double Speed = 1.0, double AnimePos = 0);

		//-----------------------------------------------------------
		//   ChangeAnimeSmooth関数の、アニメ名指定バージョン
		//-----------------------------------------------------------
		bool ChangeAnimeSmooth(const ZString& AnimeName, float StartTime, float Duration, bool loop, double Speed = 1.0, double AnimePos = 0);

		//
		bool AddAnimeSmooth(UINT AnimeNo, float StartTime, float Duration, bool loop, double Speed, double AnimePos, float startWeight, float endWeight);
		//
		bool AddAnimeSmooth(const ZString& AnimeName, float StartTime, float Duration, bool loop, double Speed, double AnimePos, float startWeight, float endWeight);

		//-----------------------------------------------------------
		//   全トラックのイベントを全て消す
		//-----------------------------------------------------------
		void UnkeyAllTrackEvents()
		{
			for (auto& var : m_Track)
				var->UnkeyTrackEvents();
		}

		//-----------------------------------------------------------
		//   [0]を除く、全トラックを削除
		//-----------------------------------------------------------
		void ResetTrack()
		{
			m_Track.clear();
			m_Track.push_back(Make_Shared(ZAnimatorTrack,appnew));
			m_Track[0]->Init();
		}
		
		//-----------------------------------------------------------
		//   アニメーション名からIndexを取得。ない場合は-1
		//  	AnimeName	… アニメ名
		//  @return 発見：アニメ番号　アニメ無し：-1
		//-----------------------------------------------------------
		int SearchAnimation(const ZString& AnimeName)
		{
			for (UINT i = 0; i < m_AnimeList.size(); i++)
			{
				if (m_AnimeList[i]->m_AnimeName == AnimeName)
				{
					return i;
				}
			}
			return -1;
		}

		//=====================================================================
		//
		// ベースウェイト
		//
		//=====================================================================

		//-----------------------------------------------------------
		//   アニメータの重みを取得
		//-----------------------------------------------------------
		float GetBaseWeight() { return m_BaseWeight; }

		//-----------------------------------------------------------
		//   アニメータの合成の重みをセット
		//   weight	… セットする重み(0〜1)
		//   speed	… 重みを、現在地からweightへ徐々に変化させる速度　1で即座にセット
		//-----------------------------------------------------------
		void SetBaseWeight(float weight, float speed = 1.0f)
		{
			m_BaseWeightAnime = weight;
			m_BaseWeightAnimeSpeed = speed;
			if (speed >= 1.0f)
			{
				m_BaseWeight = weight;
			}
		}


		//=====================================================================
		// 
		ZAnimator();

		// 
		~ZAnimator()
		{
		}

	private:

		bool			m_EnableRootMotion = true;		// Root Motionを有効にする ※回転の座標のみ


		//-----------------------------------------------------------
		//   トラックの情報から全ボーンのTransMat更新
		//  @param[inout] inoutRootMotionMatrix	… 0番目に登録されている参照行列の変化量を、この行列に合成出力する。
		//-----------------------------------------------------------
		void CalcAnimation(ZMatrix* inoutRootMotionMatrix);

		//-----------------------------------------------------------
		// アニメータ自体の合成の重み
		//-----------------------------------------------------------
		float			m_BaseWeight;					// 現在全体のベースブレンドウェイト
		float			m_BaseWeightAnime;				// 滑らか変化時に使用。変化後のベースブレンドウェイト
		float			m_BaseWeightAnimeSpeed;			// 滑らか変化時に使用。変化速度


		//-----------------------------------------------------------
		//   アニメーションリスト
		//-----------------------------------------------------------
		ZAVector<ZSP<ZAnimationSet>>	m_AnimeList;

		//-----------------------------------------------------------
		//   アニメーショントラック情報
		// 
		//   ※最低でも１つは存在する	\n
		//   ※ChangeAnimeSmooth系の関数で新しいTrackが追加される。複数Trackがある場合は、複数アニメが合成される。\n
		//-----------------------------------------------------------
		ZADeque<ZSP<ZAnimatorTrack>>	m_Track;

		//-----------------------------------------------------------
		// 行列参照構造データ
		//  １つのボーン単位でもある
		//-----------------------------------------------------------
		struct RefMatrixData
		{

			// 参照行列　ここで参照されている行列へ、アニメーション結果がコピーされる
			ZMatrix*	refTransMat = nullptr;

			// アニメーション再生 最適化用データ
			UINT		nowKeyIndex_Rota = 0;
			UINT		nowKeyIndex_Scale = 0;
			UINT		nowKeyIndex_Pos = 0;

			RefMatrixData() {}
			RefMatrixData(ZMatrix* m) : refTransMat(m) {}

		};

		ZAVector<RefMatrixData>	m_RefMatrixTbl;

	};


	//=====================================================================
	// 
	//   低機能アニメーション管理クラス
	// 
	//  ZAnimationSetの0番目(Rootボーン)のアニメーションのみを行う。	\n
	//  アニメーション結果はm_Rotate, m_Scale, m_Posに格納される。		\n
	//  座標に関しては、 m_outPosX, m_outPosY, m_outPosZにも出力される	\n
	// 
	//  @ingroup Animation
	//=====================================================================
	class ZSimpleAnimator
	{
	public:

		//------------------------------------------------------------------
		//   XMDファイルからアニメを読み込む
		//------------------------------------------------------------------
		void LoadXMD(const ZString& xmdFilename)
		{
			m_Anime = Make_Shared(ZAnimationSet,appnew);
			m_Anime->LoadXMD(xmdFilename);
		}

		//------------------------------------------------------------------
		//   アニメーションを直接セットする
		//------------------------------------------------------------------
		void SetAnimation(const ZSP<ZAnimationSet>& anime)
		{
			m_Anime = anime;
		}

		//------------------------------------------------------------------
		//   アニメーションを進める
		// 
		//  アニメーション結果は、m_Rotate, m_Scale, m_Posに格納される \n
		//  座標に関しては、 m_outPosX, m_outPosY, m_outPosZにも出力される
		// 
		//   speed	… 再生速度
		//------------------------------------------------------------------
		void Animation(double speed);

		//------------------------------------------------------------------
		//   アニメーションの再生設定
		//   loop		… ループ再生する？
		//   animePos	… 再生位置(-1で変更しない)
		//------------------------------------------------------------------
		void Play(bool loop, double animePos = 0)
		{
			m_Play = true;
			if (animePos >= 0)
			{
				m_NowAnimePos = animePos;
			}
			m_Loop = loop;
		}

		//------------------------------------------------------------------
		//   停止
		//------------------------------------------------------------------
		void Stop()
		{
			m_Play = false;
			m_NowAnimePos = 0;
		}

		//------------------------------------------------------------------
		//   一時停止
		//------------------------------------------------------------------
		void Pause()
		{
			m_Play = false;
		}

		//------------------------------------------------------------------
		//   結果データ(m_Rotate, m_Scale, m_Pos)を行列化
		//  @param[out] outMat	… 結果を入れる為の行列
		//------------------------------------------------------------------
		void ToMatrix(ZMatrix& outMat)
		{
			m_Rotate.ToMatrix(outMat);
			outMat.Scale_Local(m_Scale);
			outMat.SetPos(m_Pos);
		}

		//   アニメーション結果が入るデータ
		ZQuat			m_Rotate;
		ZVec3			m_Scale;
		ZVec3			m_Pos;

	private:
		// アニメーションデータ
		ZSP<ZAnimationSet>	m_Anime;			// アニメーションデータ

		// 
		bool					m_Play = false;		// 再生中？
		double					m_NowAnimePos = 0;	// 現在再生位置
		bool					m_Loop = false;		// ループ再生？

	};

}

#endif
