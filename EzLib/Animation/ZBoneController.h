//===============================================================
//   モデルデータのボーンを操作するクラス
// 
//===============================================================
#ifndef ZBoneController_h
#define ZBoneController_h

namespace EzLib
{
	class ZIKSolver;

	class ZPhysicsShape_Base;
	class ZPhysicsRigidBody;
	class ZPhysicsJoint_Base;
	class ZPhysicsWorld;

	//===========================================================================================
	// 
	//   モデルデータのボーンを操作するクラス
	// 
	//  ZGameModelクラスを参照し、その情報を元にボーンを動作させるクラス
	//  [主な機能]
	//  ・渡されたZGameModelと同じボーン構造を作成
	//  ・アニメーションを行いたい場合は、ZAnimatorクラスを別途使用する
	//  ・全ボーンのLocal行列をTrans行列を元に計算
	//  ・シェーダで使用するボーンの変換行列用の定数バッファの作成・書き込み
	// 
	//===========================================================================================
	class ZBoneController
	{
	private:
		struct cbBones;
	public:
		class BoneNode;
		
		class ZBCRigidBody : public ZPhysicsRigidBody
		{
		public:
			ZBCRigidBody(ZBoneController& bc);
			virtual void Release()override;
			bool Create(ZSP<ZPhysicsShape_Base> shape, const ZGM_RigidBodyData& rbData, ZSP<ZGameModel>& modle, ZSP<ZBoneController::BoneNode>& node);
			void RefrectGlobalTransform();
			void CalcLocalTransfrom();

		private:
			ZSP<ZBoneController::BoneNode> m_BoneNode;
			ZMatrix m_OffsetMat;
			ZGM_RigidBodyData::CalcType m_CalcType;
			ZBoneController* m_pBC;
		};

		struct ZPhysicsObjectSet
		{
			ZAVector<ZSP<ZBCRigidBody>> RigidBodys;
			ZAVector<ZSP<ZPhysicsJoint_Base>> Joints;
		};

		//=================================================================================
		// 情報取得
		//=================================================================================

		//   登録されているゲームモデルクラス取得
		ZSP<ZGameModel>					GetGameModel() { return m_pGameModel; }

		//   ルートボーン取得　ボーンがない場合はnullptr
		ZSP<BoneNode>						GetRootBone() { return m_BoneTree.size() == 0 ? nullptr : m_BoneTree[0]; }

		//   ボーンテーブル取得
		ZAVector<ZSP<BoneNode>>&		GetBoneTree() { return m_BoneTree; }

		//	ソート済みボーンテーブル取得
		ZAVector<ZSP<BoneNode>>&		GetSortedBones() { return m_SortedBones; }

		//   アニメーションが存在するか？
		bool								IsAnimationexist() { return m_pGameModel->IsAnimationexist(); }

		//   物理演算オブジェクトセットのリストを取得
		ZAVector<ZPhysicsObjectSet>&		GetPhysicsObjectSetList() { return m_PhysicsObjectSetList; }

		//=================================================================================
		// 設定・解放
		//=================================================================================

		//------------------------------------------------------------------------
		//   モデルを設定する
		// 
		//  ZGameModelを元にデータを構築する
		// 
		//   pGameMesh	… モデルデータであるZGameModel。
		//   enableBones	… ボーン情報も使用する。スタティックメッシュの場合は必要ないので、少しだけ処理・容量がマシになる。
		// 
		//------------------------------------------------------------------------
		bool SetModel(ZSP<ZGameModel> pGameMesh, bool enableBones = true);

		//------------------------------------------------------------------------
		//   メッシュを読み込む
		// 
		//   実際の読み込みはZGameModelクラスで行っている		\n
		//   この関数はResourceStorageを使用しモデルを読み込み(ZGameModel)、SetModel()を読んでるだけ
		// 
		//  	filename	… ファイル名
		//------------------------------------------------------------------------
		bool LoadMesh(const ZString& filename);

		//------------------------------------------------------------------------
		//   参照行列設定
		//------------------------------------------------------------------------
		void SetRefMatrix(ZSP<ZMatrix> mat);

		//------------------------------------------------------------------------
		//   参照行列初期化(nullptr代入)
		//------------------------------------------------------------------------
		void ResetRefMatrix();

		//------------------------------------------------------------------------
		//   解放
		//------------------------------------------------------------------------
		void Release();

		//------------------------------------------------------------------------
		//   このモデル用にアニメータを初期化する
		//  ※Rootボーンに関しては、複数トラックブレンドが無効になります
		//  	animator	… 初期化するアニメータ
		//------------------------------------------------------------------------
		void InitAnimator(ZAnimator& animator);

		//------------------------------------------------------
		// モデルの物理オブジェクトを物理ワールドへ追加
		//------------------------------------------------------
		void AddAllPhysicsObjToPhysicsWorld(ZPhysicsWorld& world);

		//=================================================================================
		// ボーン行列演算
		//=================================================================================

		//------------------------------------------------------------------------
		//   m_Matをベースに全てのボーンのLocalMatを更新する(計算にはTransMatを使用)。
		//  内部でrecCalcBoneMatrix関数を呼んでいるだけ
		//  	baseMat	… 処理の基になる行列。この行列を先頭に全ボーンを計算していく。
		//------------------------------------------------------------------------
		void CalcBoneMatrix(bool afterPhysics = false);

		void UpdatePhysics();

		//   parentMatをベースに指定ボーン以下のLocalMatを更新する(計算にはTransMatを使用)。
		static void recCalcBoneMatrix(BoneNode& node, const ZMatrix& parentMat)
		{
			// 計算
			// ワールド行列作成
			ZMatrix::Multiply(node.LocalMat, node.TransMat, parentMat);

			// 子再帰
			for (UINT i = 0; i < node.Child.size(); i++)
				recCalcBoneMatrix(*node.Child[i].GetPtr(), node.LocalMat);
		}

		//------------------------------------------------------------------------
		//   全ボーンを、描画で使用するボーン用定数バッファに書き込む
		//------------------------------------------------------------------------
		void UpdateBoneConstantBuffer();

		// ボーン定数バッファ取得
		ZConstantBuffer<cbBones>& GetBoneConstantBuffer()
		{
			return m_cb_Bones;
		}

		//------------------------------------------------------------------------
		//   全ボーンのTransMatをデフォルトに戻す
		//------------------------------------------------------------------------
		void ResetDefaultTransMat();

		//=================================================================================
		//=================================================================================

		//------------------------------------------------------------------------
		//   ボーンテーブルから指定した名前のボーンの情報を取得 名前が存在しない場合はnullptrが返る
		//  	BoneName	… 検索したいボーン名
		//  @return 存在する場合:ボーンデータのアドレス　存在しない場合:nullptr
		//------------------------------------------------------------------------
		ZSP<BoneNode> SearchBone(const ZString& BoneName)
		{
			for (UINT i = 0; i < m_BoneTree.size(); i++)
			{
				if (m_BoneTree[i]->pSrcBoneNode->BoneName == BoneName)
				{
					return m_BoneTree[i];
				}
			}
			return nullptr;
		}

		//------------------------------------------------------------------------
		//   ボーンテーブルから指定した名前のボーンの番号を取得 名前が存在しない場合は-1が返る
		//  	BoneName	… 検索したいボーン名
		//  @return 存在する場合:ボーン番号　存在しない場合:-1
		//------------------------------------------------------------------------
		int SearchBoneIndex(const ZString& BoneName)
		{
			for (UINT i = 0; i < m_BoneTree.size(); i++)
			{
				if (m_BoneTree[i]->pSrcBoneNode->BoneName == BoneName)
				{
					return i;
				}
			}
			return -1;
		}

		void EnableIKBone(const ZString& IKBoneName, bool flg);

		bool IsEnableIK(const ZString& IKBoneName);

		// 
		ZBoneController()
		{
		}
		// 
		ZBoneController(ZSP<ZGameModel> gameModel, bool enableBones)
		{
			SetModel(gameModel, enableBones);
		}

		//
		~ZBoneController()
		{
			Release();
		}

	private:
		void CreateBoneTreeFromModel();
		
		bool CreatePhysicsObjectsFromModel();

	private:
		//---------------------------------------------------------------------------------
		//   元となるZGameModelのポインタ
		//---------------------------------------------------------------------------------
		ZSP<ZGameModel>						m_pGameModel = nullptr;

		//---------------------------------------------------------------------------------
		//   ボーンツリー＆ボーン配列
		//  [0]がRoot ツリー構造であるが、1次元配列としてもアクセス可能。
		//---------------------------------------------------------------------------------
		ZAVector<ZSP<BoneNode>>				m_BoneTree;

		// ソート済みボーン配列
		ZAVector<ZSP<BoneNode>>				m_SortedBones;
		
		// 物理更新前に計算するボーン
		ZAVector<ZSP<BoneNode>>				m_DeformBeforePhysicsBones;

		// 物理更新後に計算するボーン
		ZAVector<ZSP<BoneNode>>				m_DeformAfterPhysicsBones;

		// IK
		ZAVector<ZUP<ZIKSolver>>			m_IKs;

		// 行列参照
		ZWP<ZMatrix>							m_RefMat;

		// 物理オブジェクト(剛体,ジョイント)リスト
		ZAVector<ZPhysicsObjectSet>	 m_PhysicsObjectSetList;

		// 定数バッファ用
		struct cbBones
		{
			ZMatrix	mWArray[1024];	// ボーン行列配列
		};
		ZConstantBuffer<cbBones>				m_cb_Bones;

	public:


		//=====================================================================
		//   ZBoneController用ボーンノード
		//  @ingroup Graphics_Model_Important
		//=====================================================================
		class BoneNode
		{
		public:
			ZSP<const ZGameModel::BoneNode>	pSrcBoneNode;	// 元データ側(ZGameModeのBoneNode)のボーンへの参照

			ZWP<BoneNode>				Mother;			// 親ボーンへのアドレス
			ZAVector<ZSP<BoneNode>>		Child;			// 子ボーン配列

			int							Level;			// 階層 Rootが0

			ZMatrix						TransMat;		// 変換行列(Animation関数で更新される)　※親ボーンからの相対的な行列

			ZMatrix						LocalMat;		// ローカル行列(CalcBoneMatrix関数で更新される)　※原点からの相対的な行列

			// ※ボーンの最終的なワールド行列は、「LocalMat* キャラのワールド行列」となります。

			//-----------------------------------------------------------
			//   ボーンIndex取得
			//  @return ボーンIndex  失敗時は-1
			//-----------------------------------------------------------
			int GetBoneIndex() const
			{
				if (pSrcBoneNode == nullptr)return -1;
				return pSrcBoneNode->OffsetID;
			}

			// 
			BoneNode() : Level(0), pSrcBoneNode(nullptr)
			{
			}
		};


	private:
		// コピー禁止用
		ZBoneController(const ZBoneController& src) {}
		void operator=(const ZBoneController& src) {}
	};

	bool CreateRigidBody(ZBoneController::ZBCRigidBody** out, ZGM_RigidBodyData& rbData,ZSP<ZGameModel>& model,ZBoneController& bc);
	
	class ZIKSolver
	{
	private:
		using BoneNode = ZBoneController::BoneNode;

		enum class SolveAxis
		{
			X,
			Y,
			Z
		};

		struct IKChain
		{
			ZSP<BoneNode> Node;
			bool EnableAxisLimit;
			ZVec3 LimitMax;
			ZVec3 LimitMin;
		};

	public:
		ZIKSolver(ZBoneController& bc);
		~ZIKSolver()
		{
			Release();
		}

		void Release();

		void AddIKChain(ZSP<BoneNode> node, bool isKnee = false);
						
		void AddIKChain(ZSP<BoneNode> node, bool axisLimit, const ZVec3& limitMin, const ZVec3& limitMax);

		void Solve();

		void SetIKNode(ZSP<BoneNode> node)
		{
			m_IKNode = node;
		}
		ZSP<BoneNode> GetIKNode()const
		{
			return m_IKNode;
		}

		void SetTargetNode(ZSP<BoneNode> node)
		{
			m_IKTarget = node;
		}
		ZSP<BoneNode> GetTargetNode()const
		{
			return m_IKTarget;
		}

		ZString GetName()const
		{
			if (m_IKNode != nullptr)
				return m_IKNode->pSrcBoneNode->BoneName;
			else
				return "";
		}

		void SetIterateCount(uint32 count)
		{
			m_IterateCnt = count;
		}
		void SetLimitAngle(float ang)
		{
			m_LimitAng = ang;
		}
		void SetEnable(bool flg)
		{
			m_IsEnable = flg;
		}
		bool GetEnabled()const
		{
			return m_IsEnable;
		}

		void SaveBaseAnim()
		{
			m_IsBaseAnimEnable = m_IsEnable;
		}
		void LoadBaseAnim()
		{
			m_IsEnable = m_IsBaseAnimEnable;
		}
		void ClearBaseAnim()
		{
			m_IsBaseAnimEnable = true;
		}
		bool GetBaseAnimEnabled()const
		{
			return m_IsBaseAnimEnable;
		}

	private:
		void UpdateBone(BoneNode& bone);
		
		void AddIKChain(IKChain&& chain);
		void _Solve(uint32 iteration);
		
	private:
		ZBoneController& m_BC;
		ZAVector<IKChain> m_Chains;
		ZSP<BoneNode> m_IKNode;
		ZSP<BoneNode> m_IKTarget;
		uint32 m_IterateCnt;
		float m_LimitAng; // ループ計算時の1回あたりの制限角度
		bool m_IsEnable;
		bool m_IsBaseAnimEnable;

	};

	class ZDynamicMotionState : public ZMotionState
	{
	public:
		ZDynamicMotionState(ZSP<ZBoneController::BoneNode>& node, const ZMatrix& offset, bool override = true)
			: m_Node(node), m_Offset(offset), m_IsOverride(override)
		{
			m_InvOffset = offset.Inversed();
			Reset();
		}

		void getWorldTransform(btTransform& worldTransform)const override
		{
			worldTransform = m_Trans;
		}

		void setWorldTransform(const btTransform& worldTransform)override
		{
			m_Trans = worldTransform;
		}

		void Reset()override
		{
			ZMatrix global = m_Offset * m_Node->LocalMat;
			m_Trans.setFromOpenGLMatrix(&global._11);
			m_InitTrans = m_Trans;
		}

		void ReflectGlobalTransform()override
		{
			ZMatrix world;
			m_Trans.getOpenGLMatrix(&world._11);
			ZMatrix global = m_InvOffset * world;

			if (m_IsOverride)
			{
				m_Node->LocalMat = global;
				for (auto& child : m_Node->Child)
					// 子ボーンに反映
					ZBoneController::recCalcBoneMatrix(*child, m_Node->LocalMat);
			}
		}

		virtual btTransform GetInitTrans()override
		{
			return m_InitTrans;
		}

	private:
		ZSP<ZBoneController::BoneNode> m_Node;
		ZMatrix m_Offset;
		ZMatrix m_InvOffset;
		btTransform m_Trans;
		btTransform m_InitTrans;
		bool m_IsOverride;
	};

	// モデルボーン追従
	class ZDynamicAndBoneMerggeMotionState : public ZMotionState
	{
	public:
		ZDynamicAndBoneMerggeMotionState(ZSP<ZBoneController::BoneNode>& node, const ZMatrix offset, bool override = true)
			: m_Node(node), m_Offset(offset), m_IsOverride(override)
		{
			m_InvOffset = offset.Inversed();
			Reset();
		}

		void getWorldTransform(btTransform& worldTransform)const override
		{
			worldTransform = m_Trans;
		}

		void setWorldTransform(const btTransform& worldTransform)override
		{
			m_Trans = worldTransform;
		}

		void Reset()override
		{
			ZMatrix global = m_Offset * m_Node->LocalMat;
			m_Trans.setFromOpenGLMatrix(&global._11);
			m_InitTrans = m_Trans;
		}

		void ReflectGlobalTransform()override
		{
			ZMatrix world;
			m_Trans.getOpenGLMatrix(&world._11);
			ZMatrix btGlobal = m_InvOffset * world;
			ZMatrix global = m_Node->LocalMat;
			btGlobal.SetPos(global.GetPos());

			if (m_IsOverride)
			{
				m_Node->LocalMat = btGlobal;

				for (auto& child : m_Node->Child)
					ZBoneController::recCalcBoneMatrix(*child, m_Node->LocalMat);
			}

		}

		virtual btTransform GetInitTrans()override
		{
			return m_InitTrans;
		}

	private:
		ZSP<ZBoneController::BoneNode> m_Node;
		ZMatrix m_Offset;
		ZMatrix m_InvOffset;
		btTransform m_Trans;
		btTransform m_InitTrans;
		bool m_IsOverride;
	};


	class ZKinematicMotionState : public ZMotionState
	{
	public:
		ZKinematicMotionState(ZSP<ZBoneController::BoneNode>& node, const ZMatrix& offset)
			: m_Node(node), m_Offset(offset)
		{
			getWorldTransform(m_InitTrans);
		}

		void getWorldTransform(btTransform& worldTransform)const override
		{
			ZMatrix m;
			if (m_Node != nullptr)
				m = m_Offset * m_Node->LocalMat;
			else
				m = m_Offset;
			worldTransform.setFromOpenGLMatrix(&m._11);
		}

		void setWorldTransform(const btTransform& worldTransform)override
		{
		}

		void Reset()override
		{
		}

		void ReflectGlobalTransform()override
		{
		}

		virtual btTransform GetInitTrans()override
		{
			return m_InitTrans;
		}

	private:
		ZSP<ZBoneController::BoneNode> m_Node;
		ZMatrix m_Offset;
		btTransform m_InitTrans;
	};



}
#endif
