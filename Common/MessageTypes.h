#ifndef __MESSAGE_TYPES_H__
#define __MESSAGE_TYPES_H__

// メッセージタイプ識別用
enum MessageTypes : char
{
	Login = 1,						/* Client->Server ユーザー名など送信 */
	LoginError,						/* Server->Client ユーザーが希望したプレイスタイルの人数上限等 */
	NotifyLogin,					/* Server->Client ログイン通知 */
	Logout,							/* Client->Server 切断 */
	NotifyLogout,					/* Server->Client ログアウト通知 */
	NotifyDisConnect,				/* Server->Client 回線切断通知 */
	NotifyLastMatchingTimeLimit,	/* Server->Client 残りのマッチングタイムリミット通知 */
	NotifyMatchingTimeOver,			/* Server->Client マッチング時間切れ通知 */
	NotifyLastStartMatchTimeLimit,	/* Server->Client マッチ開始までのタイムリミット通知 */
	NotifyStartMatch,				/* Server->Client マッチ開始通知 */
	Message,
	KeyInput,						/* Client->Server キー入力情報送信 */
	LoadMap,						/* Server->Client マップのファイルパス送信 */
	SpawnObject,					/* オブジェクト生成通知 */
	MoveObject,						/* オブジェクト同期 */
	DeSpawnObject,					/* オブジェクト削除通知 */
	//		:
	//		:
	// 必要に応じて要追加
};

struct LoginData
{
	enum PlayStyle : char
	{
		Observer = -1,	// 観戦者(主に発表用に一応用意)
		Hacker,
		Security,
	};

	static const char* GetPlayStyleName(PlayStyle playStyle)
	{
		switch (playStyle)
		{
			case PlayStyle::Observer:
				return "Observer";
			break;

			case PlayStyle::Hacker:
				return "Hacker";
			break;

			case PlayStyle::Security:
				return "Security";
			break;

			default:
				return "Unknown";
			break;
		}
	}

	char Name[32];		// ユーザー名
	PlayStyle Style;	// 基本的にハッカー or セキュリティのどちらか
	//	:
	// 必要に応じて要追加
};

struct LoginErrorData
{
	char ErrorMsg[128];
};

struct NotifyLoginData
{
	UUID UserUUID;
	LoginData LoginData;
};

struct NotifyLogoutData
{
	UUID UserUUID;
};

struct NotifyMatchingLastTimeLimitData
{
	float MatchingLastTimeLimit;
};

struct LoadMapData
{
	char FileName[256];
};

struct NotifyLastMatchingTimeLimitData
{
	float LastMatchingTimeLimit;
};

struct NotifyLastStartMatchTimeLimitData
{
	float LastStartMatchTimeLimit;
};

struct SpawnObjectData
{
	char ModelFile[256];
	UUID UUID;
	ZVec3 Pos;
	ZVec3 Rot;
	ZVec3 Scale;
};

struct MoveObjectData
{
	ZUUID TargetObject;
	ZVec3 DeltaPos;
	ZVec3 DeltaRot;
	ZVec3 DeltaScale;
};

//		:
//		:

#endif