#ifndef __CHARACTER_COMPONENTS_H__
#define __CHARACTER_COMPONENTS_H__

#include "Camera/GameCamera.h"
#include "State/State.h"

DefComponent(CameraComponent)
{
	GameCamera Cam;

	bool Enable;
	
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// プレイヤー共通
DefComponent(PlayerComponent)
{
	ZString Name;		// 名前
	int ActionState;	// ステート
	float Speed;	// 移動速度
	float Gravity;		// 重力
	bool IsSky;			// 空中か
	ZVec3 Velocity;		// 移動（保存）

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

DefComponent(StateMachineComponent)
{
	ZUP<StateMachine> pStateMachine;
	ZString StartState;
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

DefComponent(PlayerControllerComponent)
{
	bool Enable;
	ZVec3 Axis;
	int Button;		// 現在のフレームのボタン入力情報
	bool ControllerConnected;
	int ControllerID;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// セキュリティ(キラー)用
DefComponent(SecurityComponent)
{
	ZUnorderedMap<ZUUID, int> MultiHitMap;	// 多段ヒット防止用
	int AttackPower;						// 攻撃力

	int CreateWallCnt;						// 生成した壁の数
	ZSP<ECSEntity> WallEntity;
	ZDeque<ZSP<ECSEntity>> WallEntityList;	// 生成した壁
	ZSP<ZGameModel> WallModel;

	float WalkSpeed;						// 移動速度

	static const int MaxNumCreateWall;		// 生成できる壁の数

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// ハッカー(サバイバー)用
DefComponent(HackerComponent)
{
	Effekseer::Effect* DamageEffect;
	ZAUnorderedMap<Effekseer::Handle,ZVec3> DamageEffectPos;

	Effekseer::Effect* WarpEffect;
	Effekseer::Effect* TraceEffect;
	Effekseer::Effect* InjuryEffect;
	

	float	HP;					// 体力
	int		CureGage = 0;		// 負傷・ダウン時の回復ゲージ
	int		DamageEffectTime = 0;
	
	ZWP<ECSEntity> AccessTerminal; // アクセスしている端末
	
	ZWP<ECSEntity> DestroyObj;
	bool	DestroyObjMode = false;
	int		DestroyObjCnt = 0;	// 破壊したオブジェクト数

	ZWP<ECSEntity> CurePlayer;	// 回復させているプレイヤー
	int MaxCure;				// 治療ゲージの最大値
	
	int DestroyObjLimit;		// 破壊できるオブジェクト数

	// 移動スピード関係
	float WalkSpeed;
	float RunSpeed;
	float SquatWalkSpeed;
	float DownWalkSpeed;
	
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

DefComponent(DamageComponent)
{
	int DamageVal{0};				// ダメージ量
	ZWP<ECSEntity> DamagedEntity;	// ダメージを与えた本人
};

DefComponent(HackingComponent) // ハッキング
{
	int NumAccess; // 同時にアクセスしてるプレイヤー数
};

DefComponent(CureComponent) // 回復
{
	int NumCurePlayer; // 同時に回復させているプレイヤー数
};

#endif