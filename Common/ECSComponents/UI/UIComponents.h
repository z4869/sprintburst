#ifndef __UI_COMPONENTS_H__
#define __UI_COMPONENTS_H__

struct SpriteData
{
	SpriteData()
	{
		Color.Set(1, 1, 1, 1);
	}
	ZSP<ZTexture>	Tex;
	ZVec2			Size;	//	wid,hei
	ZVec4			Color;
	bool			BsAdd = false;
};

struct Button
{
	int					ButtonID;
	ZMatrix				Transform;
	//	ZSP<SpriteData>		Sprite;
	ZVector<ZSP<SpriteData>> Sprite;
	ZVec2				ButtonSize;
	bool				IsSelect;	//	一応
};

// ボタンコンポーネント
DefComponent(RadioButtonComponent)
{
	ZString GroupName = "";
	ZVector<ZSP<Button>> ButtonList;	//buttonName,data
	int		InitButtonID = 0;
	virtual void InitFromJson(const json11::Json& jsonObj);
};


DefComponent(AccessBarComponent) {
	virtual void InitFromJson(const json11::Json& jsonObj);
};



#endif