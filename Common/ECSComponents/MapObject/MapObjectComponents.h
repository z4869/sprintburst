#ifndef __MAPOBJECT_COMPONENTS_H__
#define __MAPOBJECT_COMPONENTS_H__

// セキュリティ
DefComponent(SecurityTerminalComponent)
{
	bool Enable;
	float Gage;		// 現在のゲージ

	Effekseer::Effect* NormalEffect;	// 通常時
	Effekseer::Effect* HackEffect;		// ハッキング完了時

	size_t EffectCnt;
	ZEffectManager::EffectHandle EffectHandle;

	static const float MaxGage; // ゲージが溜まりきる時間
	static const float DecreaseGage; // スキルチェック失敗時の減少分

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// サーバー
DefComponent(ServerTerminalComponent)
{
	bool Enable;
	int Gage;		// 現在のゲージ

	static const int MaxGage; // ゲージが溜まりきる時間

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// スポーンポイント
DefComponent(SpawnPointComponent)
{
};

#endif