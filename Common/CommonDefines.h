#ifndef __COMMON_DEFINES_H__
#define __COMMON_DEFINES_H__

constexpr uint MinHackerPlayer = 2;
constexpr uint MaxHackerPlayer = 4;
constexpr uint MinSecurityPlayer = 1;
constexpr uint MaxSecurityPlayer = 1;
constexpr uint MatchCountDownTime = 10;
constexpr float MatchingTimeLimit = 30; // マッチングのタイムリミット 300秒(5分)
constexpr float MatchStartTimeLimit = 30; // マッチ開始までのタイムリミット 30秒

#endif