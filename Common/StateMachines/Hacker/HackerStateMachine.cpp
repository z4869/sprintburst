#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "ECSComponents/Components.h"
#include "HackerStates.h"
#include "HackerStateMachine.h"

void HackerStateMachine::InitFromJson(const json11::Json & jsonObj)
{
}

void HackerStateMachine::OnDamage()
{
	// 生きていなければ
	if (m_IsAlive == false)return;
	
	((ZSP<HackerState>)m_NowState)->OnDamage();
	
}

void HackerStateMachine::StartAccess()
{
	// 生きていなければ
	if (m_IsAlive == false)return;

	((ZSP<HackerState>)m_NowState)->StartAccess();

}

void HackerStateMachine::StartDestroy()
{
	// 生きていなければ
	if (m_IsAlive == false)return;

	((ZSP<HackerState>)m_NowState)->StartDestroy();

}