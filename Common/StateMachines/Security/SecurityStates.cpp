#include "MainFrame/ZMainFrame.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "ECSComponents/Components.h"
#include "SecurityStateMachine.h"
#include "SecurityStates.h"

void SecurityState::SetMoveSpeed(float speed)
{
	auto entity = m_pStateMachine->GetEntity();
	auto playerComp = entity->GetComponent<PlayerComponent>();
	playerComp->Speed = speed;
}

void SecurityState_Wait::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Wait", 0, 10, true);
	SetMoveSpeed(0);
}

void SecurityState_Wait::Update(float delta)
{
	if (m_IsNext)return;

	auto& axis = m_pStateMachine->m_InputAxis;
	auto& button = m_pStateMachine->m_InputButton;
	if (axis.x != 0 || axis.z != 0)
	{
		// 歩き状態へ
		ChangeState("Walk");
		return;
	}

	auto stateMachine = ((SecurityStateMachine*)m_pStateMachine);
	if ((button & KeyMap::Access) && stateMachine->CreateWallMode == false)
	{
		// 攻撃前の為状態へ
		ChangeState("Attack_Sink");
		return;
	}
}

void SecurityState_Wait::OnChangeEvent()
{
	m_IsNext = false;
}

void SecurityState_Walk::Start()
{
	auto animator = m_pStateMachine->GetAnimator();
	animator->ChangeAnimeSmooth("Movement", 0, 10, true);
	auto securityComp = m_pStateMachine->GetEntity()->GetComponent<SecurityComponent>();
	SetMoveSpeed(securityComp->WalkSpeed);
}

void SecurityState_Walk::Update(float delta)
{
	if (m_IsNext)return;

	auto& axis = m_pStateMachine->m_InputAxis;
	auto& button = m_pStateMachine->m_InputButton;
	if (axis.x == 0 && axis.z == 0)
	{
		// 待ち状態へ
		ChangeState("Wait");
		return;
	}
	
	auto stateMachine = ((SecurityStateMachine*)m_pStateMachine);
	auto securityComp = stateMachine->GetEntity()->GetComponent<SecurityComponent>();
	if ((button & KeyMap::Access) && stateMachine->CreateWallMode == false)
	{
		// 攻撃前の為状態へ
		ChangeState("Attack_Sink");
		return;
	}
}

void SecurityState_Walk::OnChangeEvent()
{
	m_IsNext = false;
}

void SecurityState_Attack_Sink::Start()
{
	m_pAnimator = m_pStateMachine->GetAnimator();
	m_pAnimator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
	auto stateMachine = ((SecurityStateMachine*)m_pStateMachine);
	stateMachine->IsAttacking = true;
	SetMoveSpeed(0);
}

void SecurityState_Attack_Sink::Update(float delta)
{
	if (m_IsNext)return;

	auto& button = m_pStateMachine->m_InputButton;

	if (m_pAnimator->IsAnimationEnd() || !(button & KeyMap::Access_Stay))
	{
		auto entity = m_pStateMachine->GetEntity();
		auto securityComp = entity->GetComponent<SecurityComponent>();
		auto camComp = entity->GetComponent<CameraComponent>();

		camComp->Enable = false;

		// 攻撃へ
		ChangeState("Attack");
		return;
	}

}

void SecurityState_Attack_Sink::OnChangeEvent()
{
	m_IsNext = false;
}

void SecurityState_Attack::Start()
{
	m_pAnimator = m_pStateMachine->GetAnimator();
	m_pAnimator->ChangeAnimeSmooth("Attack", 0, 3, false);
	auto stateMachine = ((SecurityStateMachine*)m_pStateMachine);
	stateMachine->IsAttacking = true; // 一応
	SetMoveSpeed(0);
}

void SecurityState_Attack::Update(float delta)
{
	if (m_IsNext)return;

	if(m_pAnimator->IsAnimationEnd())
	{
		auto entity = m_pStateMachine->GetEntity();
		auto stateMachine = ((SecurityStateMachine*)m_pStateMachine);
		auto camComp = entity->GetComponent<CameraComponent>();
		camComp->Enable = true;

		// 攻撃が当たっていれば
		if (stateMachine->HitAttack)
		{
			ChangeState("Attack_Hit");
			stateMachine->HitAttack = false;
		}
		else
			ChangeState("Attack_NoHit");
		
		return;
	}

}

void SecurityState_Attack::OnChangeEvent()
{
	auto stateMachine = ((SecurityStateMachine*)m_pStateMachine);
	stateMachine->IsAttacking = false;
	m_IsNext = false;
}

void SecurityState_Attack_Hit::Start()
{
	m_pAnimator = m_pStateMachine->GetAnimator();
	m_pAnimator->ChangeAnimeSmooth("Attack(Hit)", 0, 3, false);
	SetMoveSpeed(0);
}

void SecurityState_Attack_Hit::Update(float delta)
{
	if (m_IsNext)return;

	if (m_pAnimator->IsAnimationEnd())
	{
		ChangeState("Wait");
		return;
	}

}

void SecurityState_Attack_Hit::OnChangeEvent()
{
	m_IsNext = false;
}

void SecurityState_Attack_NoHit::Start()
{
	m_pAnimator = m_pStateMachine->GetAnimator();
	m_pAnimator->ChangeAnimeSmooth("Attack(NoHit)", 0, 3, false);
	SetMoveSpeed(0);
}

void SecurityState_Attack_NoHit::Update(float delta)
{
	if (m_IsNext)return;

	if (m_pAnimator->IsAnimationEnd())
	{
		ChangeState("Wait");
		return;
	}
}

void SecurityState_Attack_NoHit::OnChangeEvent()
{
	m_IsNext = false;
}
