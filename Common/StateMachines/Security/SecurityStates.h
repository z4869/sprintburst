#ifndef __SECURITY_STATES_H__
#define __SECURITY_STATES_H__

#include "State/State.h"

class SecurityState : public StateBase
{
public:
	virtual void Start() = 0;
	virtual void Update(float delta) = 0;
	virtual void OnChangeEvent() = 0;

protected:
	void SetMoveSpeed(float speed);
};

class SecurityState_Wait : public SecurityState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

};

class SecurityState_Walk : public SecurityState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

};

class SecurityState_Attack_Sink : public SecurityState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	ZSP<ZAnimator> m_pAnimator;
};

class SecurityState_Attack : public SecurityState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	ZSP<ZAnimator> m_pAnimator;
};

class SecurityState_Attack_Hit : public SecurityState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	ZSP<ZAnimator> m_pAnimator;
};

class SecurityState_Attack_NoHit : public SecurityState
{
public:
	void Start();
	void Update(float delta);
	void OnChangeEvent();

private:
	ZSP<ZAnimator> m_pAnimator;
};

#endif