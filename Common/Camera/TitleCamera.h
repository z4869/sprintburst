#ifndef __TITLE_CAMERA_H__
#define __TITLE_CAMERA_H__

// ゲーム用にある程度機能実装したカメラ
class TitleCamera : public ZCamera {
public:
	TitleCamera();

	// 初期設定
	void Init(float rx, float ry, float camZoom);

	// 更新
	void Update();

	// セット
	void SetCamera();

public:
	ZMatrix m_LocalMat;
	ZMatrix m_BaseMat;

	ZVec2	m_MoveVal;
	static const int Limit = 5;

	ZVec2 m_Vec;
};


#endif
