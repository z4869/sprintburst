#include "MainFrame/ZMainFrame.h"
#include "Shader/ShaderManager.h"
#include "CommonSubSystems.h"

#include "TitleCamera.h"

TitleCamera::TitleCamera()
{
	m_LocalMat.CreateMove(0, 0, 0);
	m_BaseMat.CreateMove(0, 0, 0);
	SetPerspectiveFovLH(60, (float)ZDx.GetRezoW() / (float)ZDx.GetRezoH(), 0.01f, 200);

	POINT mouse_pos;
	GetCursorPos(&mouse_pos);
	INPUT.SetInitMousePos(mouse_pos);
}

void TitleCamera::Init(float rx, float ry, float camZoom)
{
	//m_LocalMat.CreateRotateX(rx);
	//m_LocalMat.RotateY(ry);
	//m_LocalMat.Move_Local(0, 0, camZoom);
	//m_BaseMat.CreateMove(0, 0, 0);
	m_BaseMat.CreateRotateX(rx);
	m_BaseMat.RotateY(ry);
	m_BaseMat.Move_Local(0, 0, camZoom);
	UpdateFrustumPlanes();

	m_MoveVal.Set(0.f, 0.f);
	m_Vec.Set(0, 0);
}

void TitleCamera::Update()
{

	// 視錐台平面更新
	UpdateFrustumPlanes();


	if (APP.m_Window->IsWindowActive())
	{
		//	
		static POINT befor_pt = INPUT.GetMousePos();
		POINT now_pt = INPUT.GetMousePos();

		m_Vec.x = (befor_pt.x - now_pt.x)*0.006f;
		m_Vec.y = (befor_pt.y - now_pt.y)*0.006f;

		m_MoveVal.x += m_Vec.x;
		m_MoveVal.y += m_Vec.y;


		if (m_MoveVal.x >= Limit) {
			m_MoveVal.x = Limit;
		}
		if (m_MoveVal.x <= -Limit) {
			m_MoveVal.x = -Limit;
		}

		if (m_MoveVal.y >= Limit) {
			m_MoveVal.y = Limit;
		}
		if (m_MoveVal.y <= -Limit) {
			m_MoveVal.y = -Limit;
		}

	//	// ホイールドラッグ
	//	if (INPUT.MouseStay(ZInput::BUTTON_MID)) {

	//		m_BaseMat.GetPos() -= mCam.GetXAxis()* (float)pt.x* 0.05f* ratio;
	//		m_BaseMat.GetPos() += mCam.GetYAxis()* (float)pt.y* 0.05f* ratio;

	//		m_BaseMat.SetLookAt(ZVec3(-0.3, 0.9, -4.0), ZVec3(0, 1, 0));
	//	}
		m_LocalMat = m_BaseMat;
		m_LocalMat.SetPos(m_BaseMat.GetPos());
		ZVec3 p(0, 1, -0.5f);
		p.x += m_MoveVal.x * -0.05f;
		p.y += m_MoveVal.y * -0.05f;

		ZMatrix tr;
		tr.CreateMove(p);
		tr.RotateY(m_MoveVal.x);
		tr.RotateX(-m_MoveVal.y);
		m_LocalMat *= tr;

		befor_pt = now_pt;

	}


}

void TitleCamera::SetCamera()
{
	// カメラ設定
	// 射影行列設定
	SetProj(m_mProj);

	// 最終的なカメラ行列を求める
//	mCam = m_LocalMat * m_BaseMat;
	m_mCam = m_LocalMat;

	// カメラ行列からビュー行列を作成
	CameraToView();

	ZCamera::LastCam = *this;

	// シェーダー側の定数バッファに書き込む
	ShMgr.UpdateCamera(this);
}