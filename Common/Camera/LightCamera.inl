inline void LightCamera::SetCameraDistance(float dis)
{
	m_Distance = dis;
}

inline void LightCamera::SetNearFar(float n, float f)
{
	m_Near = n;
	m_Far = f;
}

inline void LightCamera::SetCameraDirection(const ZVec3& dir)
{
	ZVec3 tmp = dir;
	tmp.Normalized();
	m_Direction = -tmp;	//	反転することでカメラ座標へ向かうベクトルに
}

inline void LightCamera::SetTargetPos(const ZVec3& pos)
{
	m_TarPos = pos;
}

inline void LightCamera::SetCamPos(const ZVec3& pos)
{
	m_mCam.SetPos(pos);
}