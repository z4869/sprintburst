#ifndef __COMMON_SUBSYSTEMS_H__
#define __COMMON_SUBSYSTEMS_H__

#define CTMgr GetSubSystem<ClosureTaskManager>()
#define ShMgr GetSubSystem<ShaderManager>()
#define LiMgr GetSubSystem<LightManager>()
#define RenderingPipeline GetSubSystem<ModelRenderingPipeline>()
#define ColEng GetSubSystem<CollisionEngine>()

#define FadeIO GetSubSystem<Fade>()

#endif