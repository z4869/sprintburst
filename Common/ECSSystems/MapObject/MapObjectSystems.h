#ifndef __MAPOBJECT_SYSTEMS_H__
#define __MAPOBJECT_SYSTEMS_H__

class SecurityTerminalUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,SecurityTerminalComponent,HackingComponent,AnimatorComponent)
public:
	SecurityTerminalUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

};

class ServerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ServerTerminalComponent,HackingComponent
		//SoundSetComponent,
		)
public:
	ServerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;
};

#endif