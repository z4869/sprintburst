#include "PCH/pch.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"

#include "ECSComponents/Components.h"
#include "MapObjectSystems.h"
#include "CommonSubSystems.h"
#include "MainSystems/GameWorld/GameWorld.h"

SecurityTerminalUpdateSystem::SecurityTerminalUpdateSystem()
{
	Init();
	m_DebugSystemName = "SecurityTerminalUpdateSystem";
}

void SecurityTerminalUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto securityComp = GetCompFromUpdateParam(SecurityTerminalComponent, components);
	auto hackingComp = GetCompFromUpdateParam(HackingComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// 無効時
	if (!securityComp->Enable) return;
	
	if (securityComp->EffectCnt == 0)
		securityComp->EffectHandle = EFFECT.SubmitPlayEffect(securityComp->NormalEffect, transComp->Transform.GetPos());
	securityComp->EffectCnt = (securityComp->EffectCnt + 1) % 162;//

	// アクセス数分ゲージ増加
	securityComp->Gage += delta * hackingComp->NumAccess;
	
}

void SecurityTerminalUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto securityComp = GetCompFromUpdateParam(SecurityTerminalComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);

	// セキュリティ解除
	if (securityComp->Enable &&
		securityComp->Gage >= SecurityTerminalComponent::MaxGage)
	{
		securityComp->Enable = false;
		animatorComp->Enable = true;
		animatorComp->Animator->ChangeAnimeSmooth("Lifted(Loop)", 0, 10, true);
		EFFECT.StopEffect(securityComp->EffectHandle);
		EFFECT.SubmitPlayEffect(securityComp->HackEffect, transComp->Transform.GetPos());
	}
}

ServerUpdateSystem::ServerUpdateSystem()
{
	Init();
	m_DebugSystemName = "ServerUpdateSystem";
}

void ServerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto serverComp = GetCompFromUpdateParam(ServerTerminalComponent, components);
	auto hackingComp = GetCompFromUpdateParam(HackingComponent, components);
	//auto soundComp = GetCompFromUpdateParam(SoundSetComponent, components);

	// 無効時
	//if (!serverComp->Enable) 
	//{ 
	//	soundComp->Stop("securityTerminal");
	//	return; 
	//}

	//if (!soundComp->IsPlay("securityTerminal")) {
	//	soundComp->Play3D("securityTerminal", transComp->Transform.GetPos());
	//}

	// アクセス数分ゲージ増加
	serverComp->Gage += delta * hackingComp->NumAccess;
	
}

void ServerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{

	auto serverComp = GetCompFromUpdateParam(ServerTerminalComponent, components);
#if _DEBUG
	/*-----------------------------------------------------------------------------------------*/
	
	DW_STATIC(8, "ServerGage: %d", serverComp->Gage);
	//	デバッグ用のサーバーアクセス
	if (GW.m_GameState.AccesServer)
	{
		if (INPUT.KeyStay(VK_SPACE))
			serverComp->Gage++;
	}
	/*-----------------------------------------------------------------------------------------*/
#endif
	
	// ダウンロード完了
	if (serverComp->Enable &&
		serverComp->Gage >= ServerTerminalComponent::MaxGage)
	{

		serverComp->Enable = false;
		
		//	ゲームオーバーじゃなければクリア
		if (!GW.m_GameState.SecurityWin)
			GW.m_GameState.HackerWin = true;
	}

}
