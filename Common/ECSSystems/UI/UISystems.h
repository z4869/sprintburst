#ifndef __UI_SYSTEMS_H__
#define __UI_SYSTEMS_H__

class TitleMenuUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(RadioButtonComponent)
public:
	TitleMenuUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

private:
	bool	CheckMauseOver(const ZMatrix& mat, const ZVec2& size);

private:
	static const int UNSELECTED = -1;
	enum
	{
		UP = -1,
		DOWN = 1
	};
	
	// 起動直前にキーを押していた場合に起こる誤動作防止用
	bool m_IsEndOffsetWait;
	bool m_IsPressedEnter;

	int BeforSelectID = UNSELECTED;
	ZSP<ZSound>		SE;
};


class RadioButtonDrawSystem :public ECSSystemBase
{
	DefUseComponentType(RadioButtonComponent)
public:
	RadioButtonDrawSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};



#endif