#pragma once

class HackerCheckSystem : public ECSSystemBase {
	DefUseComponentType(HackerComponent)

public:
	HackerCheckSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

private:
	bool IsCheck = false;
	int HackerCnt = 0;
#if _DEBUG
	bool FrameKill = false;
#endif
};

//	処理的にはSecurityTerminalUpdateSystemで行えばいいけど、下手に触りたくないんでここに
class SecurityCheckSystem : public ECSSystemBase {
	DefUseComponentType(SecurityTerminalComponent, AnimatorComponent)

public:
	SecurityCheckSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

private:
	bool IsCheck = false;
	int SecCnt = 0;
	int EnableSecCnt = 0;

#if _DEBUG
	bool FrameKill = false;
#endif
};
