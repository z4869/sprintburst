﻿#ifndef __SECURITY_UPDATE_SYSTEM_H__
#define __SECURITY_UPDATE_SYSTEM_H__

class SecurityPreUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,SecurityComponent,PlayerComponent, PlayerControllerComponent, StateMachineComponent, VelocityComponent)

public:
	SecurityPreUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	//
	void SpawnWall(UpdateCompParams components);

};

class SecurityMoveSystem : public ECSSystemBase
{
	DefUseComponentType(SecurityComponent, TransformComponent, CameraComponent, VelocityComponent, PlayerComponent, PlayerControllerComponent)

public:
	SecurityMoveSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

class SecurityUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, ModelBoneControllerComponent,
						PlayerComponent, PlayerControllerComponent, SecurityComponent,AnimatorComponent,
						CameraComponent, ColliderComponent,VelocityComponent,StateMachineComponent)
public:
	SecurityUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

private:
	//
	void WallMoveAndInstallation(UpdateCompParams components);

	void Script_Attack(ModelBoneControllerComponent* bcComp,StateMachineComponent* stateMachineComp, SecurityComponent* securityComp,
					   ColliderComponent* colliderComp, json11::Json& scrItem);

private:

	ZSP<ECSEntity> m_WallEntity;
	GameModelComponent* m_WallModelComponent;
	ColliderComponent* m_WallColliderComponent;
	ZSP<ZSound>		hit_SE;
};

#endif