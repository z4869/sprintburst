#ifndef __PLAYER_UPDATE_SYSTEM_H__
#define __PLAYER_UPDATE_SYSTEM_H__

// プレイヤーの更新
class PlayerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(PlayerComponent, CameraComponent, PlayerControllerComponent)

public:
	PlayerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

};


#endif