#ifndef __HACKER_UPDATE_SYSTEM_H__
#define __HACKER_UPDATE_SYSTEM_H__

#include "MainSystems/CollisionEngine/CollisionEngine.h"

class HackerPreUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(HackerComponent, PlayerControllerComponent, StateMachineComponent,VelocityComponent)
public:
	HackerPreUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	
};

class HackerCureSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,HackerComponent, CureComponent,StateMachineComponent)
public:
	HackerCureSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

class HackerMoveSystem : public ECSSystemBase
{
	DefUseComponentType(HackerComponent,TransformComponent,CameraComponent,VelocityComponent,PlayerComponent,PlayerControllerComponent)
public:
	HackerMoveSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

class HackerDamageSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,HackerComponent,StateMachineComponent,DamageComponent,ColliderComponent,PlayerComponent)
public:
	HackerDamageSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};

class HackerStateMachine;

class HackerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,ModelBoneControllerComponent,
						PlayerComponent, PlayerControllerComponent, HackerComponent,AnimatorComponent,
						CameraComponent,ColliderComponent, VelocityComponent,StateMachineComponent)

public:
	HackerUpdateSystem();
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

private:
	// 端末と接触しているか
	void ChackHitEnterTerminal(HitResData::Node& node,HackerStateMachine* hackerStateMachine,HackerComponent* hackerComp);

	// 他のダウンしているプレイヤーと接触しているか
	void ChackHitEnterDownPlayer(HitResData::Node& node, HackerStateMachine* hackerStateMachine, HackerComponent* hackerComp);

	void DestroyObjMode(UpdateCompParams components);

	Effekseer::Effect* m_DestroyEffect;
};


#endif