﻿#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "ECSComponents/Common/CommonComponents.h"
#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "StateMachines/Security/SecurityStateMachine.h"
#include "SubSystems.h"
#include "CommonSubSystems.h"

#include "SecurityUpdateSystem.h"


SecurityPreUpdateSystem::SecurityPreUpdateSystem()
{
	Init();
	m_DebugSystemName = "SecurityPreUpdateSystem";

}

void SecurityPreUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	auto& stateMachine = stateMachineComp->pStateMachine;
	stateMachine->m_InputAxis = playerContComp->Axis;
	stateMachine->m_InputButton = playerContComp->Button;
	
	auto securityStateMachine = (SecurityStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	// 攻撃中じゃなければ
	if (securityStateMachine->IsAttacking == false)
		SpawnWall(components);

	velocityComp->Velocity = ZVec3(0);
}


void SecurityPreUpdateSystem::SpawnWall(UpdateCompParams components)
{
	auto securityComp = GetCompFromUpdateParam(SecurityComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);

	auto securityStateMachine = (SecurityStateMachine*)stateMachineComp->pStateMachine.GetPtr();


	if (playerContComp->Button & KeyMap::Action)
	{
		// 壁が最大数設置されていれば設置されている物のうち、
		// 最初に設置したものを削除し新たに生成し直す
		if (securityComp->CreateWallCnt >= SecurityComponent::MaxNumCreateWall)
		{
			securityComp->WallEntityList.front()->Remove();
			securityComp->WallEntityList.pop_front();
			securityComp->CreateWallCnt--;
		}

		// 壁生成モード切り替え
		securityStateMachine->CreateWallMode = !securityStateMachine->CreateWallMode;

		if (securityStateMachine->CreateWallMode == true &&
			securityComp->WallEntity == nullptr)
		{
			TransformComponent* wallTransComp = ECS.MakeComponent<TransformComponent>();
			wallTransComp->Transform = transComp->Transform;

			GameModelComponent* wallModelComp = ECS.MakeComponent<GameModelComponent>();
			wallModelComp->Model = securityComp->WallModel;
			wallModelComp->RenderFlg.SemiTransparent = true;

			ColliderComponent* wallColliderComp = ECS.MakeComponent<ColliderComponent>();
			wallColliderComp->m_pColliders = Make_Unique(ZVector<ColliderComponent::ColliderData>, appnew);
			// 当たり判定用モデル
			wallColliderComp->m_pColliders->emplace_back();
			auto& colliderData = wallColliderComp->m_pColliders->back();
			colliderData.Name = "Wall";
			colliderData.Collider = Make_Shared(Collider_Mesh, appnew);
			((ZSP<Collider_Mesh>)colliderData.Collider)->AddMesh(securityComp->WallModel);
			colliderData.Collider->Init(0,
										0,// 判定する側時のマスク
										HitShapes::ALL, // 形状マスク
										HitGroups::_8 // 判定される側時のマスク
			);
			colliderData.AutoRegister = false;

			auto wallEntity = ECS.MakeEntity(wallTransComp, wallModelComp, wallColliderComp);

			GW.m_Entities.push_back(wallEntity);
			securityComp->WallEntity = wallEntity;
			securityComp->WallEntityList.push_back(wallEntity);
		}
		else if (securityStateMachine->CreateWallMode == false &&
				 securityComp->WallEntity != nullptr)
		{
			// 設置せずに戻った場合削除
			if (securityComp->WallEntityList.empty() == false)
			{
				securityComp->WallEntity->Remove();
				securityComp->WallEntity = nullptr;
				securityComp->WallEntityList.pop_back();
			}
			//auto wallModelComp = killerComp->WallEntity->GetComponent<GameModelComponent>();
		}

	}
}

SecurityMoveSystem::SecurityMoveSystem()
{
	Init();
	m_DebugSystemName = "SecurityMoveSystem";
}

void SecurityMoveSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = camComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();

	ZVec3 vZ = cam.m_LocalMat.GetZAxis();
	vZ.y = 0;
	mat.SetLookTo(vZ, ZVec3::Up);
	velocityComp->Velocity += vTar * playerComp->Speed * delta;
	
	
	mat.Move(velocityComp->Velocity);
}

SecurityUpdateSystem::SecurityUpdateSystem()
{
	Init();
	m_DebugSystemName = "SecurityUpdateSystem";
	auto sound = APP.m_ResStg.LoadSound("data/Sound/se/punch_hit/punch_hit.wav");
	hit_SE = sound->CreateInstance(false);
}

void SecurityUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto securityComp = GetCompFromUpdateParam(SecurityComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);

	auto securityStateMachine = (SecurityStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	// 多段ヒットリストの寿命処理
	for (auto it = securityComp->MultiHitMap.begin(); it != securityComp->MultiHitMap.end();)
	{
		it->second--;
		if (it->second <= 0)
		{
			it = securityComp->MultiHitMap.erase(it);
			continue;
		}

		++it;
	}

	// スクリプトキー時に実行される関数
	auto scriptProc = [this, bcComp, stateMachineComp, securityComp, colliderComp](ZAnimeKey_Script* scr)
	{

		// 文字列をJsonとして解析
		std::string errorMsg;
		json11::Json jsonObj = json11::Json::parse(scr->Value.c_str(), errorMsg);
		if (errorMsg.size() > 0)
		{
			DW_SCROLL(2, "JsonError（%s）", errorMsg.c_str());
			return;
		}

		auto scriptArray = jsonObj["Scripts"].array_items();
		for (auto& scrItem : scriptArray)
		{
			// スクリプトの種類
			std::string scrType = scrItem["Type"].string_value();
			// 攻撃判定発生
			if (scrType == "Attack")
				Script_Attack(bcComp, stateMachineComp, securityComp, colliderComp, scrItem);
			else if (scrType == "FootStep")
			{
				// 足音
			}

		}

	};

	animatorComp->ScriptProc = scriptProc;
	
	if (securityStateMachine->IsAttacking == false)
	{
		if (securityStateMachine->CreateWallMode)
			WallMoveAndInstallation(components);
	}

	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::ALL,
			HitGroups::_0);

		const float startY = 1;
		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, startY, 0),	// レイの開始位置
			pos + ZVec3(0, -10, 0));		// レイの終了位置

		ColEng.Test(hitObj.GetPtr());
		if (hitObj->m_HitState | HitStates::STAY)
		{
			float nearestDist = FLT_MAX;
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (transComp->m_Entity == entity)continue;
					if (nearestDist > node.RayDist)
						nearestDist = node.RayDist;
				}
			}

			if (nearestDist < startY && velocityComp->Velocity.y <= 0)
			{
				transComp->Transform._42 += (startY - nearestDist);
				playerComp->IsSky = false;
			}
			else if (nearestDist > startY + 0.3f)
				playerComp->IsSky = true;

		}
	}

}

void SecurityUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);

	// キャラの頭の座標を使用
	auto headLocalMat = bcComp->BoneController.SearchBone("頭")->LocalMat;
	auto headMat = headLocalMat * transComp->Transform;
	ZMatrix m;
	m = camComp->Cam.m_BaseMat;
	m.SetPos(headMat.GetPos());
	camComp->Cam.m_mCam = camComp->Cam.m_LocalMat * m;
	camComp->Cam.m_mView = camComp->Cam.m_mCam.Inversed();
	camComp->Cam.UpdateFrustumPlanes();
}

void SecurityUpdateSystem::WallMoveAndInstallation(UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto securityComp = GetCompFromUpdateParam(SecurityComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);

	auto securityStateMachine = (SecurityStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	// 壁設置処理
	if (playerContComp->Button & KeyMap::Access)
	{
		// 当たり判定用モデル
		auto wallColliderComp = securityComp->WallEntity->GetComponent<ColliderComponent>();
		auto wallCollider = wallColliderComp->FindCollider("Wall");
		if (wallCollider)
			wallCollider->AutoRegister = true;

		securityComp->CreateWallCnt++;
		securityComp->WallEntity = nullptr;
		securityStateMachine->CreateWallMode = false;

		return;
	}

	// 壁移動
	auto wallTransComp = securityComp->WallEntity->GetComponent<TransformComponent>();
	wallTransComp->Transform = transComp->Transform;
	wallTransComp->Transform.Move_Local(0, 0, 1);
}


void SecurityUpdateSystem::Script_Attack(ModelBoneControllerComponent* bcComp, StateMachineComponent* stateMachineComp, SecurityComponent* securityComp,
										 ColliderComponent* colliderComp, json11::Json& scrItem)
{
	colliderComp->m_pColliders->emplace_back();
	auto& colliderData = colliderComp->m_pColliders->back();
	colliderData.Name = "Attack";
	auto collider = Make_Shared(Collider_Sphere, appnew);
	collider->Init(0, HitGroups::_5, HitShapes::SPHERE, HitGroups::_5);
	colliderData.Collider = collider;
	colliderData.AutoRegister = true;
	colliderData.Life = scrItem["Time"].int_value();
	colliderData.TargetBoneIndex = bcComp->BoneController.SearchBoneIndex(scrItem["BoneName"].string_value().c_str());
	
	// スフィア情報
	colliderData.Size = ZVec3((float)scrItem["Radius"].number_value());
	
	// 自分のEntityのアドレスを仕込んでおく
	collider->m_UserMap["Entity"] = colliderComp->m_Entity;

	// ボーンノードを仕込む
	collider->m_UserMap["TargetBone"] = bcComp->BoneController.GetBoneTree()[colliderData.TargetBoneIndex];

	// デバッグ用の色
	collider->m_Debug_Color.Set(1, 0, 0, 1);

	int hitInterval = scrItem["HitInterval"].int_value();

	collider->m_OnHitStay =
		[this,hitInterval,securityComp,stateMachineComp](const ZSP<ColliderBase>& hitObj)
	{
		hitObj->m_HitResTbl;
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// 自分自身は無視
				if (securityComp->m_Entity == youEntity)continue;

				// すでにヒットしているか
				const ZUUID& entityUUID = youEntity->GetUUID();
				if (securityComp->MultiHitMap.count(entityUUID) != 0)continue;

				// ハッカー以外なら
				if (youEntity->HasComponent<HackerComponent>() == false)
					continue;


				// ハッカーにダメージコンポーネント追加
				DamageComponent* damageComp = ECS.MakeComponent<DamageComponent>();
				damageComp->DamageVal = securityComp->AttackPower;
				damageComp->DamagedEntity = securityComp->m_Entity;
				youEntity->AddComponent(damageComp);

				
				hit_SE->Play();

				auto securityStateMachine = (SecurityStateMachine*)stateMachineComp->pStateMachine.GetPtr();
				securityComp->MultiHitMap[entityUUID] = hitInterval;
				securityStateMachine->HitAttack = true;

			}
		}
	};


}

