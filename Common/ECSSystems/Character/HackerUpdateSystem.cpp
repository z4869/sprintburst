#include "MainFrame/ZMainFrame.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "ECSComponents/Components.h"
#include "StateMachines/Hacker/HackerStateMachine.h"

#include "SubSystems.h"
#include "CommonSubSystems.h"

#include "HackerUpdateSystem.h"

HackerPreUpdateSystem::HackerPreUpdateSystem()
{
	Init();
	m_DebugSystemName = "HackerPreUpdateSystem";
}

void HackerPreUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	hackerStateMachine->m_InputAxis = playerContComp->Axis;
	hackerStateMachine->m_InputButton = playerContComp->Button;

	// しゃがみ
	if (playerContComp->Button & KeyMap::Squat)
		hackerStateMachine->m_SquatFlag = true;
	else
		hackerStateMachine->m_SquatFlag = false;

	velocityComp->Velocity = ZVec3(0);

}

HackerCureSystem::HackerCureSystem()
{
	Init();
	m_DebugSystemName = "HackerCureSystem";
}

void HackerCureSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);
	auto cureComp = GetCompFromUpdateParam(CureComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	hackerComp->CureGage += delta * cureComp->NumCurePlayer;
	if (hackerComp->CureGage < hackerComp->MaxCure)return;

	hackerStateMachine->m_FinishCure = true;
	hackerComp->CureGage = 0;
	hackerComp->m_Entity->RemoveComponent<CureComponent>();
}

HackerMoveSystem::HackerMoveSystem()
{
	Init();
	m_DebugSystemName = "HackerMoveSystem";
}

void HackerMoveSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = camComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	if (vTar.Length() > 0)
	{
		// カメラの向いている方向へキャラ回転
		ZVec3 vZ = mat.GetZAxis();
		vZ.Homing(vTar, 20);
		mat.SetLookTo(vZ, ZVec3::Up);
		velocityComp->Velocity += vTar * playerComp->Speed * delta;
	}

	mat.Move(velocityComp->Velocity);

}

HackerDamageSystem::HackerDamageSystem()
{
	Init();
	m_DebugSystemName = "HackerDamageSystem";
}

void HackerDamageSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);
	auto stateMacheneComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto damageComp = GetCompFromUpdateParam(DamageComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMacheneComp->pStateMachine.GetPtr();
	auto effectPos = transComp->Transform.GetPos();
	effectPos.y += 0.01f;
	EFFECT.SubmitPlayEffect(hackerComp->DamageEffect, effectPos);

	if (hackerStateMachine->m_IsDown == false)
	{
		hackerStateMachine->OnDamage();
		hackerComp->HP -= damageComp->DamageVal;
		if (hackerStateMachine->m_IsDown)
		{
			auto cureCollider = colliderComp->FindCollider("治療");
			cureCollider->AutoRegister = true;
		}
	}

	hackerComp->m_Entity->RemoveComponent<DamageComponent>();
}

HackerUpdateSystem::HackerUpdateSystem()
{
	Init();
	m_DebugSystemName = "HackerUpdateSystem";
	m_DestroyEffect = EFFECT.LoadEffect("data/Effect/Obj_Break/Destroy.efk");
}

void HackerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto velocityComp = GetCompFromUpdateParam(VelocityComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();


	// スクリプトキー時に実行される関数
	auto scriptProc = [this, bcComp,transComp, hackerComp,hackerStateMachine](ZAnimeKey_Script* scr)
	{
		// 文字列をJsonとして解析
		std::string errorMsg;
		json11::Json jsonObj = json11::Json::parse(scr->Value.c_str(), errorMsg);
		if (errorMsg.size() > 0)
		{
			DW_SCROLL(2, "JsonError（%s）", errorMsg.c_str());
			return;
		}

		auto scriptArray = jsonObj["Scripts"].array_items();
		for (auto& scrItem : scriptArray)
		{
			// スクリプトの種類
			std::string scrType = scrItem["Type"].string_value();

			if (scrType == "FootStep")
			{
				auto model = bcComp->BoneController.GetGameModel();
				auto& half = model->GetAABB_HalfSize();
				ZVec3 effectPos = ZVec3(
					RAND.GetFloat(-half.x, half.x),
					RAND.GetFloat(-half.y, half.y),
					RAND.GetFloat(-half.z, half.z));

				effectPos *= ZVec3(0.8f, 1, 0.8f);
				effectPos += ZVec3(0, half.y, 0);
				effectPos.Transform(transComp->Transform);

				// 10分の1の確率で追跡用の痕跡エフェクト発生
				if (RAND.GetInt() % 10 > 0)continue;

				if ((hackerStateMachine->m_InputButton & KeyMap::Run) &&
					hackerStateMachine->m_InputAxis.Length() > 0)
					EFFECT.SubmitPlayEffect(hackerComp->TraceEffect, effectPos);

			}

		}

	};
	animatorComp->ScriptProc = scriptProc;


	// オブジェクト破壊モード バグるのでなし
	//if (hackerStateMachine->m_DestroyModeFlg)
	//	DestroyObjMode(components);
	//else if (hackerStateMachine->m_SquatFlag == false &&
	//		 hackerStateMachine->m_IsDown == false)
	if(hackerStateMachine->m_SquatFlag == false &&
	   hackerStateMachine->m_IsDown == false)
	{
		// ハッキング
		if (hackerStateMachine->m_InputButton & KeyMap::Access)
		{
			auto accessCollider = colliderComp->FindCollider("アクセス");
			ColEng.Test(accessCollider->Collider.GetPtr());
			auto& collider = accessCollider->Collider;
			if (collider->m_HitState | HitStates::STAY)
			{
				for (auto& res : collider->m_HitResTbl)
				{
					for (auto& node : res.HitDataList)
					{
						ChackHitEnterTerminal(node, hackerStateMachine, hackerComp);
						ChackHitEnterDownPlayer(node, hackerStateMachine, hackerComp);
					}
				}
				collider->m_HitResTbl.clear();
			}

		}
		else if (!(hackerStateMachine->m_InputButton & KeyMap::Access_Stay))
		{
			auto terminal = hackerComp->AccessTerminal.Lock();
			if (terminal)
			{
				auto hackingComp = terminal->GetComponent<HackingComponent>();
				if(hackingComp)
				{
					hackingComp->NumAccess--;
					if (hackingComp->NumAccess <= 0)
						terminal->RemoveComponent<HackingComponent>();
					
				}
				hackerComp->AccessTerminal.Reset();
				hackerStateMachine->m_AccessFlag = false;
			}

			auto curePlayer = hackerComp->CurePlayer.Lock();
			if (curePlayer)
			{
				auto cureComp = curePlayer->GetComponent<CureComponent>();
				if (cureComp)
				{
					cureComp->NumCurePlayer--;
					if (cureComp->NumCurePlayer <= 0)
						curePlayer->RemoveComponent<CureComponent>();
				}
				hackerComp->CurePlayer.Reset();
				hackerStateMachine->m_AccessFlag = false;
			}

		}
	}

	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::ALL,
			HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		const float startY = 1;
		hitObj->Set(pos + ZVec3(0, startY, 0),	// レイの開始位置
			pos + ZVec3(0, -10, 0));		// レイの終了位置

		ColEng.Test(hitObj.GetPtr());
		
		if (hitObj->m_HitState | HitStates::STAY)
		{
			float nearestDist = FLT_MAX;

			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (transComp->m_Entity == entity)continue;

					if (nearestDist > node.RayDist)
						nearestDist = node.RayDist;
				}
			}

			
			if (nearestDist < startY && velocityComp->Velocity.y <= 0)
			{
				transComp->Transform._42 += (startY - nearestDist);
				playerComp->IsSky = false;
			}
			else if (nearestDist > startY + 0.3f)
				playerComp->IsSky = true;
		}
	}

	// カメラ座標更新
	ZMatrix m;
	m = camComp->Cam.m_BaseMat;
	m.Move(transComp->Transform.GetPos());
	
	camComp->Cam.m_mCam = camComp->Cam.m_LocalMat * m;
	camComp->Cam.m_mView = camComp->Cam.m_mCam.Inversed();
	camComp->Cam.UpdateFrustumPlanes();

	// ダウン時はHPを無条件に減らす(毎秒1ずつ)
	if (hackerStateMachine->m_IsDown)
	{
		if (hackerComp->CureGage >= hackerComp->MaxCure)
			hackerStateMachine->m_FinishCure = true;

		if (hackerComp->HP <= 0)
		{
			hackerStateMachine->m_IsAlive = false;
			return;
		}
		hackerComp->HP -= delta;

	}

	// 負傷していなければエフェクトなし
	if (hackerStateMachine->m_IsInjury == false)
		return;
	
	// 負傷時のエフェクト表示 改良予定
	hackerComp->DamageEffectTime++;
	if (hackerComp->DamageEffectTime > 100) //
	{
		hackerComp->DamageEffectTime = 0;

		auto model = bcComp->BoneController.GetGameModel();

		auto effectFunc = [this, hackerComp, transComp, model](const Effekseer::Handle handle, Effekseer::Manager* manager)mutable
		{
			auto& half = model->GetAABB_HalfSize();
			ZVec3 effectPos = ZVec3(
				RAND.GetFloat(-half.x,half.x),
				RAND.GetFloat(-half.y,half.y),
				RAND.GetFloat(-half.z,half.z));

			effectPos *= ZVec3(0.8f, 1, 0.8f);
			effectPos += ZVec3(0, half.y, 0);
			hackerComp->DamageEffectPos[handle] = effectPos;
			effectPos.Transform(transComp->Transform);
			manager->SetLocation(handle, Effekseer::Vector3D(effectPos.x, effectPos.y, effectPos.z));
		};

		auto h = EFFECT.SubmitPlayEffect(hackerComp->InjuryEffect, effectFunc);
	}

}

void HackerUpdateSystem::ChackHitEnterTerminal(HitResData::Node& node, HackerStateMachine* hackerStateMachine, HackerComponent* hackerComp)
{
	// 他のプレイヤーを回復させていれば
	if (hackerComp->CurePlayer)return;

	auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
	auto securityTerminalComp = entity->GetComponent<SecurityTerminalComponent>();
	auto serverTerminalComp = entity->GetComponent<ServerTerminalComponent>();
	auto hackingComp = entity->GetComponent<HackingComponent>();
	bool isAccess = false;

	if (securityTerminalComp)
	{
		if (securityTerminalComp->Gage <= securityTerminalComp->MaxGage)
			isAccess = true;
	}
	else if (GW.m_GameState.AccesServer && serverTerminalComp)
	{
		if (serverTerminalComp->Gage <= serverTerminalComp->MaxGage)
			isAccess = true;
	}

	if (isAccess == false)return;

	hackerComp->AccessTerminal = entity;
	hackerStateMachine->StartAccess();
	
	// アクセスしたなら
	if (hackingComp)
		hackingComp->NumAccess++;
	else
	{
		hackingComp = ECS.MakeComponent<HackingComponent>();
		hackingComp->NumAccess++;
		entity->AddComponent(hackingComp);
	}
	
}

void HackerUpdateSystem::ChackHitEnterDownPlayer(HitResData::Node& node, HackerStateMachine* hackerStateMachine, HackerComponent* hackerComp)
{
	// 端末にアクセス中なら無視
	if (hackerComp->AccessTerminal)return;

	auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
	auto otherHackerComp = entity->GetComponent<HackerComponent>();
	auto cureComp = entity->GetComponent<CureComponent>();

	if (otherHackerComp == nullptr) return;

	auto stateMachineComp = entity->GetComponent<StateMachineComponent>();
	auto otherHackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();
	if (otherHackerStateMachine->m_IsDown == false)return; // ダウンしていなければ

	hackerComp->CurePlayer = entity;
	hackerStateMachine->StartAccess(); // アクセス

	// アクセスしたなら
	if (otherHackerComp)
		cureComp->NumCurePlayer++;
	else
	{
		cureComp = ECS.MakeComponent<CureComponent>();
		cureComp->NumCurePlayer++;
		entity->AddComponent(cureComp);
	}
}

void HackerUpdateSystem::DestroyObjMode(UpdateCompParams components)
{
	auto camComp = GetCompFromUpdateParam(CameraComponent, components);
	auto hackerComp = GetCompFromUpdateParam(HackerComponent, components);
	auto stateMachineComp = GetCompFromUpdateParam(StateMachineComponent, components);

	auto hackerStateMachine = (HackerStateMachine*)stateMachineComp->pStateMachine.GetPtr();

	if (hackerStateMachine->m_InputButton & KeyMap::Access)
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
					 HitGroups::_0,
					 HitShapes::ALL,
					 HitGroups::_0);

		ZVec3 startPos, endPos;
		startPos = camComp->Cam.m_mCam.GetPos();
		endPos = startPos + camComp->Cam.m_mCam.GetZAxis() * 100;
		hitObj->Set(startPos,endPos);

		ColEng.Test(hitObj.GetPtr());

		ZSP<ECSEntity> nearestObj;
		if (hitObj->m_HitState | HitStates::STAY)
		{
			float nearestDist = FLT_MAX;

			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					auto entity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					auto modelComp = entity->GetComponent<GameModelComponent>();
					if (modelComp == nullptr)return;
					if (hackerComp->m_Entity == entity)continue;
					if (modelComp->RenderFlg.DestoryObject == false)continue;

					if (nearestDist > node.RayDist)
					{
						nearestDist = node.RayDist;
						nearestObj = entity;
					}
				}
			}
		}
		if (nearestObj)
		{
			hackerComp->DestroyObj = nearestObj;
			hackerStateMachine->StartDestroy();
		}

	}

	
}
