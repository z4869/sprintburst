#ifndef __FADE_H__
#define __FADE_H__

//	フェードIN,OUT　簡易的なものなので細かい事はノー
class Fade
{
private:
	static const int DEFAULT_FADE_TIME = 1;		//	1秒でフェードをデフォルトに

public:
	Fade();
	~Fade();

	void Release();

	//==================================================================
	//	更新描画
	//==================================================================
	void Update();		//	終了でFALSE
	void Draw();
	void ChangeFadeTime(float time);

	//	開始
	bool StartFadeIn(float time = DEFAULT_FADE_TIME);
	bool StartFadeOut(float time = DEFAULT_FADE_TIME);
	//	終了
	void EndFade();
	
	bool IsActive();
	
	void SetFadeTexure(const ZSP<ZTexture> tex);
	void SetExecuteEndFunction(std::function<void()> func);

private:
	bool UpdateFadeOut();
	bool UpdateFadeIn();
	float CalcFadeValue();

private:
	bool m_IsActive;

	std::function<bool()> FadeFunction = nullptr;

	//	フェードの終了時に実行する関数
	std::function<void()> m_ExecutionEndFunciton = nullptr;

	ZTimer			m_Timer;
	std::chrono::duration<double> m_FadeElapsedTime;
	float			m_FadeTime;		//	何秒でフェードさせるか
	ZSP<ZTexture>	m_TexFade;
	float			m_Alpha;
	bool			m_OnEndFade;
};

#endif