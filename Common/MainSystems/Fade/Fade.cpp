#include "MainFrame/ZMainFrame.h"
#include "Shader/ShaderManager.h"
#include "CommonSubSystems.h"
#include "Fade.h"

Fade::Fade() : m_Timer(m_FadeElapsedTime)
{
	ChangeFadeTime(DEFAULT_FADE_TIME);
	m_TexFade = ZDx.GetBlackTex();
	m_OnEndFade = false;
}

Fade::~Fade()
{
	Release();
}

void Fade::Release()
{
	m_TexFade = nullptr;
}

void Fade::ChangeFadeTime(float time)
{
	m_FadeTime = time;
}

void Fade::Update()
{
	if (m_IsActive == false)return;

	if (FadeFunction)FadeFunction();
}

void Fade::Draw()
{
	if (m_IsActive == false)return;

	if (!m_TexFade)
		m_TexFade = ZDx.GetBlackTex();

	ZVec4 Col(1, 1, 1, m_Alpha);
	float w = (float)ZDx.GetRezoW();
	float h = (float)ZDx.GetRezoH();
	auto& sr = ShMgr.GetRenderer<SpriteRenderer>();
	sr.Draw2D(m_TexFade->GetTex(), 0, 0, w, h, &Col);

	if (m_OnEndFade)EndFade();
}

bool Fade::StartFadeIn(float time)
{
	if (m_IsActive)return false;

	ChangeFadeTime(time);
	m_Alpha = 1.f;
	m_IsActive = true;
	m_Timer.Start();
	FadeFunction = std::bind(&Fade::UpdateFadeIn, this);

	return true;
}

bool Fade::StartFadeOut(float time)
{
	if (m_IsActive)return false;
	
	ChangeFadeTime(time);
	m_Alpha = 0.f;
	m_IsActive = true;
	m_Timer.Start();
	FadeFunction = std::bind(&Fade::UpdateFadeOut, this);

	return true;
}

void Fade::EndFade()
{
	FadeFunction = nullptr;
	m_IsActive = false;
	m_OnEndFade = false;
	if (m_ExecutionEndFunciton) m_ExecutionEndFunciton();
	m_ExecutionEndFunciton = nullptr;
}

bool Fade::IsActive()
{
	return m_IsActive;
}

void Fade::SetFadeTexure(const ZSP<ZTexture> tex)
{
	m_TexFade = tex;
}

void Fade::SetExecuteEndFunction(std::function<void()> func)
{
	m_ExecutionEndFunciton = func;
}

bool Fade::UpdateFadeOut()
{
	m_Timer.Stop();
	m_Alpha += CalcFadeValue();
	m_Timer.Start();
	if (m_Alpha < 1.f)
		return true;
	m_Alpha = 1;
	m_OnEndFade = true;
	m_Timer.Stop();
	return false;
}

bool Fade::UpdateFadeIn()
{
	m_Timer.Stop();
	m_Alpha -= CalcFadeValue();
	m_Timer.Start();
	if (m_Alpha > 0.f)
		return true;
	m_Alpha = 0;
	m_OnEndFade = true;
	m_Timer.Stop();
	return false;
}

float Fade::CalcFadeValue()
{
	// �o�ߎ���
	float elapsedTime = (float)m_FadeElapsedTime.count();
	return elapsedTime / m_FadeTime;
}
