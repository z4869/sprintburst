#ifndef __CLOSURETASKMANAGER_H__
#define __CLOSURETASKMANAGER_H__

// 時間制限付き関数オブジェクト
class TimedClosure
{
	friend class ClosureTaskManager;
public:
	using ClosureType = std::function<void(float, bool)>;

public:
	TimedClosure(ClosureType closure,float timeLimit);

private:
	const float m_TimeLimit;	// 制限時間
	float m_ElapsedTime;		// 経過時間
	std::function<void(float, bool)> m_Closure;
};

class ClosureTaskManager
{
public:
	// 全クロージャ実行
	void Update();

	// クロージャ登録
	void AddClosure(const ZString& taskName, std::function<bool()> closure);
	void AddTimedClosure(const ZString& taskName, TimedClosure::ClosureType closure, float timeLimit);

	// 全クロージャ削除
	void Release();

	// 指定したクロージャ削除
	void DeleteClosure(const ZString& taskName);

private:
	ZUnorderedMap<ZString, std::function<bool()> > m_Closures;
	ZUnorderedMap<ZString, TimedClosure> m_TimedClosures;
};

#endif // !__CLOSURETASKMANAGER_H__