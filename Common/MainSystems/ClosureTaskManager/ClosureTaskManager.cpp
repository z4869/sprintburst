#include "MainFrame/ZMainFrame.h"
#include "ClosureTaskManager.h"

TimedClosure::TimedClosure(TimedClosure::ClosureType closure, float timeLimit)
	: m_Closure(closure), m_TimeLimit(timeLimit), m_ElapsedTime(0)
{
}

void ClosureTaskManager::Update()
{
	// 通常関数オブジェクト処理
	{
		auto it = m_Closures.begin();
		while (it != m_Closures.end())
		{
			// クロージャ実行
			bool result = (*it).second();

			if (result == false)
				it = m_Closures.erase(it); // 削除
			else ++it;
		}
	}

	// 制限時間付き関数オブジェクト処理
	{
		auto it = m_TimedClosures.begin();
		while (it != m_TimedClosures.end())
		{
			float delta = APP.m_DeltaTime;
			auto& timedClosure = it->second;
			timedClosure.m_ElapsedTime += delta;
			
			bool timeOver = timedClosure.m_ElapsedTime >= timedClosure.m_TimeLimit;
			if (timeOver)
			{
				timedClosure.m_Closure(timedClosure.m_TimeLimit, timeOver);
				it = m_TimedClosures.erase(it);
			}
			else
			{
				timedClosure.m_Closure(timedClosure.m_ElapsedTime, timeOver);
				++it;
			}
		}
	}
}

void ClosureTaskManager::AddClosure(const ZString & taskName, std::function<bool()> closure)
{
	m_Closures[taskName] = closure;
}

void ClosureTaskManager::AddTimedClosure(const ZString& taskName, TimedClosure::ClosureType closure, float timeLimit)
{
	m_TimedClosures.emplace(taskName,TimedClosure(closure,timeLimit));
}

void ClosureTaskManager::Release()
{
	m_Closures.clear();
	m_TimedClosures.clear();
}

void ClosureTaskManager::DeleteClosure(const ZString& taskName)
{
	if(m_Closures.find(taskName) != m_Closures.end())
		m_Closures.erase(taskName);
	if (m_TimedClosures.find(taskName) != m_TimedClosures.end())
		m_TimedClosures.erase(taskName);
}
