#include "PCH/pch.h"
#include "Level.h"

Level::Level():m_IsEnable(true)
{
}

Level::~Level()
{
	Release();
}

void Level::Init(const char* dataFilePath)
{
	m_DataFilePath = dataFilePath;
	if (ZPathUtil::GetExt(m_DataFilePath) == "json")
		ECSEntity::MakeEntityFromJsonFile(m_DataFilePath,&m_Entities);
	else
		assert(false); // json以外は非対応
}

void Level::Release()
{
	ECSEntity::RemoveAllEntity(m_Entities);
	m_DataFilePath.clear();
}

void Level::Update()
{
}

void Level::Draw()
{
}