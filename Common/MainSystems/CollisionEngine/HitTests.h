#ifndef __HIT_TESTS_H__
#define __HIT_TESTS_H__

// 衝突判定
namespace HitTests
{
	// 球 vs レイ
	static bool SphereToRay(const Collider_Sphere& sph, HitResData* resData_Sph, const Collider_Ray& ray, HitResData* resData_Ray)
	{
		// 当たった距離
		float dist;

		// 判定
		if (sph.m_Sphere.Intersects(ray.GetPos1(), ray.GetDir(), dist) == false)
			return false;

		// 長さ確認
		if (dist > ray.GetRayLen())
			return false;

		// 当たっているなら結果を登録

		// 球側に結果を記憶
		if (resData_Sph)
		{
			// 結果データ追加
			HitResData::Node node;
			node.MyHitObj = &sph;
			node.YouHitObj = &ray;
			node.vNearHitPos = ray.GetPos1() + ray.GetDir() * dist;
			resData_Sph->HitDataList.push_back(std::move(node));
		}

		// レイに結果を記憶
		if (resData_Ray)
		{
			// 結果データ追加
			HitResData::Node node;
			node.MyHitObj = &ray;
			node.YouHitObj = &sph;
			node.RayDist = dist;
			node.vNearHitPos = ray.GetPos1() + ray.GetDir() * dist;

			resData_Ray->HitDataList.push_back(std::move(node));
		}

		return true;
	}

	// 球 vs メッシュ
	static bool SphereToMesh(const Collider_Sphere& sph, HitResData* resData_Sph, const Collider_Mesh& mesh, HitResData* resData_Mesh)
	{
		bool bHit = false;

		ZVec3 vOut;
		ZVec3 vNearHitPos(FLT_MAX);
		ZSVector<HitResData::FaceHitInfo> faceTbl;

		for (size_t i = 0; i < mesh.m_MeshTbl.size(); i++)
		{
			ZVector<ZCollision::MTS_HitData> hitData; // ヒット結果格納用
			ZVec3 v;
			if (ZCollision::MeshToSphere(*mesh.m_MeshTbl[i], &mesh.m_Mat, sph.m_Sphere.Center, sph.m_Sphere.Radius, &v, 0, -1, &hitData) == false)
				continue;

			bHit = true;
			vOut += v;
			for (auto& hd : hitData)
			{
				faceTbl.emplace_back(i, hd.faceNo, hd.vNearPos);
				if (hd.vNearPos.LengthSq() < vNearHitPos.LengthSq())
					vNearHitPos = hd.vNearPos;
			}

		}

		if (bHit == false)
			return false;

		// 球側に結果を記録
		if (resData_Sph)
		{
			HitResData::Node node;
			node.MyHitObj = &sph;
			node.YouHitObj = &mesh;
			node.vNearHitPos = vNearHitPos;
			node.vPush = vOut;
			node.Mesh_FaceHitTbl = faceTbl;

			resData_Sph->HitDataList.push_back(std::move(node));
		}

		if (resData_Mesh)
		{
			HitResData::Node node;
			node.MyHitObj = &mesh;
			node.YouHitObj = &sph;
			node.vNearHitPos = vNearHitPos;
			node.vPush = -vOut;
			node.Mesh_FaceHitTbl = faceTbl;

			resData_Mesh->HitDataList.push_back(std::move(node));
		}

		return true;
	}

	// 球 vs Box
	static bool SphereToBox(const Collider_Sphere& sph, HitResData* resData_Sph, const Collider_Box& box, HitResData* resData_Box)
	{

		// 判定
		// 参考：http://marupeke296.com/COL_3D_No12_OBBvsPoint.html

		ZOperationalVec3 v = ZOperationalVec3(sph.m_Sphere.Center) - box.GetBox().Center;

		// 最終的に長さを求めるベクトル
		ZOperationalVec3 Vec(0, 0, 0);
		for (int i = 0; i < 3; i++)
		{
			float L = (&box.GetBox().Extents.x)[i];
			float s = ZOperationalVec3::Dot(v, box.GetDir(i)) / L;

			if (s > 1)
				Vec -= (1 - s) * L * box.GetDir(i); // はみ出した部分のベクトル算出
			else if (s < -1)
			{
				s *= -1;
				Vec += (1 - s) * L * box.GetDir(i);	// はみ出した部分のベクトル算出
			}
		}

		float len = Vec.Length();
		// 当たってないなら終了
		if (len > sph.m_Sphere.Radius)
			return false;

		// 最短位置
		ZVec3 vNearPt = sph.m_Sphere.Center - Vec;
		Vec.Normalize();
		// めり込んでいる分のベクトル
		ZVec3 vPush = Vec * (sph.m_Sphere.Radius - len);

		// 球側に結果を記録する
		if (resData_Sph)
		{
			HitResData::Node node;
			node.MyHitObj = &sph;
			node.YouHitObj = &box;
			node.vNearHitPos = vNearPt;

			node.vPush = vPush;
			resData_Sph->HitDataList.push_back(std::move(node));
		}

		// Box側に結果を記録する
		if (resData_Box)
		{
			HitResData::Node node;
			node.MyHitObj = &box;
			node.YouHitObj = &sph;
			node.vNearHitPos = vNearPt;

			node.vPush = -vPush;
			resData_Box->HitDataList.push_back(std::move(node));
		}

		return true;
	}

	// レイ vs Box
	static bool RayToBox(const Collider_Ray& ray, HitResData* resData_Ray, const Collider_Box& box, HitResData* resData_Box)
	{
		float dist;
		if (box.GetBox().Intersects(ray.GetPos1(), ray.GetDir(), dist) == false)
			return false;

		if (dist < 0 || dist > ray.GetRayLen())
			return false;

		if (resData_Ray)
		{
			HitResData::Node node;
			node.MyHitObj = &ray;
			node.YouHitObj = &box;
			node.vNearHitPos = ray.GetPos1() + ray.GetDir() * dist;
			node.RayDist = dist;

			resData_Ray->HitDataList.push_back(std::move(node));
		}

		if (resData_Box)
		{
			HitResData::Node node;
			node.MyHitObj = &box;
			node.YouHitObj = &ray;
			node.vNearHitPos = ray.GetPos1() + ray.GetDir() * dist;

			resData_Box->HitDataList.push_back(std::move(node));
		}

		return true;
	}

	// レイ vs メッシュ
	static bool RayToMesh(const Collider_Ray& ray, HitResData* resData_Ray, const Collider_Mesh& mesh, HitResData* resData_Mesh)
	{
		// レイ vs メッシュ判定
		float dist = FLT_MAX;
		int meshNo = 0;
		size_t faceIndex;
		bool bHit = false;
		const ZVec3* pvN = nullptr;

		for (size_t i = 0; i < mesh.m_MeshTbl.size(); i++)
		{
			float d;
			UINT fi;
			if (ZCollision::RayToMesh(*mesh.m_MeshTbl[i], &mesh.m_Mat, ray.GetPos1(), ray.GetPos2(), &d, &fi) == false)
				continue;
			bHit = true;
			if (d < dist)
			{
				dist = d;
				faceIndex = fi;
				pvN = &mesh.m_MeshTbl[i]->GetExtFace()[faceIndex].vN; // 面の方向
				meshNo = (int)i;
			}
		}
		if (bHit == false)
			return false;

		if (dist > ray.GetRayLen())
			return false;

		// レイ側に結果を記憶する
		if (resData_Ray)
		{
			HitResData::Node node;
			node.MyHitObj = &ray;
			node.YouHitObj = &mesh;
			node.vNearHitPos = ray.GetPos1() + ray.GetDir() * dist;
			node.RayDist = dist;
			node.Mesh_FaceHitTbl.emplace_back(meshNo, faceIndex, node.vNearHitPos);

			resData_Ray->HitDataList.push_back(std::move(node));
		}

		// メッシュ側に結果を記憶する
		if (resData_Mesh)
		{
			HitResData::Node node;
			node.MyHitObj = &mesh;
			node.YouHitObj = &ray;
			node.vNearHitPos = ray.GetPos1() + ray.GetDir() * dist;
			node.Mesh_FaceHitTbl.emplace_back(meshNo, faceIndex, node.vNearHitPos);

			resData_Mesh->HitDataList.push_back(std::move(node));
		}

		return true;
	}

}


#endif