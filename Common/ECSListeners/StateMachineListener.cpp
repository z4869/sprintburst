#include "PCH/pch.h"
#include "ECSComponents/Components.h"
#include "StateMachineListener.h"

StateMachineListener::StateMachineListener()
{
	AddComponentID(AnimatorComponent::ID);
	AddComponentID(StateMachineComponent::ID);
}

void StateMachineListener::OnMadeEntityFromJson(EntityHandle handle)
{
	auto animComp = ECS.GetComponent<AnimatorComponent>(handle);
	auto stateMachineComp = ECS.GetComponent<StateMachineComponent>(handle);

	if (stateMachineComp == nullptr)
		return;

	if (animComp == nullptr)
		return;

	// アニメーターセット
	stateMachineComp->pStateMachine->SetAnimator(animComp->Animator);
	stateMachineComp->pStateMachine->SetEntity(stateMachineComp->m_Entity);
}
