#include "PCH/pch.h"
#include "Lights.h"

DirLight::DirLight()
{
	Diffuse = ZVec4::one;
	Direction = ZVec3(0, -1, 0);
	Intensity = 1.0f;
}

void DirLight::SetData(const ZVec3& direction, const ZVec4& diffuse)
{
	Diffuse = diffuse;
	Direction = direction;
	Direction.Normalize();
}

PointLight::PointLight()
{
	Diffuse = ZVec4::one;
	Radius = 1;
	Intensity = 1.0f;
}

void PointLight::SetData(const ZVec3& pos, const ZVec4& diffuse, float radius)
{
	Position = pos;
	Diffuse = diffuse;
	Radius = radius;
}

SpotLight::SpotLight()
{
	Diffuse = ZVec4::one;
	Direction = ZVec3(0, 0, 1);
	Range = 10;
	MinAngle = 15;
	MaxAngle = 20;
	Intensity = 1.0f;
}

void SpotLight::SetData(const ZVec3& pos, const ZVec3& direction, const ZVec4& diffuse, float range, float minAngle, float maxAngle)
{
	Position = pos;
	Diffuse = diffuse;
	Direction = direction;
	Direction.Normalize();
	Range = range;
	MinAngle = minAngle;
	MaxAngle = maxAngle;
}