#ifndef __LIGHTS_H__
#define __LIGHTS_H__

// 各種ライトデータ
//・DirLight		... 平行光源データ 
//・PointLight		... ポイントライトデータ
//・SpotLight		... スポットライトデータ
//・LightManager	... 上記のデータを管理する、ライト管理クラス

struct DirLight
{
public:
	DirLight();
	// 平行光源としてデータ設定
	void SetData(const ZVec3& direction, const ZVec4& diffuse);

public:
	ZVec4		Diffuse;	// ライト色
	ZVec3		Direction;	// 方向
	float		Intensity;	// 明るさ Diffuse* Intensityとして使う
};

// ポイントライト
struct PointLight
{
public:
	PointLight();
	// 点光源としてデータ設定
	void SetData(const ZVec3& pos, const ZVec4& diffuse, float radius);

public:
	ZVec4			Diffuse;			// 基本色
	ZVec3			Position;			// 座標
	float			Radius;				// 半径
	float			Intensity;			// 明るさ Diffuse* Intensityとして使う

};

// スポットライト
struct SpotLight
{
public:
	SpotLight();
	// スポット光源としてデータ設定
	void SetData(const ZVec3& pos, const ZVec3& direction, const ZVec4& diffuse, float range, float minAngle, float maxAngle);

public:
	ZVec4			Diffuse;	// 基本色
	ZVec3			Position;	// 座標
	ZVec3			Direction;	// 方向
	float			Range;		// 照射距離
	float			MinAngle;	// 角度
	float			MaxAngle;	// 角度
	float			Intensity;	// 明るさ Diffuse* Intensityとして使う

};

#endif