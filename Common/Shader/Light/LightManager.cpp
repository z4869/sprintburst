#include "PCH/pch.h"
#include "Camera/LightCamera.h"
#include "LightManager.h"

LightManager::LightManager()
{
	m_AmbientLightColor = ZVec3(0.3f, 0.3f, 0.3f);
	m_DirLight = Make_Shared(DirLight, appnew);
}

void LightManager::Init()
{
	// シェーダ用の定数バッファ作成
	m_cb12_Light.Create(12);

	m_Cam = Make_Shared(LightCamera,appnew);
}

void LightManager::Release()
{
	m_DirLight = nullptr;
	m_cb12_Light.Release();
	m_Cam = nullptr;
}

ZSP<DirLight> LightManager::GetDirLight()
{
	return m_DirLight;
}

ZSP<PointLight> LightManager::AddPointLight()
{
	// メモリ確保
	ZSP<PointLight> add(sysnew(PointLight));
	// ポイントライトリストへ追加
	m_PointLightList.push_back(add);
	return add;
}

ZSP<SpotLight> LightManager::AddSpotLight()
{
	// メモリ確保
	ZSP<SpotLight> add(sysnew(SpotLight));
	// スポットライトリストへ追加
	m_SpotLightList.push_back(add);
	return add;
}

ZSP<LightCamera> LightManager::GetLightCamera()
{
	return m_Cam;
}

void LightManager::SetAmbientLightColor(const ZVec3& color)
{
	m_AmbientLightColor = color;
}

void LightManager::SetTexLightDepthSize(const ZVec2 & size)
{
	m_cb12_Light.m_Data.DL.TexelSize = ZVec2(1) / size;
}

void LightManager::SetShadowCamTargetPoint(const ZVec3& pos)
{
	m_Cam->SetTargetPos(pos);
}

void LightManager::Update()
{
	//-----------------------------------
	// 環境光
	//-----------------------------------
	m_cb12_Light.m_Data.AmbientLight.Set(m_AmbientLightColor, 0);

	//-----------------------------------
	// 平行光源
	//-----------------------------------
	m_cb12_Light.m_Data.DL.Color = m_DirLight->Diffuse * m_DirLight->Intensity;
	m_cb12_Light.m_Data.DL.Dir = m_DirLight->Direction;

	// ポイントライト
	//  無効になってるライトは削除する
	{
		auto it = m_PointLightList.begin();
		while (it != m_PointLightList.end())
		{
			if ((*it).IsActive() == false)
				it = m_PointLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.PL_Cnt = 0;
	for (auto& light : m_PointLightList)
	{
		int idx = m_cb12_Light.m_Data.PL_Cnt;
		if (idx >= MAX_POINTLIGHT)
			break;	// 限度数以上なら終了

		m_cb12_Light.m_Data.PL[idx].Color = light.Lock()->Diffuse* light.Lock()->Intensity;
		m_cb12_Light.m_Data.PL[idx].Pos = light.Lock()->Position;
		m_cb12_Light.m_Data.PL[idx].Radius = light.Lock()->Radius;

		m_cb12_Light.m_Data.PL_Cnt++;
	}

	// スポットライト
	//  無効になってるライトは削除する
	{
		auto it = m_SpotLightList.begin();
		while (it != m_SpotLightList.end())
		{
			if ((*it).IsActive() == false)
				it = m_SpotLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.SL_Cnt = 0;
	for (auto& light : m_SpotLightList)
	{
		int idx = m_cb12_Light.m_Data.SL_Cnt;
		
		// 限度数以上なら終了
		if (idx >= MAX_POINTLIGHT)
			break;

		m_cb12_Light.m_Data.SL[idx].Color = light.Lock()->Diffuse* light.Lock()->Intensity;
		m_cb12_Light.m_Data.SL[idx].Pos = light.Lock()->Position;
		m_cb12_Light.m_Data.SL[idx].Range = light.Lock()->Range;
		m_cb12_Light.m_Data.SL[idx].Dir = light.Lock()->Direction;
		m_cb12_Light.m_Data.SL[idx].Dir.Normalize();
		m_cb12_Light.m_Data.SL[idx].MinAng = ToRadian(light.Lock()->MinAngle);
		m_cb12_Light.m_Data.SL[idx].MaxAng = ToRadian(light.Lock()->MaxAngle);

		m_cb12_Light.m_Data.SL_Cnt++;
	}

	//	カメラ更新
	m_Cam->SetCameraDirection(m_DirLight->Direction);
	m_Cam->Update();
	m_cb12_Light.m_Data.sVP = m_Cam->m_mView * m_Cam->m_mProj;


	// 実際に定数バッファへ書き込み
	m_cb12_Light.WriteData();

	m_cb12_Light.SetVS();		// 頂点シェーダパイプラインへセット
	m_cb12_Light.SetPS();		// ピクセルシェーダパイプラインへセット
}

void LightManager::ImGui()
{
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("Light"))
			ImGui::DragFloat3("DirLightVec", m_DirLight->Direction, 0.1f,-1.f, 1.f);
		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);

	GetLightCamera()->ImGui();
}
