#ifndef __MODEL_RENDERING_PIPELINE_H__
#define __MODEL_RENDERING_PIPELINE_H__

#include "Shader/ContactBufferStructs.h"

class MeshRenderer;
class SkinMeshRenderer;
class DebugColliderMeshRenderer;

class ModelRenderingPipeline
{
public:
	ModelRenderingPipeline() = default;
	~ModelRenderingPipeline() { Release(); }

	// 初期化
	bool Init();
	// 解放
	void Release();

	void Begin3DRendering();
	void End3DRendering();
	void Draw();
	void DrawShadow();
	void Z_Prepass();

	void ImGui();

public:
	ZSP<MeshRenderer>					m_StaticModelRenderer;
	ZSP<SkinMeshRenderer>				m_SkinMeshRenderer;
	ZSP<DebugColliderMeshRenderer>		m_DebugColliderMeshRenderer;

	//	レンダーターゲットテクスチャをまとめたもの
	ZSP<ZTextureSet>	m_RenderTargets;
	static const int	RenderTargetCnt = 5;

	ZRenderTargets			m_BackUp;
	ZRenderTargets			m_RT;

	//	シャドウマップテクスチャ
	ZSP<ZTexture>		m_TexLightScene;
	ZSP<ZTexture>		m_TexLightDepth;
	ZVec2				m_LightDepthTexSize;

	bool m_ZPreFlg = false;

};

#endif