#ifndef __SKIN_MESH_RENDERER_H__
#define __SKIN_MESH_RENDERER_H__

#include "ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

struct GameModelComponent;
struct ModelBoneControllerComponent;

class SkinMeshRenderer : public ModelRendererBase<GameModelComponent,ModelBoneControllerComponent>
{
public:
	~SkinMeshRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init() override;
	// 解放
	virtual void Release() override;
	
	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, GameModelComponent* modelComp,ModelBoneControllerComponent* boneComp) override;
	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画)
	virtual void Flash() override;

	void FlashShadowObject();

private:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers() override;

	void SetMatrix(const ZMatrix& mat);

	void SetXColor(const ZVec4& col);

	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_Matrix> m_cb0_Matrix;
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;


};

#endif

