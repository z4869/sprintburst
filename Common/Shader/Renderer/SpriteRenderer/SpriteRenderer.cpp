#include "MainFrame/ZMainFrame.h"
#include "Shader/ShaderManager.h"
#include "SpriteRenderer.h"
#include "CommonSubSystems.h"

const int SpriteRenderer::NumVertex = 4;

bool SpriteRenderer::Init()
{
	Release();

	bool successed = true;

	// シェーダ読み込み
	m_Shaders.SetVS("Sprite", APP.m_ResStg.LoadVertexShader("Shader/Sprite_VS.cso"));
	m_Shaders.SetPS("Sprite", APP.m_ResStg.LoadPixelShader("Shader/Sprite_PS.cso"));

	successed &= m_cb1_PerObject.Create(1);

	m_RectVertex[0].UV = ZVec2(0, 1);
	m_RectVertex[1].UV = ZVec2(0);
	m_RectVertex[2].UV = ZVec2(1);
	m_RectVertex[3].UV = ZVec2(1, 0);

	m_VB.Create(m_RectVertex, NumVertex, m_RectVertex->GetVertexTypeData());

	m_IsBegined = false;
	return successed;
}

void SpriteRenderer::Release()
{
	m_Shaders.RemoveAllShaders();
	m_cb1_PerObject.Release();
}

void SpriteRenderer::Begin(bool enableZBuffer, bool useLinerSampler)
{
	if (m_IsBegined)return;

	m_IsBegined = true;

	// 2D用射影行列作成
	UINT pNumViewports = 1;

	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);
	m_Proj2DMat.CreateOrthoLH(vp.Width, vp.Height,0,1);
	m_Proj2DMat.Move(-1, -1, 0); // 左上を基準に
	m_Proj2DMat.Scale(1, -1, 1); // 上下反転

	// 定数バッファ書き込み
	m_cb1_PerObject.m_Data.Mat = m_Proj2DMat;
	m_cb1_PerObject.WriteData();

	// 現状の各種ステート記憶
	m_DSBackUp.SetAll_GetState();
	m_BSBackUp.SetAll_GetState();
	
	// Z書き込み、判定無効化
	ZDepthStencilState ds;
	ds.Set_FromDesc(m_DSBackUp.GetDesc());
	ds.Set_ZEnable(enableZBuffer);
	ds.Set_ZWriteEnable(enableZBuffer);
	ds.SetState();
	
	// ブレンドステート
	ZBlendState bs;
	bs.Set_Alpha();
	bs.SetState();

	// サンプラーステート
	if (useLinerSampler)
		ShMgr.m_smp1_Linear_Clamp.SetStatePS(5);
	else
		ShMgr.m_smp3_Point_Clamp.SetStatePS(5);
}

void SpriteRenderer::End()
{
	if (!m_IsBegined)return;

	m_IsBegined = false;

	// ステート復元
	m_BSBackUp.SetState();
	m_DSBackUp.SetState();
}

void SpriteRenderer::Draw2D(ID3D11ShaderResourceView * src, float x, float y, float w, float h, const ZVec4* color)
{
	bool isBgn = m_IsBegined;
	if (isBgn == false)
		Begin();
	
	SetConstantBuffers();

	// テクスチャ
	if (src)
		ZDx.GetDevContext()->PSSetShaderResources(0, 1, &src);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);

	UpdateRectVertex(ZVec2(x, y), ZVec2(w, h), *color);
	m_VB.SetDrawData();
	m_VB.WriteAndDraw(m_RectVertex, 4);

	// セットされているテクスチャの解除
	ZDx.RemoveTexturePS(0);
	
	if (isBgn == false) End();
}

void SpriteRenderer::Draw2D(ZTexture & src, float x, float y, float w, float h, const ZVec4 * color)
{
	Draw2D(src.GetTex(), x, y, w, h, color);
}

void SpriteRenderer::Draw2D(ID3D11ShaderResourceView * src, const ZMatrix & m, const ZVec4 * color)
{
	if (src == nullptr)
		return;

	// テクスチャ情報取得
	ComPtr<ID3D11Resource> resource;
	src->GetResource(&resource);
	ComPtr<ID3D11Texture2D> texture2D;
	if (FAILED(resource.As(&texture2D))) return;

	D3D11_TEXTURE2D_DESC desc;
	texture2D->GetDesc(&desc);

	bool isBgn = m_IsBegined;
	if (isBgn == false) Begin();

	SetConstantBuffers();

	// テクスチャ
	ZDx.GetDevContext()->PSSetShaderResources(0, 1, &src);

	UpdateRectVertex(ZVec2(0), ZVec2(desc.Width, desc.Height), *color);
	// 変換
	for (auto& v : m_RectVertex) v.Pos.Transform(m);
	m_VB.SetDrawData();
	m_VB.WriteAndDraw(m_RectVertex, 4);

	// セットされているテクスチャの解除
	ZDx.RemoveTexturePS(0);

	if (isBgn) End();
}

void SpriteRenderer::Draw2D(ID3D11ShaderResourceView* src, const ZVec2& size, const ZMatrix& m, const ZVec4* color) {
	if (src == nullptr)
		return;

	// テクスチャ情報取得
	ComPtr<ID3D11Resource> resource;
	src->GetResource(&resource);
	ComPtr<ID3D11Texture2D> texture2D;
	if (FAILED(resource.As(&texture2D)))
		return;
	bool isBgn = m_IsBegined;
	if (isBgn == false)
		Begin();

	SetConstantBuffers();

	// テクスチャ
	ZDx.GetDevContext()->PSSetShaderResources(0, 1, &src);

	// 頂点作成
	float imageW = size.x * 0.5f;
	float imageH = size.y * 0.5f;

	const int numVtx = 4;
	ZVertex_Pos_UV_Color vertices[numVtx] =
	{
		{ ZVec3(-imageW,imageH,0),ZVec2(0,1),*color },
		{ ZVec3(-imageW,-imageH,0),ZVec2(0,0),*color },
		{ ZVec3(imageW,imageH,0),ZVec2(1,1),*color },
		{ ZVec3(imageW,-imageH,0),ZVec2(1,0),*color }
	};
	// 変換
	for (auto& v : vertices)
		v.Pos.Transform(m);

	m_VB.SetDrawData();
	m_VB.WriteAndDraw(vertices, numVtx);

	// セットされているテクスチャの解除
	ZDx.RemoveTexturePS(0);

	if (isBgn)
		End();
}

ZSP<ZFontSprite> SpriteRenderer::DrawFont(int fontNo, const ZString& text, float x, float y, const ZVec4* color, int antiAliasingFlag)
{
	ZSP<ZFontSprite> fontSprite;

	ZMatrix m;
	m.CreateMove(x, y, 0);
	DrawFont(*fontSprite,m,color,antiAliasingFlag);

	return fontSprite;
}

ZSP<ZFontSprite> SpriteRenderer::DrawFont(int fontNo, const ZString & text, const ZMatrix & m, const ZVec4 * color, int antiAliasingFlag)
{
	ZSP<ZFontSprite> fontSprite;

	DrawFont(*fontSprite, m, color, antiAliasingFlag);

	return fontSprite;
}

void SpriteRenderer::DrawFont(ZFontSprite & fontSprite, float x, float y, const ZVec4 * color, int antialiasingFlag)
{
	ZMatrix m;
	m.CreateMove(x, y, 0);
	
	DrawFont(fontSprite, m, color, antialiasingFlag);
}

void SpriteRenderer::DrawFont(ZFontSprite& fontSprite, const ZMatrix & m, const ZVec4 * color, int antialiasingFlag)
{
	if (fontSprite.GetTexList().size() <= 0)
		return;

	bool isBgn = m_IsBegined;
	if (isBgn == false) Begin();

	SetConstantBuffers();

	m_VB.SetDrawData();

	for (auto& v : m_RectVertex)
		v.Color = *color;

	// フォントの高さ
	float h = (float)fontSprite.GetTexList()[0]->Tex->GetInfo().Height;

	// すべての文字テクスチャを描画する
	float moveX = 0;
	ZMatrix mat = m;
	for (auto& data : fontSprite.GetTexList())
	{
		// 改行文字の場合は座標操作
		if (data->Code == '\n')
		{
			mat.Move_Local(-moveX, h, 0);
			moveX = 0;
		}
		else
		{
			data->Tex->SetTexturePS(0);
			float imageW = (float)data->Tex->GetInfo().Width;
			float imageH = (float)data->Tex->GetInfo().Height;
			
			UpdateRectVertex(ZVec2(0, 0), ZVec2(imageW, imageH),*color);

			// 1文字描画
			m_VB.WriteAndDraw(m_RectVertex,NumVertex);
			
			// 次の文字のためX座標の間隔を開ける
			mat.Move_Local((float)data->Tex->GetInfo().Width, 0, 0);
			
			moveX += data->Tex->GetInfo().Width;
		}

	}

	ZDx.RemoveTexturePS(0);

	if (isBgn == false) End();
}

void SpriteRenderer::SetConstantBuffers()
{
	// シェーダーセット
	m_Shaders.GetVS("Sprite")->SetShader();
	m_Shaders.GetPS("Sprite")->SetShader();

	// 定数バッファセット
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
}

void SpriteRenderer::UpdateRectVertex(const ZVec2& pos, const ZVec2& size, const ZVec4& color)
{
	// 頂点作成
	float x2 = pos.x + size.x;
	float y2 = pos.y + size.y;

	m_RectVertex[0].Color = color;
	m_RectVertex[1].Color = color;
	m_RectVertex[2].Color = color;
	m_RectVertex[3].Color = color;
	m_RectVertex[0].Pos = ZVec3(pos.x, y2, 0);
	m_RectVertex[1].Pos = ZVec3(pos.x, pos.y, 0);
	m_RectVertex[2].Pos = ZVec3(x2, y2, 0);
	m_RectVertex[3].Pos = ZVec3(x2, pos.y, 0);
}
