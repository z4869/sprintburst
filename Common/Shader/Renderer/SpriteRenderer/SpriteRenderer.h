#ifndef __SPRITE_RENDERER_H__
#define __SPRITE_RENDERER_H__

#include "../RendererBase.h"
#include "Shader/ContactBufferStructs.h"

class SpriteRenderer : public RendererBase
{
private:
	static const int NumVertex;

public:
	virtual ~SpriteRenderer()
	{
	}

	// 初期化
	bool Init();
	// 解放
	void Release();

	// 2D描画開始
	void Begin(bool enableZBuffer = false, bool useLinerSampler = false);

	// 2D描画終了
	void End();

	// 2D描画
	void Draw2D(ID3D11ShaderResourceView* src, float x, float y, float w, float h, const ZVec4* color = &ZVec4(1, 1, 1, 1));
	void Draw2D(ZTexture& src, float x, float y, float w, float h, const ZVec4* color = &ZVec4(1, 1, 1, 1));

	// 2D描画(行列指定ver)
	void Draw2D(ID3D11ShaderResourceView* src, const ZMatrix& m = ZMatrix::Identity, const ZVec4* color = &ZVec4(1, 1, 1, 1));
	void Draw2D(ZTexture& src, const ZMatrix& m = ZMatrix::Identity, const ZVec4* color = &ZVec4(1, 1, 1, 1));

	void Draw2D(ID3D11ShaderResourceView* src, const ZVec2& size, const ZMatrix& m = ZMatrix::Identity, const ZVec4* color = &ZVec4(1, 1, 1, 1));

	// スプライトへフォント描画
	//	戻り値 ... 描画したフォントのテクスチャ
	ZSP<ZFontSprite> DrawFont(int fontNo, const ZString& text, float x, float y, const ZVec4* color = &ZVec4::one, int antiAliasingFlag = 0);

	// スプライトへフォント描画(行列指定ver)
	//	戻り値 ... 描画したフォントのテクスチャ
	ZSP<ZFontSprite> DrawFont(int fontNo, const ZString& text, const ZMatrix& m, const ZVec4* color = &ZVec4::one, int antiAliasingFlag = 0);

	// フォント描画(fontSpriteを直接指定)
	void DrawFont(ZFontSprite& fontSprite, float x, float y, const ZVec4* color = &ZVec4::one, int antialiasingFlag = 0);

	// フォント描画(fontSpriteを直接指定 行列指定ver)
	void DrawFont(ZFontSprite& fontSprite, const ZMatrix& m, const ZVec4* color = &ZVec4::one, int antialiasingFlag = 0);

protected:
	// コンタクトバッファをシェーダーにセット
	void SetConstantBuffers();

private:
	void UpdateRectVertex(const ZVec2& pos,const ZVec2& size, const ZVec4& color);

protected:
	// 頂点バッファ
	ZPolygon m_VB;
	ZVertex_Pos_UV_Color m_RectVertex[4];

	ZConstantBuffer<cbPerObject_Matrix> m_cb1_PerObject;

	// ステート記憶用
	ZDepthStencilState m_DSBackUp;
	ZBlendState m_BSBackUp;

	bool m_IsBegined;

	ZMatrix m_Proj2DMat;
};


#endif
