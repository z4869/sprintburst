#ifndef __SSAO_SHADER_H__
#define __SSAO_SHADER_H__

#include "../RendererBase.h"
#include "Shader/ContactBufferStructs.h"

class BlurShader;
class SpriteRenderer;

class SSAOShader : public RendererBase
{
public:
	SSAOShader();
	~SSAOShader() { Release(); }

	bool Init();
	void Release();

	void Render(ZSP<ZTexture> normal, ZSP<ZTexture> depth,ZSP<ZTexture> source, ZSP<ZTexture> out);
	
	ZSP<ZTexture> GetSSAOResult() { return m_TexSSAOResult; }

	cbSSAO& GetSSAOParams() { return m_cb1_SSAO.m_Data; }

private:
	void SetConstantBuffers();

private:
	ZMatrix m_Proj2DMat;
	ZPolygon m_Plane; // 板ポリ
	ZSP<ZTexture> m_TexRandom; // ランダムノイズテクスチャ
	ZSP<ZTexture> m_TexLinerDepth; // 深度変換用
	ZSP<ZTexture> m_TexSSAOOrigin; // ブラーなしSSAOテクスチャ
	ZSP<ZTexture> m_TexSSAOResult; // 完成図

	ZConstantBuffer<cbPerObject_Matrix> m_cb0_Matrix;
	ZConstantBuffer<cbSSAO> m_cb1_SSAO;

	ZSP<ZPixelShader> m_LinerDepthPS;
	ZSP<ZPixelShader> m_MergePS; // 合成用

	BlurShader* m_pBs;
	SpriteRenderer* m_pSr;
};

#endif