#include "MainFrame/ZMainFrame.h"
#include "Shader/ShaderManager.h"
#include "Shader/Renderer/Blur/BlurShader.h"
#include "Shader/Renderer/SpriteRenderer/SpriteRenderer.h"
#include "SSAOShader.h"
#include "SubSystems.h"
#include "CommonSubSystems.h"

SSAOShader::SSAOShader()
{
}

bool SSAOShader::Init()
{
	// シェーダロード
	m_Shaders.SetVS("SSAO", APP.m_ResStg.LoadVertexShader("Shader/SSAO_VS.cso"));
	m_Shaders.SetPS("SSAO", APP.m_ResStg.LoadPixelShader("Shader/SSAO_PS.cso"));

	m_Shaders.SetPS("LinerDepth", APP.m_ResStg.LoadPixelShader("Shader/LinerDepth_PS.cso"));
	m_Shaders.SetPS("Merge", APP.m_ResStg.LoadPixelShader("Shader/Merge_PS.cso"));
	
	// 板ポリ作成
	float w = (float)ZDx.GetRezoW();
	float h = (float)ZDx.GetRezoH();
	ZVertex_Pos_UV vertices[4] =
	{
		{ ZVec3(0,h,0),ZVec2(0,1) },
		{ ZVec3(0,0,0),ZVec2(0,0) },
		{ ZVec3(w,h,0),ZVec2(1,1) },
		{ ZVec3(w,0,0),ZVec2(1,0) }
	};
	m_Plane.Create(vertices, 4, ZVertex_Pos_UV::GetVertexTypeData());

	// ランダムノイズテクスチャ作成
	constexpr uint bufferSize = 64 * 64 * 4;
	uint8 data[bufferSize];
	for (uint i = 0; i < bufferSize; i += 4)
	{
		data[i + 0] = RAND.GetInt(0, 255);
		data[i + 1] = RAND.GetInt(0, 255);
		data[i + 2] = RAND.GetInt(0, 255);
		data[i + 3] = 255;
	}

	D3D11_SUBRESOURCE_DATA* fillData = sysnew(D3D11_SUBRESOURCE_DATA);
	ZeroMemory(fillData, sizeof(D3D11_SUBRESOURCE_DATA));
	fillData->pSysMem = data;
	fillData->SysMemPitch = (UINT)(64 * 4);

	m_TexRandom = Make_Shared(ZTexture, sysnew);
	
	bool s = m_TexRandom->Create(64, 64, DXGI_FORMAT_R8G8B8A8_UNORM, fillData);
	assert(s);
	SAFE_DELPTR(fillData);

	const int rezoW = ZDx.GetRezoW() / 2;
	const int rezoH = ZDx.GetRezoH() / 2;

	m_TexLinerDepth = Make_Shared(ZTexture, sysnew);
	s &= m_TexLinerDepth->CreateRT(rezoW, rezoH, DXGI_FORMAT_R32_FLOAT);
	assert(s);
	
	m_TexSSAOOrigin = Make_Shared(ZTexture, sysnew);
	s &= m_TexSSAOOrigin->CreateRT(rezoW, rezoH);
	assert(s);

	m_TexSSAOResult = Make_Shared(ZTexture, sysnew);
	s &= m_TexSSAOResult->CreateRT(rezoW, rezoH);
	assert(s);

	m_cb0_Matrix.Create(0);
	m_cb1_SSAO.Create(1);

	m_Proj2DMat.CreateOrthoLH((float)ZDx.GetRezoW(), (float)ZDx.GetRezoH(), 0, 1);
	m_Proj2DMat.Move(-1, -1, 0);
	m_Proj2DMat.Scale(1, -1, 1);
	m_cb0_Matrix.m_Data.Mat = m_Proj2DMat;
	m_cb0_Matrix.WriteData();

	m_pBs = &ShMgr.GetRenderer<BlurShader>();
	m_pSr = &ShMgr.GetRenderer<SpriteRenderer>();

	return s;
}

void SSAOShader::Release()
{
	m_Shaders.RemoveAllShaders();
	m_TexRandom = nullptr;

	m_cb0_Matrix.Release();
	m_cb1_SSAO.Release();
}

void SSAOShader::Render(ZSP<ZTexture> normal, ZSP<ZTexture> depth, ZSP<ZTexture> source, ZSP<ZTexture> out)
{
	m_TexLinerDepth->ClearRT(ZVec4(0, 0, 0, 1));
	m_TexSSAOOrigin->ClearRT(ZVec4(0, 0, 0, 1));
	m_TexSSAOResult->ClearRT(ZVec4(0, 0, 0, 1));

	// バックアップ
	ZRenderTarget_BackUpper bu;

	// レンダーターゲットセット
	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, m_TexLinerDepth->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	// 
	m_Plane.SetDrawData();
	m_Shaders.GetVS("SSAO")->SetShader();
	m_Shaders.GetPS("LinerDepth")->SetShader();

	m_cb0_Matrix.SetVS();
	depth->SetTexturePS(1);

	// レンダリングステート保存
	ZDepthStencilState DSBackUp;
	ZBlendState BSBackUp;
	{
		// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();

		// ブレンドステート
		ZBlendState bs;
		bs.Set_Alpha();
		bs.SetState();
	}


	// 描画
	m_Plane.Draw();

	//-------

	rt.RT(0, m_TexSSAOOrigin->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	SetConstantBuffers();
	m_cb1_SSAO.WriteData();
	normal->SetTexturePS(0);
	m_TexLinerDepth->SetTexturePS(1);

	// 描画
	m_Plane.Draw();

	ZDx.RemoveTexturePS(0);
	ZDx.RemoveTexturePS(1);
	ZDx.RemoveTexturePS(2);

	//-----

	// ぼかす
	m_pBs->ChangeRenderTarget(BlurShader::BlurRenderFlgs::Harf);
	m_pBs->GenerateBlur(m_TexSSAOOrigin, 2, 1.8f);

	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();

	rt.RT(0, m_TexSSAOResult->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	m_pSr->Begin(false, true);
	auto info = m_TexSSAOResult->GetInfo();
	m_pSr->Draw2D(*m_pBs->GetTexture(), 0, 0, (float)info.Width, (float)info.Height);
	m_pSr->End();

	//-----

	{
		// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();
	}
	
	rt.RT(0, out->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	SetConstantBuffers();
	m_Shaders.GetPS("Merge")->SetShader();
	m_cb0_Matrix.m_Data.Mat = m_Proj2DMat;
	m_cb0_Matrix.WriteData();

	source->SetTexturePS(3);
	m_TexSSAOResult->SetTexturePS(4);
	m_Plane.SetDrawData();

	// 描画
	m_Plane.Draw();

	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();

	ZDx.RemoveTexturePS(3);
	ZDx.RemoveTexturePS(4);
}

void SSAOShader::SetConstantBuffers()
{
	m_Shaders.GetVS("SSAO")->SetShader();
	m_Shaders.GetPS("SSAO")->SetShader();

	m_cb0_Matrix.SetVS();
	m_cb0_Matrix.SetPS();
	m_cb1_SSAO.SetPS();

	m_TexRandom->SetTexturePS(2);
}
