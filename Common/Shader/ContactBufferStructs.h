#ifndef __CONTACT_BUFFER_STRUCTS_H__
#define __CONTACT_BUFFER_STRUCTS_H__

#include "LightDefines.h"

// オブジェクト単位での設定データ(行列のみ)
// インスタンシングを使用する際は行列を定数バッファにセットする必要がない
// そのため↓のオブジェクト単位の設定データとは切り離して無駄なデータ転送を避ける
struct cbPerObject_Matrix
{
	ZMatrix Mat;
};

// オブジェクト単位での設定データ(スタティックメッシュ用)
struct cbPerObject_STModel
{
public:
	cbPerObject_STModel()
	{
		MulColor = ZVec4::one;
		LightEnable = 1;
		DistanceFogEnable = 0;
		DistanceFogColor = ZVec3::One;
		DistanceFogDensity = 0.02f;
		X_Col.Set(ZVec4::zero);
	}

public:
	ZVec4 MulColor;				// 無条件で合成する色 強制的に色を変更する場合に使用
	int LightEnable;			// ライト有効/無効
	int DistanceFogEnable;		// 距離フォグ有効/無効
	float tmp[2];				// パッキング規則のためゴミを入れる
	ZVec3 DistanceFogColor;		// 距離フォグ色
	float DistanceFogDensity;	// 距離フォグ密度
	ZVec4 X_Col;
};

// マテリアル単位での設定データ
struct cbPerMaterial
{
public:
	cbPerMaterial()
	{
		Diffuse = ZVec4::one;
		Specular = ZVec4::one;
		SpePower = 0;
		Emissive = ZVec4::zero;
		ParallaxVal = 0;
	}

public:
	ZVec4 Diffuse;		// 基本色
	ZVec4 Specular;		// スペキュラ色
	float SpePower;		// スペキュラの強さ
	float tmp[3];	// パッキング規則のためゴミを入れる
	ZVec4 Emissive;		// エミッシブ
	float ParallaxVal;	// 視差度
	float tmp3[3];	// パッキング規則のためゴミを入れる
};

// カメラ用定数バッファ
struct cbCamera
{
	ZMatrix	mV;		// ビュー行列
	ZMatrix	mP;		// 射影行列
	ZMatrix	mPO;	// 射影行列
	ZVec3 CamPos;	// カメラの座標
	float m_Near;
	float m_Far;
	float tmp2[3];
};

struct cbSampleShaderLight
{
public:
	// ライトデータ
	struct DLData
	{
		ZVec4 Color;	// 色
		ZVec3 Dir;	// 方向
		float _tmp;
		ZVec2 TexelSize;
		ZVec2 gomi;
	};

	// ポイントライトのデータ
	struct PLData
	{
	public:
		PLData() :Radius(1)
		{
		}

	public:
		ZVec4 Color;		// 色
		ZVec3 Pos;			// 座標
		float Radius;		// 半径
	};

	// スポットライトのデータ
	struct SLData
	{
		ZVec4 Color;	// 色
		ZVec3 Pos;		// 座標
		float Range;	// 照射範囲

		ZVec3 Dir;		// 方向
		float MinAng;// min〜maxにかけて、徐々に光の強さが減衰される
		float MaxAng;
		float tmp[3];
	};

public:
	// 環境光
	ZVec4	AmbientLight;

	// 平行光源
	DLData DL;	// 平行光データ配列(最大3個)

	// ポイントライト
	int	PL_Cnt{ 0 };	// ポイントライト数
	float tmp3[3];
	PLData PL[MAX_POINTLIGHT];	// ポイントライトデータ配列(最大100個)

	// スポットライト
	int SL_Cnt{ 0 }; // スポットライト数
	float tmp4[3];
	SLData SL[MAX_SPOTLIGHT];	// スポットライトデータ配列(最大100個)

	//	シャドウ変換行列
	ZMatrix	sVP;
};

//	変換用行列
struct cbView
{
	ZMatrix view;
};

//	ガウス用バッファ
struct cbGauss
{
	float weights[8];
	ZVec2 Offset;
	ZVec2 Size;
	ZVec2 Texel;
	float tmp[2];
};

// DOFパラメータ
struct cbDoFParam
{
public:
	cbDoFParam()
	{
		FarState.Set(1.f, 50.f);
		NearState.Set(0.05f, 50.f);
	}

	template<class Archive>
	void serialize(Archive & archive)
	{
		SaveVector2(archive, FarState, "Far");
		SaveVector2(archive, NearState, "Near");
		archive(cereal::make_nvp("UseFar", UseFarBlur));
	}

public:
	ZVec2 FarState;		//	x:距離 y:補正係数
	ZVec2 NearState;	//	x:距離 y:補正係数
	int   UseFarBlur = 1;
	float tmp[3];
};

//	変換用行列
struct cbXRay
{
	ZMatrix view;
	ZVec4	xCol;
	//		int FresnelEnable = 0;	//	Todo:フレネル反射を入れるときは、フレネル用のステートを他にも追加すること
	cbXRay()
	{
		xCol.Set(1, 0, 0, 1);
	}
};

// SSAOパラメータ
struct cbSSAO
{
	float Strength = 1.f;
	float Falloff = -0.1f;
	float Rad = 0.13f;
	float tmp;

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(cereal::make_nvp("Strength", Strength));
		archive(cereal::make_nvp("Falloff", Falloff));
		archive(cereal::make_nvp("Rad", Rad));
	}

};

// デバッグ用定数バッファ
struct cbDebug
{
public:
	cbDebug()
	{
		showCascadeDist = 0;
	}

public:
	int showCascadeDist;
	float tmp[3];

};

#endif