#ifndef __HE_PASS_H__
#define __HE_PASS_H__

class BlurShader;

class HEPass : public ZPostEffectPass
{
public:
	HEPass();

	void Init();
	void Release();

	void ImGui()override;

	ZString GetPassName()override;

	void RenderPass(ZSP<ZTexture> source);
private:
	ZSP<ZTexture> m_TexEmissive;
	BlurShader* m_pBs;
};


#endif