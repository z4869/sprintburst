#include "PCH/pch.h"
#include "CommonSubSystems.h"
#include "Shader/ShaderManager.h"
#include "HEPass.h"

HEPass::HEPass() : ZPostEffectPass()
{
}

void HEPass::Init()
{
	m_TexEmissive = Make_Shared(ZTexture, sysnew);
	m_TexEmissive->CreateRT(ZDx.GetRezoW() / 2, ZDx.GetRezoH() / 2, DXGI_FORMAT_R11G11B10_FLOAT);
	m_pBs = &ShMgr.GetRenderer<BlurShader>();
}

void HEPass::Release()
{
	m_TexEmissive = nullptr;
}

void HEPass::ImGui()
{
	auto textureDebug = [this]()
	{
		if(ImGui::Begin(ZPostEffects::TexDebugWindowName))
		{
			if (ImGui::Begin("Emissive"))
			{
				if (m_TexEmissive != nullptr)
					ImGui::Image(m_TexEmissive->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::End();
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(textureDebug);
}

ZString HEPass::GetPassName()
{
	return "HE Pass";
}

void HEPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_IsEnable == false)return;
	if (m_pPostEffects == nullptr)return;

	m_TexEmissive->ClearRT(ZVec4(0));
	auto& info = m_TexEmissive->GetInfo();
	m_pBs->HIE(source, m_TexEmissive, ZVec2((float)info.Width, (float)info.Height));

	m_pPostEffects->SetTextureResource("Emissive",m_TexEmissive);
}
