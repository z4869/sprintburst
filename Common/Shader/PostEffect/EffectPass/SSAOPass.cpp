#include "PCH/pch.h"
#include "CommonSubSystems.h"
#include "Shader/ShaderManager.h"
#include "SSAOPass.h"

SSAOPass::SSAOPass() : ZPostEffectPass()
{
}

void SSAOPass::Init()
{
	m_SSAOShader.Init();
	m_TexSSAOCompleted = Make_Shared(ZTexture,sysnew);
	m_TexSSAOCompleted->CreateRT(ZDx.GetRezoW(), ZDx.GetRezoH(), DXGI_FORMAT_R16G16B16A16_FLOAT);
	m_pSr = &ShMgr.GetRenderer<SpriteRenderer>();
}

void SSAOPass::Release()
{
	m_SSAOShader.Release();
	m_TexSSAOCompleted = nullptr;
}

void SSAOPass::ImGui()
{
	auto setting = [this]()
	{
		if (ImGui::Begin(ZPostEffects::PostEffectWindowName) == false)
		{
			ImGui::End();
			return;
		}

		auto& SSAOParams = m_SSAOShader.GetSSAOParams();

		if (ImGui::CollapsingHeader("SSAOParams"))
		{
			ImGui::DragFloat("Strength", &SSAOParams.Strength, 0.01f, -100, 100);
			ImGui::DragFloat("Falloff", &SSAOParams.Falloff, 0.01f, -100,100);
			ImGui::DragFloat("Rad", &SSAOParams.Rad, 0.01f, 0, 100);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(setting);

	auto textureDebug = [this]()
	{
		if (ImGui::Begin(ZPostEffects::TexDebugWindowName))
		{
			if (ImGui::Begin("SSAO"))
			{
				if (m_SSAOShader.GetSSAOResult() != nullptr)
					ImGui::Image(m_SSAOShader.GetSSAOResult()->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::End();
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(textureDebug);
}

void SSAOPass::LoadSettingFromJson(const json11::Json & jsonObj)
{
	// 有効フラグ
	{
		auto item = jsonObj["Flgs"].object_items();
		m_IsEnable = item["SSAO"].bool_value();
	}

	// LightBloomパラメータ
	{
		auto& SSAOParams = m_SSAOShader.GetSSAOParams();
		auto item = jsonObj["SSAOState"].object_items();
		SSAOParams.Strength = (float)item["Strength"].number_value();
		SSAOParams.Falloff = (float)item["Falloff"].number_value();
		SSAOParams.Rad = (float)item["Rad"].number_value();
	}
}

void SSAOPass::SaveSettingToJson(cereal::JSONOutputArchive & archive)
{
	auto& SSAOParams = m_SSAOShader.GetSSAOParams();
	archive(cereal::make_nvp("SSAOState", SSAOParams));
}

ZString SSAOPass::GetPassName()
{
	return "SSAO Pass";
}

void SSAOPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_pPostEffects == nullptr) return;
	if (m_IsEnable == false)return;

	auto texDepth = m_pPostEffects->GetSourceTexture("Depth");
	if (texDepth == nullptr)return;
	auto texNormal = m_pPostEffects->GetSourceTexture("Normal");
	if (texNormal == nullptr)return;

	m_SSAOShader.Render(texNormal, texDepth, source, m_TexSSAOCompleted);

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, source->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	m_pSr->Begin(false, true);
	ShMgr.m_bsAlpha.SetState();

	auto info = source->GetInfo();
	m_pSr->Draw2D(*m_TexSSAOCompleted, 0, 0, (float)info.Width, (float)info.Height);

	m_pSr->End();

	// 一応
	m_pPostEffects->PresentToCompletedTexture(source);
}
