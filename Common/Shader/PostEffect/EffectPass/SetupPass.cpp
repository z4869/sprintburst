#include "PCH/pch.h"
#include "CommonSubSystems.h"
#include "Shader/ShaderManager.h"
#include "Shader/Renderer/ModelRenderer/ModelRenderingPipeline.h"
#include "SetupPass.h"

SetupPass::SetupPass() : ZPostEffectPass()
{
}

void SetupPass::Init()
{
	m_pSr = &ShMgr.GetRenderer<SpriteRenderer>();
}

ZString SetupPass::GetPassName()
{
	return "SetUp Pass";
}

void SetupPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_pPostEffects == nullptr)return;

	ZRenderTarget_BackUpper bu;

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.Depth(nullptr);
	rt.RT(0, m_pPostEffects->GetCompletedTexture()->GetRTTex());
	rt.SetToDevice();
	
	m_pSr->Begin(false, true);
	auto info = ZDx.GetBackBuffer()->GetInfo();
	m_pSr->Draw2D(source->GetTex(), 0, 0, (float)info.Width, (float)info.Height);
	m_pSr->End();

	// 各種テクスチャセット
	m_pPostEffects->SetTextureResource("Depth", RenderingPipeline.m_RenderTargets->GetTexZSP(1));
	m_pPostEffects->SetTextureResource("CharaDepth", RenderingPipeline.m_RenderTargets->GetTexZSP(2));
	m_pPostEffects->SetTextureResource("CharaReflect", RenderingPipeline.m_RenderTargets->GetTexZSP(3));
	m_pPostEffects->SetTextureResource("Normal", RenderingPipeline.m_RenderTargets->GetTexZSP(4));

}
