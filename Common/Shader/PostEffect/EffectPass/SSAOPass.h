#ifndef __SSAO_PASS_H__
#define __SSAO_PASS_H__

#include "Shader/Renderer/SSAO/SSAOShader.h"

class SpriteRenderer;

class SSAOPass : public ZPostEffectPass
{
public:
	SSAOPass();

	void Init();
	void Release();

	void ImGui()override;

	void LoadSettingFromJson(const json11::Json& jsonObj)override;
	void SaveSettingToJson(cereal::JSONOutputArchive& archive)override;

	ZString GetPassName()override;
	void RenderPass(ZSP<ZTexture> source);

private:
	SSAOShader m_SSAOShader;
	ZSP<ZTexture> m_TexSSAOCompleted;
	SpriteRenderer* m_pSr;
};

#endif