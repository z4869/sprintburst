#include "PCH/pch.h"
#include "CommonSubSystems.h"
#include "Shader/ShaderManager.h"
#include "LightBloomPass.h"

LightBloomPass::BloomState::BloomState()
{
	AddPow.resize(BlurShader::BlurRenderCnt);
	for (int i = 0; i < BlurShader::BlurRenderCnt; i++)
		AddPow[i] = 2;

	Loop = 1;
	Disp = 8.f;
	MipLoop = 4;
}

LightBloomPass::LightBloomPass() : ZPostEffectPass()
{
}

void LightBloomPass::Init()
{
	m_pBs = &ShMgr.GetRenderer<BlurShader>();
	m_pSr = &ShMgr.GetRenderer<SpriteRenderer>();
}

void LightBloomPass::Release()
{
}

void LightBloomPass::ImGui()
{
	auto setting = [this]()
	{
		if (ImGui::Begin(ZPostEffects::PostEffectWindowName) == false)
		{
			ImGui::End();
			return;
		}

		if(ImGui::CollapsingHeader("BloomParams"))
		{
			ImGui::SliderInt("BlurLoop", &m_BloomState.Loop, 1, 10);
			ImGui::DragFloat("BlurDisp", &m_BloomState.Disp, 0.1f, -15.f, 15.f);
			ImGui::SliderInt("BlurMipLoop", &m_BloomState.MipLoop, 0, 6);

			for (int i = 0; i < BlurShader::BlurRenderCnt; i++)
			{
				std::string name = "BlurPow" + std::to_string(i);
				ImGui::DragFloat(name.c_str(), &m_BloomState.AddPow[i], 0.1f, 0.f, 10.f);
			}
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(setting);

}

void LightBloomPass::LoadSettingFromJson(const json11::Json & jsonObj)
{
	// 有効フラグ
	{
		auto item = jsonObj["Flgs"].object_items();
		m_IsEnable = item["LightBloom"].bool_value();
	}

	// LightBloomパラメータ
	{
		auto item = jsonObj["BloomState"].object_items();
		m_BloomState.Loop = item["Loop"].int_value();
		m_BloomState.Disp = (float)item["Disp"].number_value();
		m_BloomState.MipLoop = item["MipLoop"].int_value();
		for (int i = 0; i < m_BloomState.MipLoop; i++)
			m_BloomState.AddPow[i] = (float)item["BlurPow"].array_items()[i].number_value();
	}
}

void LightBloomPass::SaveSettingToJson(cereal::JSONOutputArchive& archive)
{
	archive(cereal::make_nvp("BloomState", m_BloomState));
}

ZString LightBloomPass::GetPassName()
{
	return "LightBloom Pass";
}

void LightBloomPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_IsEnable == false)return;
	if (m_pPostEffects == nullptr) return;
	
	auto texEmissive = m_pPostEffects->GetSourceTexture("Emissive");

	// 縮小バッファブラー
	{
		BlurShader::BlurRenderFlgs test[6] =
		{
			BlurShader::BlurRenderFlgs::Harf,
			BlurShader::BlurRenderFlgs::Quarter,
			BlurShader::BlurRenderFlgs::Eighth,
			BlurShader::BlurRenderFlgs::Sixteen,
			BlurShader::BlurRenderFlgs::ThirtyOne,
			BlurShader::BlurRenderFlgs::Sixty
		};

		m_pBs->ChangeRenderTarget(test[0]);
		m_pBs->GenerateBlur(texEmissive, m_BloomState.MipLoop, m_BloomState.Disp);

		for (int i = 1; i < m_BloomState.MipLoop; i++)
		{
			/*========================================*/
			//	バックバッファに加算
			/*========================================*/
			m_RT.RT(0, m_pPostEffects->GetCompletedTexture()->GetRTTex());
			m_RT.Depth(nullptr);
			m_RT.SetToDevice();
			
			m_pSr->Begin(false, true);
			ShMgr.m_bsAdd.SetState();

			auto& info = m_pPostEffects->GetCompletedTexture()->GetInfo();
			auto blurTex = m_pBs->GetTexture();
			m_pSr->Draw2D(blurTex->GetTex(), 0.f, 0.f,(float)info.Width, (float)info.Height, &ZVec4(m_BloomState.AddPow[i]));

			m_pSr->End();
			ShMgr.m_bsAlpha_AtoC.SetState();

			m_pBs->ChangeRenderTarget(test[i]);
			m_pBs->GenerateBlur(m_pBs->GetMipTex(test[i - 1]), m_BloomState.Loop, m_BloomState.Disp);
		}

	}


}

