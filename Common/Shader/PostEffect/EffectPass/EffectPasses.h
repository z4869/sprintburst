#ifndef __EFFECT_PASSES_H__
#define __EFFECT_PASSES_H__

#include "DOFPass.h"
#include "HEPass.h"
#include "LightBloomPass.h"
#include "SetupPass.h"
#include "SSAOPass.h"
#include "XRayPass.h"

#endif