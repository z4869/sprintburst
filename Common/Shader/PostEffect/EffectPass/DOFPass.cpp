#include "PCH/pch.h"
#include "CommonSubSystems.h"
#include "Shader/ShaderManager.h"
#include "DOFPass.h"

DOFPass::DOFPass() : ZPostEffectPass()
{
}

void DOFPass::Init()
{
	m_DOF.Init();

	m_TexDOF = Make_Shared(ZTexture, sysnew);
	m_TexDOF->CreateRT(ZDx.GetRezoW() / 2, ZDx.GetRezoH() / 2, DXGI_FORMAT_R16G16_UNORM);
	m_cb9_DOFParam.Create(9);
	m_cb9_DOFParam.SetPS();
	m_cb9_DOFParam.SetVS();

	// 念のため初期化時にもデータ書き込み(基本的にデータに変更があった際にのみ書き込む)
	m_cb9_DOFParam.WriteData();

	m_pBs = &ShMgr.GetRenderer<BlurShader>();
}

void DOFPass::Release()
{
	m_DOF.Release();
	m_cb9_DOFParam.Release();
}

void DOFPass::ImGui()
{
	auto setting = [this]()
	{
		if(ImGui::Begin(ZPostEffects::PostEffectWindowName) == false)
		{
			ImGui::End();
			return;
		}

		if (ImGui::CollapsingHeader("DoFParams"))
		{
			bool onChanged = false;
			onChanged |= ImGui::DragFloat("FarRange", &m_cb9_DOFParam.m_Data.FarState.x, 0.01f, 0.f, 1.f);
			onChanged |= ImGui::DragFloat("FarPow", &m_cb9_DOFParam.m_Data.FarState.y, 0.1f, 0.f, 100.f);
			onChanged |= ImGui::DragFloat("NearRange", &m_cb9_DOFParam.m_Data.NearState.x, 0.01f, 0.f, 1.f);
			onChanged |= ImGui::DragFloat("NearPow", &m_cb9_DOFParam.m_Data.NearState.y, 0.1f, 0.f, 100.f);
			onChanged |= ImGui::DragInt("UseFarBlur", &m_cb9_DOFParam.m_Data.UseFarBlur, 1, 0, 1);
			if (onChanged) m_cb9_DOFParam.WriteData(); // 1項目でも変更があった -> 変更後のデータ書き込み
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(setting);

	auto textureDebug = [this]()
	{
		if (ImGui::Begin(ZPostEffects::TexDebugWindowName))
		{
			if (ImGui::Begin("DOF"))
			{
				if (m_TexDOF != nullptr)
					ImGui::Image(m_TexDOF->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::End();
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(textureDebug);
}

void DOFPass::LoadSettingFromJson(const json11::Json& jsonObj)
{
	// 有効フラグ
	{
		auto item = jsonObj["Flgs"].object_items();
		m_IsEnable = item["DoF"].bool_value();
	}

	// DoFステータス
	{
		auto item = jsonObj["DoFState"].object_items();
		m_cb9_DOFParam.m_Data.FarState.Set(item["Far"]);
		m_cb9_DOFParam.m_Data.NearState.Set(item["Near"]);
		m_cb9_DOFParam.m_Data.UseFarBlur = item["UseFar"].int_value();
	}

	m_cb9_DOFParam.WriteData();
}

void DOFPass::SaveSettingToJson(cereal::JSONOutputArchive& archive)
{
	archive(cereal::make_nvp("DoFState", m_cb9_DOFParam.m_Data));
}

ZString DOFPass::GetPassName()
{
	return "DoF Pass";
}

void DOFPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_IsEnable == false) return;
	if (m_pPostEffects == nullptr) return;
	
	// 深度テクスチャ取得
	auto depth = m_pPostEffects->GetSourceTexture("Depth");
	if (depth == nullptr) return;

	// テクスチャクリア
	m_TexDOF->ClearRT(ZVec4(0));
	
	// DoFMap作成
	{
		auto texInfo = m_TexDOF->GetInfo();
		ZVec2 texSize((float)texInfo.Width, (float)texInfo.Height);
		m_DOF.GenerateDOFMap(depth, m_TexDOF, texSize);
	}

	// 被写界深度ブレンド
	{
		// Sourceの絵をぼかす
		m_pBs->ChangeRenderTarget(BlurShader::BlurRenderFlgs::Quarter);
		m_pBs->GenerateBlur(source, 2, 5.f);

		// ブレンド
		auto texCompleted = m_pPostEffects->GetCompletedTexture();
		auto texInfo = texCompleted->GetInfo();
		ZVec2 texSize((float)texInfo.Width, (float)texInfo.Height);
		m_DOF.Render(source, m_pBs->GetTexture(), m_TexDOF,texCompleted,texSize);

		// ブラーターゲットサイズを戻す
		m_pBs->ChangeRenderTarget();
	}
}
