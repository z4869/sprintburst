#ifndef __SETUP_PASS_H__
#define __SETUP_PASS_H__

class SpriteRenderer;

class SetupPass : public ZPostEffectPass
{
public:
	SetupPass();

	void Init();
	void Release() {};

	ZString GetPassName()override;
	void RenderPass(ZSP<ZTexture> source);
private:
	SpriteRenderer* m_pSr;
};

#endif