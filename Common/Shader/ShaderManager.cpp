#include "PCH/pch.h"
#include "ShaderManager.h"

ShaderManager::ShaderManager()
{
}

void ShaderManager::Init()
{
	// シェーダー初期化
	m_RendererSet.AddRenderer<LineRenderer>(2000);
	m_RendererSet.AddRenderer<SpriteRenderer>();
	m_RendererSet.AddRenderer<BlurShader>();
	m_RendererSet.GetRenderer<LineRenderer>().Init();
	m_RendererSet.GetRenderer<SpriteRenderer>().Init();
	m_RendererSet.GetRenderer<BlurShader>().Init();

	// よく使うブレンドステートを作成
	// 半透明合成用ステート
	m_bsAlpha.Set_Alpha(-1);
	m_bsAlpha.Create();
	// 加算合成用ステート
	m_bsAdd.Set_Add(-1);
	m_bsAdd.Create();
	// アルファ値無効モード
	m_bsNoAlpha.Set_NoAlpha(-1, true);
	m_bsNoAlpha.Create();
	// 半透明合成用ステート(AlphaToCoverage付き)
	m_bsAlpha_AtoC.Set_Alpha(-1);
	m_bsAlpha_AtoC.Set_AlphaToCoverageEnable(true);
	m_bsAlpha_AtoC.Create();

	// よく使うサンプラ作成& セット
	m_smp0_Linear_Wrap.SetAll_Standard();
	m_smp0_Linear_Wrap.Set_Wrap();
	m_smp0_Linear_Wrap.Set_Filter_Anisotropy(4);	// 高品質な異方性フィルタリングを使用(動作を重視したい場合はコメントにしてください)
	m_smp0_Linear_Wrap.Create();
	m_smp0_Linear_Wrap.SetStateVS(0);
	m_smp0_Linear_Wrap.SetStatePS(0);

	m_smp1_Linear_Clamp.SetAll_Standard();
	m_smp1_Linear_Clamp.Set_Clamp();
	m_smp1_Linear_Clamp.Set_Filter_Anisotropy(4);	// 高品質な異方性フィルタリングを使用(動作を重視したい場合はコメントにしてください)
	m_smp1_Linear_Clamp.Create();
	m_smp1_Linear_Clamp.SetStateVS(1);
	m_smp1_Linear_Clamp.SetStatePS(1);

	m_smp2_Point_Wrap.SetAll_Standard();
	m_smp2_Point_Wrap.Set_Filter_Point();
	m_smp2_Point_Wrap.Set_Wrap();
	m_smp2_Point_Wrap.Create();
	m_smp2_Point_Wrap.SetStateVS(2);
	m_smp2_Point_Wrap.SetStatePS(2);

	m_smp3_Point_Clamp.SetAll_Standard();
	m_smp3_Point_Clamp.Set_Filter_Point();
	m_smp3_Point_Clamp.Set_Clamp();
	m_smp3_Point_Clamp.Create();
	m_smp3_Point_Clamp.SetStateVS(3);
	m_smp3_Point_Clamp.SetStatePS(3);

	m_smp11_Point_Mirror.SetAll_Standard();
	m_smp11_Point_Mirror.Set_Filter_Point();
	m_smp11_Point_Mirror.Set_Mirror();
	m_smp11_Point_Mirror.Create();
	m_smp11_Point_Mirror.SetStateVS(11);
	m_smp11_Point_Mirror.SetStatePS(11);

	//	シャドウマップ用
	m_smp10_Shadow.SetAll_Standard();
	m_smp10_Shadow.Set_Border(ZVec4(1, 1, 1, 1));
	m_smp10_Shadow.Set_ComparisonMode();
	m_smp10_Shadow.Set_MipLOD(0, 0);
	m_smp10_Shadow.Create();
	m_smp10_Shadow.SetStatePS(10);
	m_smp10_Shadow.SetStateVS(10);

	// カメラ定数バッファ
	m_cb11_Camera.Create(11);
	m_cb11_Camera.SetVS();		// 頂点シェーダパイプラインへセット
	m_cb11_Camera.SetPS();		// ピクセルシェーダパイプラインへセット
	m_cb11_Camera.SetGS();		// ジオメトリシェーダパイプラインへセット

	// デバッグ用定数バッファ
	m_cb13_Debug.Create(13);
	m_cb13_Debug.SetVS();
	m_cb13_Debug.SetPS();
	m_cb13_Debug.SetGS();

	// キューブマップ
	m_TexCubeMap.LoadTexture("data/Texture/CubeMap.dds");
	m_TexCubeMap.SetTexturePS(20);
}

void ShaderManager::Release()
{
	m_RendererSet.Release();

	// ステート解放
	m_bsAdd.Release();
	m_bsAlpha.Release();

	// サンプラ解放
	m_smp0_Linear_Wrap.Release();
	m_smp1_Linear_Clamp.Release();
	m_smp2_Point_Wrap.Release();
	m_smp3_Point_Clamp.Release();
	m_smp10_Shadow.Release();
	m_smp11_Point_Mirror.Release();

	// カメラ用定数バッファー解放
	m_cb11_Camera.Release();

	m_cb13_Debug.Release();
}

void ShaderManager::UpdateCamera(const ZCamera* camera)
{
	if (camera == nullptr)return;

	// カメラ情報
	m_cb11_Camera.m_Data.mV = camera->m_mView;			// ビュー行列
	m_cb11_Camera.m_Data.mP = camera->m_mProj;			// 射影行列
	m_cb11_Camera.m_Data.CamPos = camera->m_mCam.GetPos();	// カメラ座標
	m_cb11_Camera.m_Data.m_Near = camera->m_Near;				// カメラ距離
	m_cb11_Camera.m_Data.m_Far = camera->m_Far;				// カメラ距離
	m_cb11_Camera.m_Data.mPO = camera->m_mOrtho;			// 射影行列

	m_cb11_Camera.WriteData();	// 定数バッファへ書き込む

	m_cb11_Camera.SetVS();		// 頂点シェーダパイプラインへセット
	m_cb11_Camera.SetPS();		// ピクセルシェーダパイプラインへセット
	m_cb11_Camera.SetGS();		// ジオメトリシェーダパイプラインへセット
}