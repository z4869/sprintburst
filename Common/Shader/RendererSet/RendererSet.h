#ifndef __RENDERER_SET_H__
#define __RENDERER_SET_H__

#include "TypeIndex.h"

class RendererBase;

class RendererSet
{
public:
	~RendererSet();

	void Release();

	template<typename T,typename... Args>
	T& AddRenderer(Args&&... args);

	template<typename T>
	T& GetRenderer();

	template<typename T>
	bool HasRenderer();

	template<typename T>
	void RemoveRenderer();


private:
	ZVector<size_t> m_Orders;
	ZUnorderedMap<size_t, ZSP<RendererBase>> m_Renderers;
};

#include "RendererSet.inl"

#endif