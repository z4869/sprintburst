#include "inc_Shadow.hlsli"
#include "../inc_LightCB.hlsli"

VsDepthOut main(VsDepthInstIn In)
{
	VsDepthOut Out = (VsDepthOut)0;

	Out.pos = mul(In.pos, In.instanceMat);
	Out.pos = mul(Out.pos, LightVP);

	Out.Depth.xy = Out.pos.zw;

	Out.uv = In.uv;

	return Out;
}