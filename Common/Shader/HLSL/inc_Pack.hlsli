// 主に3次元ベクトルの法線を2次元ベクトルに圧縮
float2 encode(float3 n)
{
	static const float PI = 3.141592653589f;
	return (float2(atan2(n.y, n.x) / PI, n.z) + 1.0)*0.5;
}

// ↑で圧縮された2次元ベクトルをもとに3次元ベクトルを算出
float3 decode(float2 enc)
{
	static const float PI = 3.141592653589f;
	float2 ang = enc * 2 - 1;
	float2 scth;
	sincos(ang.x * PI, scth.x, scth.y);
	float2 scphi = float2(sqrt(1.0 - ang.y*ang.y), ang.y);
	return float3(scth.y*scphi.x, scth.x*scphi.x, scphi.y);
}