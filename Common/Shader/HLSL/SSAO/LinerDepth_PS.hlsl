#include "inc_SSAO.hlsli"

#include "../inc_CameraCB.hlsli"

float GetLinerDepth(float d)
{
	float z_b = d;
	float z_n = 2.0 * z_b - 1.0;
	float liner = 2.0 * g_Near * g_Far / (g_Far + g_Near - z_n * (g_Far - g_Near));
	//float liner = 2.0 * g_Far * g_Near / (g_Near + g_Far - z_n * (g_Near - g_Far));
	return liner;
}

float4 main(VS_OUT In) : SV_Target0
{
	float ld = GetLinerDepth(DepthTex.Sample(PointClampSmp, In.UV).r);
	return float4(ld, 0, 0, 1);
	//return DepthTex.Sample(PointClampSmp, In.UV);
}
