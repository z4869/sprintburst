#include "inc_SSAO.hlsli"
#include "../inc_CameraCB.hlsli"
#include "../inc_Pack.hlsli"

// 参考 : https://github.com/turanszkij/WickedEngine

#define NUM_SAMP 10
static const float InvSamp = 1.0 / (float)NUM_SAMP;

static const float3 SAMPLE_POS[NUM_SAMP] =
{
#if NUM_SAMP==16
	float3(0.355512, -0.709318, -0.102371),
	float3(0.534186, 0.71511, -0.115167),
	float3(-0.87866, 0.157139, -0.115167),
	float3(0.140679, -0.475516, -0.0639818),
	float3(-0.0796121, 0.158842, -0.677075),
	float3(-0.0759516, -0.101676, -0.483625),
	float3(0.12493, -0.0223423, -0.483625),
	float3(-0.0720074, 0.243395, -0.967251),
	float3(-0.207641, 0.414286, 0.187755),
	float3(-0.277332, -0.371262, 0.187755),
	float3(0.63864, -0.114214, 0.262857),
	float3(-0.184051, 0.622119, 0.262857),
	float3(0.110007, -0.219486, 0.435574),
	float3(0.235085, 0.314707, 0.696918),
	float3(-0.290012, 0.0518654, 0.522688),
	float3(0.0975089, -0.329594, 0.609803)
#elif NUM_SAMP==10
	float3(-0.010735935, 0.01647018, 0.0062425877),
	float3(-0.06533369, 0.3647007, -0.13746321),
	float3(-0.6539235, -0.016726388, -0.53000957),
	float3(0.40958285, 0.0052428036, -0.5591124),
	float3(-0.1465366, 0.09899267, 0.15571679),
	float3(-0.44122112, -0.5458797, 0.04912532),
	float3(0.03755566, -0.10961345, -0.33040273),
	float3(0.019100213, 0.29652783, 0.066237666),
	float3(0.8765323, 0.011236004, 0.28265962),
	float3(0.29264435, -0.40794238, 0.15964167)
#endif
};

float4 main(VS_OUT In) : SV_Target0
{
	// 法線取得
	float3 normal = decode(NormalTex.Sample(LinerClampSmp, In.UV.xy).xy);
	// 乱数取得
	float3 fres = normalize(RandomTex.Load(int3((64 * In.UV.xy * 400) % 64, 0)).xyz * 2.0 - 1.0);
	// 深度取得
	float depth = DepthTex.Sample(PointClampSmp, In.UV).r * g_Far;

	float3 ep = float3(In.UV, depth);
	float bl = 0.0;
	float radD = Rad / depth;

	float3 ray;
	float3 occFrag;
	float depthDiff;
	float depthAO = 0;

	[unroll]
	for (int i = 0; i < NUM_SAMP; i++)
	{
		ray = radD * reflect(SAMPLE_POS[i], fres);
		float2 newUV = ep.xy + ray.xy;

		occFrag = decode(NormalTex.Sample(LinerClampSmp, newUV).xy);
		if (!occFrag.x && !occFrag.y && !occFrag.z)
			break;
		depthDiff = (depth - (DepthTex.Sample(PointMirrorSmp, newUV).r * g_Far));
		if (depthDiff < 0)continue; // 隠蔽されていない

		bl += step(Falloff, depthDiff) * (1.0 - saturate(dot(occFrag.xyz, normal)))
				* (1 - smoothstep(Falloff, Strength, depthDiff));
		depthAO += abs(depthDiff) / g_Far;
	}

	float ao = 1.0 - bl * InvSamp + depthAO;
	return ao.xxxx;
}