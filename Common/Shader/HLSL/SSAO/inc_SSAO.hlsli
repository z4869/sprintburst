// 定数バッファ
cbuffer cbPerObject : register(b0)
{
	row_major float4x4  g_mTransform;
};

cbuffer cbSSAOParam : register(b1)
{
	float Strength = 8.f;
	float Falloff = -0.4f;
	float Rad = 0.26f;
}

//============================================
// テクスチャ
//============================================
Texture2D NormalTex : register(t0);
Texture2D DepthTex : register(t1);
Texture2D RandomTex : register(t2);
Texture2D DiffuseTex : register(t3);
Texture2D SSAOTex : register(t4);

SamplerState LinerClampSmp : register(s1);
SamplerState PointClampSmp : register(s3);
SamplerState PointMirrorSmp:register(s11);

//============================================
// 頂点シェーダ出力用構造体
//============================================
struct VS_OUT
{
	float4 Pos : SV_Position;
	float2 UV  : TEXCOORD0;
};
