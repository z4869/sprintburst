#include "inc_X-Ray.hlsli"

PsOut main(PsIn In)
{
    float4 CharaDepth = CharaDepthTex.Sample(WrapSmp, In.uv);
    clip(CharaDepth.a - 0.01f);
    float AllDepth = AllDepthTex.Sample(WrapSmp, In.uv).r;
    float4 BaseCol = BaseTex.Sample(WrapSmp, In.uv);
    float4 RefPow = RefTex.Sample(WrapSmp, In.uv); //
//    float RefPow = RefTex.Sample(WrapSmp, In.uv).r;


    PsOut Out;
 
    if (CharaDepth.r > AllDepth)
    {
     //   Out.Col = (g_XColor * RefPow) + (BaseCol * (1 - RefPow));
        Out.Col = RefPow + (BaseCol * (1 - RefPow));
    }
    else
    {
        Out.Col = BaseCol;
    }

    return Out;
}