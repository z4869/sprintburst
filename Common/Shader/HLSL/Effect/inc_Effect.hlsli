//===================================
// オブジェクト単位データ
//===================================
cbuffer cbPerObject : register(b1)
{
    row_major float4x4 g_mW; // ワールド変換行列
    float4 g_Color; // 色
};

// テクスチャ
Texture2D InputTex : register(t0);
Texture2D EmissiveTex : register(t1);

// サンプラ
SamplerState ClampSmp : register(s1);

// 頂点シェーダ出力用
struct VS_OUT
{
    float4 Pos : SV_Position;
    float2 UV : TEXCOORD0;
    float4 Color : COLOR;
};
