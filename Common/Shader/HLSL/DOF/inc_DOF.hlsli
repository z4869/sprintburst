//	Texture
Texture2D		DOFTex	: register(t0);
Texture2D		BaseTex	: register(t1);
Texture2D		BlurTex	: register(t2);
SamplerState	Smp		: register(s1);

//
cbuffer bView : register(b0)
{
    row_major float4x4 g2DView;
}

//
struct VsIn
{
    float4 pos : POSITION;
    float2 uv : TEXCOORD;
};

struct VsOut
{
    float4 pos : SV_Position;
    float2 uv : TEXCOORD;
};
typedef VsOut PsIn;

struct PsOut
{
    float4 Col : SV_Target0;
};