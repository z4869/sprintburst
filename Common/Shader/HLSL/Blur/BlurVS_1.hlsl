#include "inc_Blur.hlsli"

VsGaussOut main(VsIn In)
{
    VsGaussOut output = (VsGaussOut) 0;

    output.pos = mul(In.pos, g2DView);
	
    float2 uvS = Texel * 0.5f; //	テクセル分UVずらす

    output.texcoord0 = In.uv + float2(-1.0f / Size.x, 0.0f) - uvS;
    output.texcoord1 = In.uv + float2(-3.0f / Size.x, 0.0f) - uvS;
    output.texcoord2 = In.uv + float2(-5.0f / Size.x, 0.0f) - uvS;
    output.texcoord3 = In.uv + float2(-7.0f / Size.x, 0.0f) - uvS;
    output.texcoord4 = In.uv + float2(-9.0f / Size.x, 0.0f) + uvS;
    output.texcoord5 = In.uv + float2(-11.0f / Size.x, 0.0f) + uvS;
    output.texcoord6 = In.uv + float2(-13.0f / Size.x, 0.0f) + uvS;
    output.texcoord7 = In.uv + float2(-15.0f / Size.x, 0.0f) + uvS;

    return output;
}