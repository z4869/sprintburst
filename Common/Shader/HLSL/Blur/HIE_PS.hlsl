#include "inc_Blur.hlsli"
PsOut main(VsOut In)
{

    PsOut Out;
    Out.Col = ColorTex.Sample(ColorSmp, In.uv);
	

//    Out.Col.rgb = max(0, Out.Col.rgb*1.6f - 1);	//	エミッシブ値の範囲を広げる
    Out.Col.rgb = max(0, Out.Col.rgb - 1);

	return Out;
}