#include "inc_Blur.hlsli"

VsGaussOut main(VsIn In)
{
    VsGaussOut output = (VsGaussOut) 0;

    output.pos = mul(In.pos, g2DView);

    float2 uvS = Texel * 0.5f;

    output.texcoord0 = In.uv + float2(0.0f, -1.0f / Size.y) - uvS;
    output.texcoord1 = In.uv + float2(0.0f, -3.0f / Size.y) - uvS;
    output.texcoord2 = In.uv + float2(0.0f, -5.0f / Size.y) - uvS;
    output.texcoord3 = In.uv + float2(0.0f, -7.0f / Size.y) - uvS;
    output.texcoord4 = In.uv + float2(0.0f, -9.0f / Size.y) + uvS;
    output.texcoord5 = In.uv + float2(0.0f, -11.0f / Size.y) + uvS;
    output.texcoord6 = In.uv + float2(0.0f, -13.0f / Size.y) + uvS;
    output.texcoord7 = In.uv + float2(0.0f, -15.0f / Size.y) + uvS;

    return output;
}