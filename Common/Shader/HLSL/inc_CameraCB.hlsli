//===================================
// カメラデータ
//===================================
cbuffer cbCamera : register(b11)
{
    row_major float4x4 g_mV; // ビュー行列
    row_major float4x4 g_mP; // 射影行列
    row_major float4x4 g_mPO; //　正射影の行列
	// カメラ
    float3 g_CamPos; // カメラ座標

	//	カメラの描画距離
    float g_Near = 0.1f;
    float g_Far = 100.f;
	
};

//	一応カメラ効果ということでここに
cbuffer cbDoF :register(b9)
{
    float2 g_DoF_Far;
    float2 g_DoF_Near;
    int g_DoF_FarEnable;	//	奥ボケを失くす時は０
};

