#include "inc_Sprite.hlsli"

//============================================
// 2D�`�� ���_�V�F�[�_
//============================================
VS_OUT main(float4 pos : POSITION,
			float2 uv : TEXCOORD0,
			float4 color : COLOR0)
{
	VS_OUT Out = (VS_OUT)0;

	// ���_���W���ˉe�ϊ�
	Out.Pos = mul(pos, g_mTransform);

	// UV
	Out.UV = uv;

	// ���_�F
	Out.Color = color;

	return Out;
}
