#include "inc_Line.hlsli"
#include "../inc_CameraCB.hlsli"

// 頂点シェーダ
VS_OUT main(	float4 pos : POSITION,  // 座標
			    float4 color : COLOR0   // 頂点色
)
{
	VS_OUT Out = (VS_OUT)0;

	// 2D変換
	Out.Pos = mul(pos, g_mW);
	Out.Pos = mul(Out.Pos, g_mV);
	Out.Pos = mul(Out.Pos, g_mP);

	// 頂点色をそのまま渡す
	Out.Color = color;

	return Out;
}
