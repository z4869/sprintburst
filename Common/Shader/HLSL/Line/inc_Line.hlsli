//===================================
// オブジェクト単位データ
//===================================
cbuffer cbPerObject : register(b1)
{
    row_major float4x4 g_mW; // ワールド変換行列
};

// 頂点シェーダ出力用
struct VS_OUT
{
    float4 Pos : SV_Position;
    float4 Color : COLOR;
};
