﻿#include "XboxConInput.h"

void XboxConInput::StateUpdate()
{
	DWORD result;
	// Zeroise the state

	for (int i = 0; i < MAX_CONTROLLERS; i++) {
		ZeroMemory(&m_Controllers[i].state, sizeof(XINPUT_STATE));

		// Get the state
		result = XInputGetState(i, &m_Controllers[i].state);

		if (result == ERROR_SUCCESS)
		{
			m_Controllers[i].bConnected = true;
		}
		else
		{
			m_Controllers[i].bConnected = false;
		}
	}
	return;
}

bool XboxConInput::IsConnected(int _playerNumber)
{
	return m_Controllers[_playerNumber].bConnected;
}

XINPUT_STATE XboxConInput::GetState(int _playerNumber)
{
	if ((m_Controllers[_playerNumber].state.Gamepad.sThumbLX < INPUT_DEADZONE &&
		m_Controllers[_playerNumber].state.Gamepad.sThumbLX > -INPUT_DEADZONE) &&
		(m_Controllers[_playerNumber].state.Gamepad.sThumbLY < INPUT_DEADZONE &&
			m_Controllers[_playerNumber].state.Gamepad.sThumbLY > -INPUT_DEADZONE))
	{
		m_Controllers[_playerNumber].state.Gamepad.sThumbLX = 0;
		m_Controllers[_playerNumber].state.Gamepad.sThumbLY = 0;
	}

	if ((m_Controllers[_playerNumber].state.Gamepad.sThumbRX < INPUT_DEADZONE &&
		m_Controllers[_playerNumber].state.Gamepad.sThumbRX > -INPUT_DEADZONE) &&
		(m_Controllers[_playerNumber].state.Gamepad.sThumbRY < INPUT_DEADZONE &&
			m_Controllers[_playerNumber].state.Gamepad.sThumbRY > -INPUT_DEADZONE))
	{
		m_Controllers[_playerNumber].state.Gamepad.sThumbRX = 0;
		m_Controllers[_playerNumber].state.Gamepad.sThumbRY = 0;
	}

	return m_Controllers[_playerNumber].state;

}

WORD XboxConInput::GetStateButton(int _playerNumber)
{
	WORD wButtons = m_Controllers[_playerNumber].state.Gamepad.wButtons;

	return wButtons;
}

bool XboxConInput::GetStateButton(int _playerNumber, const char * str)
{
	WORD wButtons = m_Controllers[_playerNumber].state.Gamepad.wButtons;

	//ABXYボタン
	if (str == "A") {
		return (wButtons & XINPUT_GAMEPAD_A) ? true : false;
	}
	if (str == "B") {
		return (wButtons & XINPUT_GAMEPAD_B) ? true : false;
	}
	if (str == "X") {
		return (wButtons & XINPUT_GAMEPAD_X) ? true : false;
	}
	if (str == "Y") {
		return (wButtons & XINPUT_GAMEPAD_Y) ? true : false;
	}

	//方向パッド
	if (str == "UP") {
		return (wButtons & XINPUT_GAMEPAD_DPAD_UP) ? true : false;
	}
	if (str == "DOWN") {
		return (wButtons & XINPUT_GAMEPAD_DPAD_DOWN) ? true : false;
	}
	if (str == "LEFT") {
		return (wButtons & XINPUT_GAMEPAD_DPAD_LEFT) ? true : false;
	}
	if (str == "RIGHT") {
		return (wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) ? true : false;
	}

	//スティッククリック
	if (str == "LS") {
		return (wButtons & XINPUT_GAMEPAD_LEFT_THUMB) ? true : false;
	}
	if (str == "RS") {
		return (wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) ? true : false;
	}

	//人差し指ボタン
	if (str == "LB") {
		return (wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) ? true : false;
	}
	if (str == "RB") {
		return (wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) ? true : false;
	}

	//メニューボタン
	if (str == "START") {
		return (wButtons & XINPUT_GAMEPAD_START) ? true : false;
	}
	if (str == "BACK") {
		return (wButtons & XINPUT_GAMEPAD_BACK) ? true : false;
	}

	return false;
}

bool XboxConInput::ButtonPressed(int _playerNumber, const char * str)
{
	WORD wButtons = m_Controllers[_playerNumber].state.Gamepad.wButtons;

	//ABXYボタン
	if (str == "A") {
		if ((wButtons & XINPUT_GAMEPAD_A)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "B") {
		if ((wButtons & XINPUT_GAMEPAD_B)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "X") {
		if ((wButtons & XINPUT_GAMEPAD_X)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "Y") {
		if ((wButtons & XINPUT_GAMEPAD_Y)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}

	//方向パッド
	if (str == "UP") {
		if ((wButtons & XINPUT_GAMEPAD_DPAD_UP)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "DOWN") {
		if ((wButtons & XINPUT_GAMEPAD_DPAD_DOWN)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "LEFT") {
		if ((wButtons & XINPUT_GAMEPAD_DPAD_LEFT)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "RIGHT") {
		if ((wButtons & XINPUT_GAMEPAD_DPAD_RIGHT)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}

	//スティッククリック
	if (str == "LS") {
		if ((wButtons & XINPUT_GAMEPAD_LEFT_THUMB)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "RS") {
		if ((wButtons & XINPUT_GAMEPAD_RIGHT_THUMB)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}

	//人差し指ボタン
	if (str == "LB") {
		if ((wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "RB") {
		if ((wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}

	//メニューボタン
	if (str == "START") {
		if ((wButtons & XINPUT_GAMEPAD_START)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}
	if (str == "BACK") {
		if ((wButtons & XINPUT_GAMEPAD_BACK)) {
			m_ButtonMap[_playerNumber][str] += 1;
			if (m_ButtonMap[_playerNumber][str] >= 2) {
				m_ButtonMap[_playerNumber][str] = 2;
			}
		}
		else {
			m_ButtonMap[_playerNumber][str] = 0;
		}
		return (m_ButtonMap[_playerNumber][str] == 1) ? true : false;
	}

	return false;
}


float XboxConInput::GetStateThumb(int _playerNumber, const char * str)
{
	if ((m_Controllers[_playerNumber].state.Gamepad.sThumbLX < INPUT_DEADZONE &&
		m_Controllers[_playerNumber].state.Gamepad.sThumbLX > -INPUT_DEADZONE) &&
		(m_Controllers[_playerNumber].state.Gamepad.sThumbLY < INPUT_DEADZONE &&
			m_Controllers[_playerNumber].state.Gamepad.sThumbLY > -INPUT_DEADZONE))
	{
		m_Controllers[_playerNumber].state.Gamepad.sThumbLX = 0;
		m_Controllers[_playerNumber].state.Gamepad.sThumbLY = 0;
	}

	if ((m_Controllers[_playerNumber].state.Gamepad.sThumbRX < INPUT_DEADZONE &&
		m_Controllers[_playerNumber].state.Gamepad.sThumbRX > -INPUT_DEADZONE) &&
		(m_Controllers[_playerNumber].state.Gamepad.sThumbRY < INPUT_DEADZONE &&
			m_Controllers[_playerNumber].state.Gamepad.sThumbRY > -INPUT_DEADZONE))
	{
		m_Controllers[_playerNumber].state.Gamepad.sThumbRX = 0;
		m_Controllers[_playerNumber].state.Gamepad.sThumbRY = 0;
	}
	
	float LX, LY, RX, RY;




	if (str == "LX") {
		if (m_Controllers[_playerNumber].state.Gamepad.sThumbLX > 0) {
			LX = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbLX / (float)MAXSHORT;
		}
		else {
			LX = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbLX / (float)MINSHORT;
		}
		return LX;
	}
	if (str == "LY") {
		if (m_Controllers[_playerNumber].state.Gamepad.sThumbLY > 0) {
			LY = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbLY / (float)MAXSHORT;
		}
		else {
			LY = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbLY / (float)MINSHORT;
		}
		return LY;
	}
	if (str == "RX") {
		if (m_Controllers[_playerNumber].state.Gamepad.sThumbRX > 0) {
			RX = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbRX / (float)MAXSHORT;
		}
		else {
			RX = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbRX / (float)MINSHORT;
		}
		return RX;
	}
	if (str == "RY") {
		if (m_Controllers[_playerNumber].state.Gamepad.sThumbRY > 0) {
			RY = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbRY / (float)MAXSHORT;
		}
		else {
			RY = (float)m_Controllers[_playerNumber].state.Gamepad.sThumbRY / (float)MINSHORT;
		}
		return RY;
	}

	return 0;
}

float XboxConInput::GetStateTrigger(int _playerNumber, const char * str)
{
	float LT, RT;
	if (str == "LT") {
		LT = (float)m_Controllers[_playerNumber].state.Gamepad.bLeftTrigger / (float)MAXBYTE;
		return LT;
	}
	if (str == "RT") {
		RT = (float)m_Controllers[_playerNumber].state.Gamepad.bRightTrigger / (float)MAXBYTE;
		return RT;
	}

	return 0;
}



