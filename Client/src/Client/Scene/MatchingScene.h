#ifndef __MATCHING_SCENE_H__
#define __MATCHING_SCENE_H__

#include "MessageTypes.h"

class MatchingScene : public ZSceneBase
{
public:
	virtual ~MatchingScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();


private:
	bool m_DisConnectedPopUpFlg;

	ZUnorderedMap<ZUUID, LoginData> m_MatchingPlayers;
	ZVector<ZUnorderedMap<ZUUID, LoginData>::iterator> m_OrderdPlayers;

	ZString m_ErrorMsg;
	bool m_IsError;

	uint m_CountDownTime;
	bool m_IsStartCountDown;

	float m_LastMatchingTimeLimit;
	bool m_IsMatchingTimeOver;
};

#endif