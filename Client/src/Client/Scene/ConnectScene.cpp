#include "Client/ClientApp.h"
#include "MainSystems/GameWorld/GameWorld.h"

#include "ECSComponents/Common/CommonComponents.h"
#include "ECSSystems/Common/CommonSystems.h"

#include "ECSComponents/MapObject/MapObjectComponents.h"
#include "ECSSystems/MapObject/MapObjectSystems.h"

#include "ECSComponents/Character/CharacterComponents.h"
#include "ECSSystems/Character/CharacterSystems.h"

#include "ECSListeners/Common/CommonListeners.h"

#include "MainSystems/GameWorld/GameWorld.h"

#include "Client/ClientSubSystems/ClientSubSystems.h"
#include "ConnectScene.h"

void ConnectScene::Init()
{
	strcpy(m_ServerAddress, "localhost");
	strcpy(m_LoginData.Name, "Unknown");
	m_IsTryConnecting = false;
	
	m_Cam.Init(0, 0, -5);
	ShMgr.GetRenderer<BlurShader>().CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));

}

void ConnectScene::Update()
{
	if (GW.m_IsFading)return;

	// サーバーに接続できたか
	if (CLIENT.IsConnected())
	{
		// マッチングシーンに移行
		GW.ChangeScene("Matching",false);
		return;
	}

	if (INPUT.KeyEnter(VK_ESCAPE))
		APP.ExitGameLoop(); // 仮置き

	CLIENT.Update();
	m_Cam.m_mCam = m_Cam.m_LocalMat * m_Cam.m_BaseMat;
	m_Cam.Update();
}

void ConnectScene::ImGuiUpdate()
{
	if (CLIENT.IsConnected()) return;

	auto connectFunc = [this]()
	{
		auto windowFlag = ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoCollapse;
		if (ImGui::Begin("Connect Server",nullptr,windowFlag))
		{
			// PlayStyle
			{
				int style = m_LoginData.Style;
				ImGui::RadioButton("Hacker", &style, LoginData::Hacker);
				ImGui::SameLine();
				ImGui::RadioButton("Security", &style, LoginData::Security);
				m_LoginData.Style = (LoginData::PlayStyle)style;
			}
			ImGui::InputText("UserName", m_LoginData.Name, 32);
			ImGui::InputText("Server Address", m_ServerAddress, 256);
			ImGui::SameLine();
			if (m_IsTryConnecting == false)
			{
				if (ImGui::Button("Connect") && strcmp("",m_LoginData.Name) != 0)
				{
					m_IsTryConnecting = true;
					GW.StartLoadEffect();
					// 接続試行
					CLIENT.Connect(m_ServerAddress,
						[this](bool isConnected)
					{
						if(isConnected)
							CLIENT.SendData((char)MessageTypes::Login, &m_LoginData, sizeof LoginData, 0);
						m_IsTryConnecting = false;
						GW.StopLoadEffect();
					});
				}
			}
			else
				ImGui::Text("Connecting...");
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(connectFunc);
	
}

void ConnectScene::Draw()
{

	// 半透明モード
	ShMgr.m_bsAlpha.SetState();
	
	//// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
	//ShMgr.m_bsAlpha_AtoC.SetState();
	
	// カメラのデータをシェーダ側に転送
	m_Cam.SetCamera();
	
	auto& pSr = ShMgr.GetRenderer<SpriteRenderer>();
	pSr.Begin(false, true);
	{
		// 中央にロゴ表示
		auto texInfo = GW.m_TexBackImg->GetInfo();
		auto& window = APP.m_Window;
		float y = (window->GetHeight() - texInfo.Height) / 2.0f;

		pSr.Draw2D(*GW.m_TexBackImg, 0,y, (float)window->GetWidth(), (float)texInfo.Height,&ZVec4(0.2f,0.2f,0.2f,1.f));
	}
	pSr.End();
}

void ConnectScene::Release()
{
	THPOOL.WaitForAllTasksFinish();
	CLIENT.RemoveAllCallBackFunction("ConnectScene");
}
