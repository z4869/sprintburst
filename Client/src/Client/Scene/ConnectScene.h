#ifndef __CONNECT_SCENE_H__
#define __CONNECT_SCENE_H__

#include "MessageTypes.h"

#include "Camera/GameCamera.h"

class ConnectScene : public ZSceneBase
{
public:
	virtual ~ConnectScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

private:
	char m_ServerAddress[128];
	LoginData m_LoginData;

	bool m_IsTryConnecting;

	// カメラ
	GameCamera m_Cam;
};

#endif