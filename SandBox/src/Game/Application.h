#ifndef __APPLICATION_H__
#define __APPLICATION_H__


#include "MainFrame/ZMainFrame.h"
#include "SubSystems.h"
#include "Camera/LightCamera.h"
#include "Shader/Light/LightManager.h"
#include "Shader/Renderer/ModelRenderer/ModelRenderingPipeline.h"
#include "Shader/ShaderManager.h"
#include "MainSystems/ClosureTaskManager/ClosureTaskManager.h"
#include "MainSystems/CollisionEngine/CollisionEngine.h"
#include "MainSystems/GameWorld/GameWorld.h"

#include "Shader/PostEffect/PostEffects.h"

#include "Shader/PostEffect/EffectPass/SetupPass.h"
#include "Shader/PostEffect/EffectPass/DOFPass.h"
#include "Shader/PostEffect/EffectPass/XRayPass.h"
#include "Shader/PostEffect/EffectPass/LightBloomPass.h"
#include "Shader/PostEffect/EffectPass/HEPass.h"
#include "Shader/PostEffect/EffectPass/SSAOPass.h"

#include "CommonSubSystems.h"

class Application : public ZMainFrame
{
public:
	Application(const char* wndTitle, const ZWindowProperties& properties);

protected:
	virtual bool Init()override;
	virtual void FrameUpdate()override;
	virtual void Release()override;

};

#endif